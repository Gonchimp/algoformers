package fiuba.algo3.tests;

import java.util.ArrayList;
import org.junit.Assert;
import org.junit.Test;
import algoformers.Megatron;
import algoformers.OptimusPrime;
import bonus.Burbuja;
import bonus.DobleCanion;
import bonus.Flash;
import superficies.Rocosa;
import utilidadesDeJuego.Posicion;
import utilidadesDeJuego.Recorrido;
import utilidadesDeJuego.Tablero;

public class BonusTest {
	
	@Test
	public void test01dobleCanionDuplicaDanio(){
		
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		
		Posicion pos1 = new Posicion(0,0,0);
		Posicion pos2 = new Posicion(1,0,0);
		Posicion pos3 = new Posicion(2,0,0);
		
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		posiciones.add(pos1);
		posiciones.add(pos2);
		posiciones.add(pos3);
		
		Rocosa rocosa = new Rocosa();
		tablero.establecerSuperficie(posiciones, rocosa);
		
		tablero.agregar(optimusPrime, pos1);
		
		
		DobleCanion bonus = new DobleCanion();
		tablero.agregarBonus(bonus, pos2);
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(optimusPrime, tablero);
		
		double ataque = optimusPrime.obtenerAtaque();
		ataque = ataque*2;
		
		recorrido.avanzar(optimusPrime, pos2, tablero);
		recorrido.avanzar(optimusPrime, pos3, tablero);
		
		Assert.assertTrue(optimusPrime.obtenerPosicionEnTablero().equals(pos3));
		
		Assert.assertTrue(optimusPrime.obtenerAtaque() == ataque);
	}
	
	@Test
	public void test02dobleCanionDuplicaDanioModoAlterno(){
			
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		
				Posicion pos1 = new Posicion(0,0,0);
		Posicion pos2 = new Posicion(1,0,0);
		Posicion pos3 = new Posicion(2,0,0);
		
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		posiciones.add(pos1);
		posiciones.add(pos2);
		posiciones.add(pos3);
		
		Rocosa rocosa = new Rocosa();
		tablero.establecerSuperficie(posiciones, rocosa);
		
		tablero.agregar(optimusPrime, pos1);
			
		DobleCanion bonus = new DobleCanion();
		tablero.agregarBonus(bonus, pos2);
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(optimusPrime, tablero);
		
		optimusPrime.transformar(tablero);
		double ataque = optimusPrime.obtenerAtaque();
		ataque = ataque*2;
		
		recorrido.avanzar(optimusPrime, pos2, tablero);
		recorrido.avanzar(optimusPrime, pos3, tablero);
		
		Assert.assertTrue(optimusPrime.obtenerPosicionEnTablero().equals(pos3));
		
		Assert.assertTrue(optimusPrime.obtenerAtaque() == ataque);
		
	}
	
	@Test
	public void test03burbujaAbsorbeElDanio(){
		
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		Megatron megatron = new Megatron();
		
		Posicion pos1 = new Posicion(0,0,0);
		Posicion pos2 = new Posicion(1,0,0);
		Posicion pos3 = new Posicion(2,0,0);
		
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		posiciones.add(pos1);
		posiciones.add(pos2);
		posiciones.add(pos3);
		
		Rocosa rocosa = new Rocosa();
		tablero.establecerSuperficie(posiciones, rocosa);
		
		tablero.agregar(optimusPrime, pos1);
		tablero.agregar(megatron, pos3);
		
		
		Burbuja bonus = new Burbuja();
		tablero.agregarBonus(bonus, pos2);
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(optimusPrime, tablero);
		
		double vidaAux = optimusPrime.obtenerVida();
		
		recorrido.avanzar(optimusPrime, pos2, tablero); //agarra burbuja
		
	
		
		Assert.assertTrue(optimusPrime.obtenerPosicionEnTablero().equals(pos2));
		
		Assert.assertTrue(optimusPrime.obtenerVida() == vidaAux);
		
		megatron.atacar(optimusPrime,tablero);
		Assert.assertTrue(optimusPrime.obtenerVida() == vidaAux);
		optimusPrime.descontarTurno();	//1 turno de burbuja restante
		
		megatron.atacar(optimusPrime,tablero);
		Assert.assertTrue(optimusPrime.obtenerVida() == vidaAux);
		optimusPrime.descontarTurno(); 	// burbuja desactivada
		
		megatron.atacar(optimusPrime,tablero);
		Assert.assertFalse(optimusPrime.obtenerVida() == vidaAux);
		optimusPrime.descontarTurno();
	}
	
	@Test
	public void test04burbujaAbsorbeElDanioModoAlterno(){
		
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		Megatron megatron = new Megatron();
		
		Posicion pos1 = new Posicion(0,0,0);
		Posicion pos2 = new Posicion(1,0,0);
		Posicion pos3 = new Posicion(2,0,0);
		
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		posiciones.add(pos1);
		posiciones.add(pos2);
		posiciones.add(pos3);
		
		Rocosa rocosa = new Rocosa();
		tablero.establecerSuperficie(posiciones, rocosa);
		
		tablero.agregar(optimusPrime, pos1);
		tablero.agregar(megatron, pos3);
		
		
		Burbuja bonus = new Burbuja();
		tablero.agregarBonus(bonus, pos2);
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(optimusPrime, tablero);
		
		recorrido.avanzar(optimusPrime, pos2, tablero);
		optimusPrime.transformar(tablero);     //MODO ALTERNO
		double vidaAux = optimusPrime.obtenerVida();
		
		Assert.assertTrue(optimusPrime.obtenerPosicionEnTablero().equals(pos2));
		
		Assert.assertTrue(optimusPrime.obtenerVida() == vidaAux);
		
		megatron.atacar(optimusPrime,tablero);
		Assert.assertTrue(optimusPrime.obtenerVida() == vidaAux);
		optimusPrime.descontarTurno();	//1 turno de burbuja restante
		
		optimusPrime.transformar(tablero); // VUELTA AL MODO COMUN
		
		megatron.atacar(optimusPrime,tablero);
		Assert.assertTrue(optimusPrime.obtenerVida() == vidaAux);
		optimusPrime.descontarTurno(); 	// burbuja desactivada
		
		optimusPrime.transformar(tablero); // VUELTA AL MODO ALTERNO
		
		megatron.atacar(optimusPrime,tablero);
		Assert.assertFalse(optimusPrime.obtenerVida() == vidaAux);

	}
	
	@Test
	public void test05flashTriplicaVelocidad(){
		
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		
		Posicion pos1 = new Posicion(0,0,0);
		Posicion pos2 = new Posicion(1,0,0);
		
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		posiciones.add(pos1);
		posiciones.add(pos2);
		
		Rocosa rocosa = new Rocosa();
		tablero.establecerSuperficie(posiciones, rocosa);
		
		tablero.agregar(optimusPrime, pos1);
		
		
		Flash bonus = new Flash();
		tablero.agregarBonus(bonus, pos2);
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(optimusPrime, tablero);
		
		int velocidad = optimusPrime.obtenerVelocidad();
		velocidad = velocidad*3; //Triplico la velocidad para corroborar
		
		recorrido.avanzar(optimusPrime, pos2, tablero);
		
		Assert.assertTrue(optimusPrime.obtenerPosicionEnTablero().equals(pos2));
		
		Assert.assertTrue(optimusPrime.obtenerVelocidad() == velocidad);
		
	}
	
	@Test
	public void test06flashTriplicaVelocidadModoAlterno(){
		
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		
		Posicion pos1 = new Posicion(0,0,0);
		Posicion pos2 = new Posicion(1,0,0);
		Posicion pos3 = new Posicion(2,0,0);
		
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		posiciones.add(pos1);
		posiciones.add(pos2);
		posiciones.add(pos3);
		
		Rocosa rocosa = new Rocosa();
		tablero.establecerSuperficie(posiciones, rocosa);
		
		tablero.agregar(optimusPrime, pos1);
		
		
		Flash bonus = new Flash();
		tablero.agregarBonus(bonus, pos2);
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(optimusPrime, tablero);
		
		int velocidad = optimusPrime.obtenerVelocidad();
		velocidad = velocidad*3;
		recorrido.avanzar(optimusPrime, pos2, tablero); //agarra el bonus
		recorrido.avanzar(optimusPrime, pos3, tablero);
		
		Assert.assertTrue(optimusPrime.obtenerPosicionEnTablero().equals(pos3));
		
		Assert.assertTrue(optimusPrime.obtenerVelocidad() == velocidad);
		optimusPrime.descontarTurno();
		
		Assert.assertTrue(optimusPrime.obtenerVelocidad() == velocidad);
		optimusPrime.descontarTurno();
		
		Assert.assertTrue(optimusPrime.obtenerVelocidad() == velocidad);
		optimusPrime.descontarTurno();
		
		//Bonus desactivado
		
		Assert.assertFalse(optimusPrime.obtenerVelocidad() == velocidad); 
		optimusPrime.descontarTurno();
	}
	
	@Test
	public void test07flashPersisteEnTransformacion(){
		
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		
		Posicion pos1 = new Posicion(0,0,0);
		Posicion pos2 = new Posicion(1,0,0);
		Posicion pos3 = new Posicion(2,0,0);
		
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		posiciones.add(pos1);
		posiciones.add(pos2);
		posiciones.add(pos3);
		
		Rocosa rocosa = new Rocosa();
		tablero.establecerSuperficie(posiciones, rocosa);
		
		tablero.agregar(optimusPrime, pos1);
		
		
		Flash bonus = new Flash();
		tablero.agregarBonus(bonus, pos2);
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(optimusPrime, tablero);
		
		int velocidadHumanAux = optimusPrime.obtenerVelocidad();
		velocidadHumanAux = velocidadHumanAux*3;
		
		optimusPrime.transformar(tablero);
		int velocidadAltAux = optimusPrime.obtenerVelocidad();
		velocidadAltAux = velocidadAltAux*3;
		
		optimusPrime.transformar(tablero);
		
		recorrido.avanzar(optimusPrime, pos2, tablero); //agarra el bonus
	
		
		Assert.assertTrue(optimusPrime.obtenerVelocidad() == velocidadHumanAux);
		optimusPrime.transformar(tablero);
		
		Assert.assertTrue(optimusPrime.obtenerVelocidad() == velocidadAltAux);
		
	}
	
	@Test
	public void test08noSePuedeAgarrarBonusDelMismoTipo(){
		
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		
		Posicion pos1 = new Posicion(0,0,0);
		Posicion pos2 = new Posicion(1,0,0);
		Posicion pos3 = new Posicion(2,0,0);
		
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		posiciones.add(pos1);
		posiciones.add(pos2);
		posiciones.add(pos3);
		
		Rocosa rocosa = new Rocosa();
		tablero.establecerSuperficie(posiciones, rocosa);
		
		tablero.agregar(optimusPrime, pos1);
		
		
		DobleCanion bonus = new DobleCanion();
		tablero.agregarBonus(bonus, pos2);
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(optimusPrime, tablero);
		
		double ataque = optimusPrime.obtenerAtaque();
		ataque = ataque*2;
		recorrido.avanzar(optimusPrime, pos2, tablero);
		recorrido.avanzar(optimusPrime, pos3, tablero);
		
		Assert.assertTrue(optimusPrime.obtenerPosicionEnTablero().equals(pos3));
		
		Assert.assertTrue(optimusPrime.obtenerAtaque() == ataque);
		
		tablero.agregarBonus(bonus, pos2);
		
		Recorrido recorrido2 = new Recorrido();
		recorrido2.determinarRecorridoPosibleSegunVelocidad(optimusPrime, tablero);
		

		recorrido2.avanzar(optimusPrime, pos2, tablero); //pasa por otro bonus
		
		Assert.assertTrue(optimusPrime.obtenerPosicionEnTablero().equals(pos2));
		
		Assert.assertTrue(optimusPrime.obtenerAtaque() == ataque);
	}
	
	@Test
	public void test09agarrar2BonusDistintosYVerificarAmbosComportamientos(){
		
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		
		Posicion pos1 = new Posicion(0,0,0);
		Posicion pos2 = new Posicion(1,0,0);
		Posicion pos3 = new Posicion(2,0,0);
		
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		posiciones.add(pos1);
		posiciones.add(pos2);
		posiciones.add(pos3);
		
		Rocosa rocosa = new Rocosa();
		tablero.establecerSuperficie(posiciones, rocosa);
		
		tablero.agregar(optimusPrime, pos1);
		
		
		DobleCanion bonus = new DobleCanion();
		tablero.agregarBonus(bonus, pos2);
		
		Flash bonus2 = new Flash();
		tablero.agregarBonus(bonus2, pos3);
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(optimusPrime, tablero);
		
		double ataque = optimusPrime.obtenerAtaque();
		ataque = ataque*2;
		int velocidad = optimusPrime.obtenerVelocidad();
		velocidad = velocidad*3;
		
		
		recorrido.avanzar(optimusPrime, pos2, tablero);
		recorrido.avanzar(optimusPrime, pos3, tablero);
		
		Assert.assertTrue(optimusPrime.obtenerPosicionEnTablero().equals(pos3));
		
		Assert.assertTrue(optimusPrime.obtenerAtaque() == ataque);
		Assert.assertTrue(optimusPrime.obtenerVelocidad() == velocidad);
	}
	

}
