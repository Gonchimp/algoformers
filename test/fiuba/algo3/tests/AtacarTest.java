package fiuba.algo3.tests;

import org.junit.Assert;
import org.junit.Test;

import algoformers.Algoformer;
import algoformers.Bonecrusher;
import algoformers.Bumblebee;
import algoformers.Frenzy;
import algoformers.Megatron;
import algoformers.OptimusPrime;
import algoformers.Ratchet;
import bonus.Fusion;
import excepciones.AtaqueAlMismoEquipoException;
import excepciones.ObjetivoFueraDeRangoException;
import utilidadesDeJuego.Casillero;
import utilidadesDeJuego.Jugador;
import utilidadesDeJuego.Posicion;
import utilidadesDeJuego.Tablero;

public class AtacarTest {
	
	@Test
	public void test01atacarAutobotADecepticon(){
		Tablero tablero = new Tablero();
		OptimusPrime optimus = new OptimusPrime();
		Megatron megatron = new Megatron();
		Posicion pos1 = new Posicion (10,10,0);
		Posicion pos2 = new Posicion (10,11,0);
		
		tablero.agregar(optimus,pos1);
		tablero.agregar(megatron, pos2);
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos2));
		
		Assert.assertTrue(megatron.obtenerVida() == 550);	
		optimus.atacar(megatron, tablero);
		
		Assert.assertTrue(megatron.obtenerVida() == 500);
	}
	@Test
	public void test02atacarDecepticonAAutobot(){
		Tablero tablero = new Tablero();
		OptimusPrime optimus = new OptimusPrime();
		Megatron megatron = new Megatron();
		Posicion pos1 = new Posicion (10,10,0);
		Posicion pos2 = new Posicion (10,11,0);
		
		tablero.agregar(optimus, pos1);
		tablero.agregar(megatron, pos2);
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos2));
		
		Assert.assertTrue(optimus.obtenerVida() == 500);	
		megatron.atacar(optimus, tablero);
		
		Assert.assertTrue(optimus.obtenerVida() == 490);
		
	}
	@Test (expected = AtaqueAlMismoEquipoException.class)
	public void test03atacarAutobotAAutobot(){
		Tablero tablero = new Tablero();
		OptimusPrime optimus = new OptimusPrime();
		Bumblebee bumblebee = new Bumblebee();
		Posicion pos1 = new Posicion (10,10,0);
		Posicion pos2 = new Posicion (10,11,0);
		
		tablero.agregar(optimus, pos1);
		tablero.agregar(bumblebee, pos2);
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos2));
		
		Assert.assertTrue(optimus.obtenerVida() == 500);	
		bumblebee.atacar(optimus, tablero);
		
		Assert.assertTrue(optimus.obtenerVida() == 500);
		
	}
	@Test (expected = AtaqueAlMismoEquipoException.class)
	public void test04atacarDecepticonADecepticon(){
		
		Tablero tablero = new Tablero();
		Megatron megatron = new Megatron();
		Bonecrusher bonecrusher = new Bonecrusher();
		Posicion pos1 = new Posicion (10,10,0);
		Posicion pos2 = new Posicion (10,11,0);
		
		tablero.agregar(megatron, pos1);
		tablero.agregar(bonecrusher, pos2);
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos2));
		
		Assert.assertTrue(bonecrusher.obtenerVida() == 200);	
		megatron.atacar(bonecrusher, tablero);
		
	}

	@Test (expected = ObjetivoFueraDeRangoException.class)
	public void test05atacarFueraDeRangoLanzaExcepcion(){
		Tablero tablero = new Tablero();
		OptimusPrime optimus = new OptimusPrime();
		Megatron megatron = new Megatron();
		Posicion pos1 = new Posicion (10,10,0);
		Posicion pos2 = new Posicion (20,20,0);
		
		tablero.agregar(optimus, pos1);
		tablero.agregar(megatron, pos2);
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos2));
		
		optimus.atacar(megatron, tablero);
	
	}
	
	@Test
	public void test06SuperionAtacaAMenasor(){
		Tablero tablero = new Tablero();
		OptimusPrime optimus = new OptimusPrime();
		Bumblebee bumblebee= new Bumblebee();
		Ratchet ratchet= new Ratchet();
		Posicion posOptimus = new Posicion (13,13,0);
		Posicion posBumblebee = new Posicion (15,14,0);
		Posicion posRatchet = new Posicion (13,15,0);
		
		Megatron megatron = new Megatron();
		Frenzy frenzy= new Frenzy();
		Bonecrusher bonecrusher= new Bonecrusher();
		Posicion posMegatron= new Posicion (13,12,0);
		Posicion posBonecrusher= new Posicion (12,11,0);
		Posicion posFrenzy = new Posicion (11,10,0);
		
		tablero.agregar(optimus, posOptimus);
		tablero.agregar(bumblebee, posBumblebee);
		tablero.agregar(ratchet, posRatchet);
		tablero.agregar(megatron, posMegatron);
		tablero.agregar(frenzy, posFrenzy);
		tablero.agregar(bonecrusher, posBonecrusher);
		
		Jugador jugador1=new Jugador(tablero,"Autobots");
		jugador1.agregarAEquipo(optimus);
		jugador1.agregarAEquipo(bumblebee);
		jugador1.agregarAEquipo(ratchet);
		
		
		Jugador jugador2=new Jugador(tablero,"Decepticons");
		jugador2.agregarAEquipo(megatron);
		jugador2.agregarAEquipo(frenzy);
		jugador2.agregarAEquipo(bonecrusher);
		
		Fusion fusion1=new Fusion(jugador1);
		Fusion fusion2=new Fusion(jugador2);
		
		optimus.combinarConAlgoformers(bumblebee, ratchet, tablero,fusion1);
		megatron.combinarConAlgoformers(frenzy, bonecrusher, tablero,fusion2);
		
		optimus.descontarTurno();
		optimus.descontarTurno();
		
		megatron.descontarTurno();
		megatron.descontarTurno();
		
		Casillero casillero1= tablero.getCasillero(jugador1.obtenerPosicion("Superion"));
		Algoformer superion=casillero1.obtenerAlgoformer();
		
		Casillero casillero2= tablero.getCasillero(jugador2.obtenerPosicion("Menasor"));
		Algoformer menasor=casillero2.obtenerAlgoformer();

		Assert.assertEquals((int)1000,(int)superion.obtenerVida());
		Assert.assertEquals((int)1150,(int)menasor.obtenerVida());
		
		superion.atacar(menasor,tablero);
		
		Assert.assertEquals((int)1000,(int)superion.obtenerVida());
		Assert.assertEquals((int)1050,(int)menasor.obtenerVida());
		
	}
	
	@Test
	public void test07MenasorAtacaSuperion(){
		Tablero tablero = new Tablero();
		OptimusPrime optimus = new OptimusPrime();
		Bumblebee bumblebee= new Bumblebee();
		Ratchet ratchet= new Ratchet();
		Posicion posOptimus = new Posicion (13,13,0);
		Posicion posBumblebee = new Posicion (15,14,0);
		Posicion posRatchet = new Posicion (13,15,0);
		
		Megatron megatron = new Megatron();
		Frenzy frenzy= new Frenzy();
		Bonecrusher bonecrusher= new Bonecrusher();
		Posicion posMegatron= new Posicion (13,12,0);
		Posicion posBonecrusher= new Posicion (12,11,0);
		Posicion posFrenzy = new Posicion (11,10,0);
				
		tablero.agregar(optimus, posOptimus);
		tablero.agregar(bumblebee, posBumblebee);
		tablero.agregar(ratchet, posRatchet);
		tablero.agregar(megatron, posMegatron);
		tablero.agregar(frenzy, posFrenzy);
		tablero.agregar(bonecrusher, posBonecrusher);
		
		Jugador jugador1=new Jugador(tablero,"Autobots");
		jugador1.agregarAEquipo(optimus);
		jugador1.agregarAEquipo(bumblebee);
		jugador1.agregarAEquipo(ratchet);
		
		
		Jugador jugador2=new Jugador(tablero,"Decepticons");
		jugador2.agregarAEquipo(megatron);
		jugador2.agregarAEquipo(frenzy);
		jugador2.agregarAEquipo(bonecrusher);
			
		Fusion fusion1=new Fusion(jugador1);
		Fusion fusion2=new Fusion(jugador2);
		
		optimus.combinarConAlgoformers(bumblebee, ratchet, tablero,fusion1);
		megatron.combinarConAlgoformers(frenzy, bonecrusher, tablero,fusion2);
		
		optimus.descontarTurno();
		optimus.descontarTurno();
		megatron.descontarTurno();
		megatron.descontarTurno();
		
		Casillero casillero1= tablero.getCasillero(jugador1.obtenerPosicion("Superion"));
		Algoformer superion=casillero1.obtenerAlgoformer();
		
		Casillero casillero2= tablero.getCasillero(jugador2.obtenerPosicion("Menasor"));
		Algoformer menasor=casillero2.obtenerAlgoformer();
		
		Assert.assertEquals((int)1000,(int)superion.obtenerVida());
		Assert.assertEquals((int)1150,(int)menasor.obtenerVida());
		
		menasor.atacar(superion,tablero);
		
		Assert.assertEquals((int)885,(int)superion.obtenerVida());
		Assert.assertEquals((int)1150,(int)menasor.obtenerVida());
		
	}
	
	
}
