package fiuba.algo3.tests;

import org.junit.Assert;
import org.junit.Test;

import algoformers.Ratchet;
import utilidadesDeJuego.Casillero;
import utilidadesDeJuego.Chispa;

public class CasilleroTest {
	
	@Test
	public void test01CrearCasilleroDebeInicializarVacio(){
		Casillero casillero = new Casillero();
		Assert.assertFalse(casillero.estaOcupado());
	}
	
	@Test
	public void test02AgregarUnAgregableAlCasilleroDebeOcuparlo(){
		Casillero casillero = new Casillero();
		Ratchet ratchet = new Ratchet();
		casillero.agregarAlgoformer(ratchet);
		Assert.assertTrue(casillero.estaOcupado());
	}
	

	@Test
	public void test03AgregarUnaChispaAlCasilleroNoDebeOcuparlo(){
		Casillero casillero = new Casillero();
		Chispa chispa= new Chispa();
		casillero.agregarChispa(chispa);
		Assert.assertFalse(casillero.estaOcupado());
	}

}
