package fiuba.algo3.tests;

import org.junit.Assert;
import org.junit.Test;

import algoformers.ModoAlternoTerrestre;
import algoformers.ModoHumanoide;
import modificadores.Modificadores;

public class ModoTest {

	 @Test
	 public void test01CrearUnModoHumanoide(){
		 ModoHumanoide modoHumanoide = new ModoHumanoide(50,2,2); //Ataque,DistanciaDeAtaque,Velocidad.
		 
		 Modificadores modificador = new Modificadores();
		 Assert.assertTrue(modoHumanoide.obtenerAtaque(modificador) == 50);
		 Assert.assertTrue(modoHumanoide.obtenerDistanciaDeAtaque() == 2);
		 Assert.assertEquals(modoHumanoide.obtenerVelocidad(modificador),2);
	 }
	 
	 @Test
	 public void test02CrearUnModoAlterno(){
		 Modificadores modificador = new Modificadores();
		 
		 ModoAlternoTerrestre modoAlterno = new ModoAlternoTerrestre(15,4,5); //Ataque,DistanciaDeAtaque,Velocidad.
		 Assert.assertTrue(modoAlterno.obtenerAtaque(modificador) == 15);
		 Assert.assertEquals(modoAlterno.obtenerDistanciaDeAtaque(),4);
		 Assert.assertEquals(modoAlterno.obtenerVelocidad(modificador),5);
	 }
}
