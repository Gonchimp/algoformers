package fiuba.algo3.tests;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;
import algoformers.Bonecrusher;
import algoformers.Bumblebee;
import algoformers.Frenzy;
import algoformers.Megatron;
import algoformers.OptimusPrime;
import algoformers.Ratchet;
import bonus.BonusNulo;
import excepciones.ObjetivoFueraDeRangoException;
import superficies.Nube;
import superficies.Rocosa;
import utilidadesDeJuego.Posicion;
import utilidadesDeJuego.Recorrido;
import utilidadesDeJuego.Tablero;

public class Entrega1Test {
	
	@Test
	public void test01UbicarAlgoformerHumanoideMoverloYVerificarAcordeAlModo(){
		Tablero tablero = new Tablero();//Creamos nuevo tablero
		Recorrido recorrido = new Recorrido();
		Bumblebee bumblebee = new Bumblebee();//Creamos a un Bumblebeee
		Posicion pos1 = new Posicion(18,18,0);
		Posicion pos2 = new Posicion(19,19,0);
		
		ArrayList<Posicion> rocosas = new ArrayList<Posicion>();
		rocosas.add(pos1);
		rocosas.add(pos2);
		tablero.establecerSuperficie(rocosas, new Rocosa());
		tablero.agregarBonus(new BonusNulo(), pos1);
		tablero.agregarBonus(new BonusNulo(), pos2);
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//No hay nada en (8,8)
		
		tablero.agregar(bumblebee,pos1);//Lo ubicamos en la posicion (8,8); Esta en Modo Humanoide
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		recorrido.determinarRecorridoPosibleSegunVelocidad(bumblebee, tablero);
		
		recorrido.avanzar(bumblebee, pos2, tablero);//Bumblebee tiene una velocidad de dos, puede moverse al (9,9)
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//Bumblebee se fue de la posicion (8,8)
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos2));//Bumblebee esta en la posicion (9,9)
	}
	@Test
	public void test02UbicarAlgoformerMoverloYVerificarAcordeAlModo(){
		Tablero tablero = new Tablero();//Creamos nuevo tablero

		Posicion pos1 = new Posicion(8,8,0);
		Posicion pos2 = new Posicion(9,8,0);
		
		ArrayList<Posicion> rocosas = new ArrayList<Posicion>();
		rocosas.add(pos1);
		rocosas.add(pos2);
		tablero.establecerSuperficie(rocosas, new Rocosa());
		tablero.agregarBonus(new BonusNulo(), pos1);
		tablero.agregarBonus(new BonusNulo(), pos2);
		
		Bonecrusher bonecrusher = new Bonecrusher();//Creamos a un Bonecrusher
		Recorrido recorrido = new Recorrido();
		
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//No hay nada en (8,8)
		
		tablero.agregar(bonecrusher,pos1);//Lo ubicamos en la posicion (8,8); Esta en Modo Humanoide
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		recorrido.determinarRecorridoPosibleSegunVelocidad(bonecrusher, tablero);
		
		recorrido.avanzar(bonecrusher, pos2, tablero);//Bonecrusher tiene una velocidad de uno, puede moverse al (9,8)
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//Bonecrusher se fue de la posicion (8,8)
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos2));//Bonecrusher esta en la posicion (9,8)
	}
	@Test
	public void test03UbicarAlgoformerMoverloYVerificarAcordeAlModo(){
		Tablero tablero = new Tablero();//Creamos nuevo tablero
		Recorrido recorrido = new Recorrido();
		Frenzy frenzy = new Frenzy();//Creamos a un Frenzy
		Posicion pos1 = new Posicion(1,1,0);
		Posicion pos2 = new Posicion(2,2,0);
		
		ArrayList<Posicion> rocosa = new ArrayList<Posicion>();
		rocosa.add(pos1);
		rocosa.add(pos2);
		
		tablero.establecerSuperficie(rocosa, new Rocosa());
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//No hay nada en (1,1)
		
		tablero.agregar(frenzy,pos1);//Lo ubicamos en la posicion (1,1); Esta en Modo Humanoide
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		recorrido.determinarRecorridoPosibleSegunVelocidad(frenzy, tablero);
		
		recorrido.avanzar(frenzy, pos2, tablero);//Frenzy tiene una velocidad de dos, puede moverse al (2,2)
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//Frenzy se fue de la posicion (1,1)
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos2));//Frenzy esta en la posicion (2,2)
	}
	@Test
	public void test04UbicarAlgoformerMoverloYVerificarAcordeAlModo(){
		Tablero tablero = new Tablero();//Creamos nuevo tablero
		Recorrido recorrido = new Recorrido();
		Megatron megatron = new Megatron();//Creamos a un Megatron
		Posicion pos1 = new Posicion(11,11,0);
		Posicion pos2 = new Posicion(11,12,0);
		
		ArrayList<Posicion> rocosa = new ArrayList<Posicion>();
		rocosa.add(pos1);
		rocosa.add(pos2);
		
		tablero.establecerSuperficie(rocosa, new Rocosa());
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//No hay nada en (1,1)
		
		tablero.agregar(megatron,pos1);//Lo ubicamos en la posicion (1,1); Esta en Modo Humanoide
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		recorrido.determinarRecorridoPosibleSegunVelocidad(megatron, tablero);
		
		recorrido.avanzar(megatron, pos2, tablero);//Magtron tiene una velocidad de uno, puede moverse al (1,2)
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//Frenzy se fue de la posicion (1,1)
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos2));//Frenzy esta en la posicion (1,2)
	}
	
	@Test
	public void test05UbicarAlgoformerMoverloYVerificarAcordeAlModo(){
		Tablero tablero = new Tablero();//Creamos nuevo tablero
		Recorrido recorrido = new Recorrido();
		OptimusPrime optimusPrime = new OptimusPrime();//Creamos a un OptimusPrime
		Posicion pos1 = new Posicion(1,1,0);
		Posicion pos2 = new Posicion(2,2,0);
		
		ArrayList<Posicion> rocosa = new ArrayList<Posicion>();
		rocosa.add(pos1);
		rocosa.add(pos2);
		
		tablero.establecerSuperficie(rocosa, new Rocosa());
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//No hay nada en (1,1)
		
		tablero.agregar(optimusPrime,pos1);//Lo ubicamos en la posicion (1,1); Esta en Modo Humanoide
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		recorrido.determinarRecorridoPosibleSegunVelocidad(optimusPrime, tablero);
		
		recorrido.avanzar(optimusPrime, pos2, tablero);//OptimusPrime tiene una velocidad de dos, puede moverse al (2,2)
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//OptimusPrime se fue de la posicion (1,1)
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos2));//OptimusPrime esta en la posicion (2,2)
	}
	@Test
	public void test06UbicarAlgoformerMoverloYVerificarAcordeAlModo(){
		Tablero tablero = new Tablero();//Creamos nuevo tablero
		Recorrido recorrido = new Recorrido();
		Ratchet ratchet = new Ratchet();//Creamos a un Ratchet
		Posicion pos1 = new Posicion(11,11,0);
		Posicion pos2 = new Posicion(11,12,0);
		
		ArrayList<Posicion> rocosa = new ArrayList<Posicion>();
		rocosa.add(pos1);
		rocosa.add(pos2);
		
		tablero.establecerSuperficie(rocosa, new Rocosa());
		
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//No hay nada en (1,1)
		
		tablero.agregar(ratchet,pos1);//Lo ubicamos en la posicion (1,1); Esta en Modo Humanoide
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		recorrido.determinarRecorridoPosibleSegunVelocidad(ratchet, tablero);
		
		recorrido.avanzar(ratchet, pos2, tablero);//ratchet tiene una velocidad de uno, puede moverse al (1,2)
				
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//ratchet se fue de la posicion (1,1)
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos2));//ratchet esta en la posicion (1,2)
	}
	
	@Test
	public void test07UbicarAlgoformerEnModoAlternoMoverloYVerificarAcordeAlModo(){
		Tablero tablero = new Tablero();//Creamos nuevo tablero
		Recorrido recorrido = new Recorrido();
		Bumblebee bumblebee = new Bumblebee();//Creamos a un Bumblebeee
		Posicion pos1 = new Posicion(8,8,0);
		Posicion pos2 = new Posicion(9,9,0);
		
		ArrayList<Posicion> rocosa = new ArrayList<Posicion>();
		rocosa.add(pos1);
		rocosa.add(pos2);
		
		tablero.establecerSuperficie(rocosa, new Rocosa());
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//No hay nada en (8,8)
		
		tablero.agregar(bumblebee,pos1);//Lo ubicamos en la posicion (8,8); Esta en Modo Humanoide
		
		bumblebee.transformar(tablero);
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		recorrido.determinarRecorridoPosibleSegunVelocidad(bumblebee, tablero);
		
		recorrido.avanzar(bumblebee, pos2, tablero);//Bumblebee tiene una velocidad de dos, puede moverse al (9,9)
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//Bumblebee se fue de la posicion (8,8)
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos2));//Bumblebee esta en la posicion (9,9)
	}
	
	@Test
	public void test08UbicarAlgoformerModoAlternoMoverloYVerificarAcordeAlModo(){
		Tablero tablero = new Tablero();//Creamos nuevo tablero
		Recorrido recorrido = new Recorrido();
		Bonecrusher bonecrusher = new Bonecrusher();//Creamos a un Bonecrusher
		Posicion pos1 = new Posicion(8,8,0);
		Posicion pos2 = new Posicion(9,8,0);
		
		ArrayList<Posicion> rocosa = new ArrayList<Posicion>();
		rocosa.add(pos1);
		rocosa.add(pos2);
		
		tablero.establecerSuperficie(rocosa, new Rocosa());
				
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//No hay nada en (8,8)
		
		tablero.agregar(bonecrusher,pos1);//Lo ubicamos en la posicion (8,8); Esta en Modo Humanoide
		
		bonecrusher.transformar(tablero);
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		recorrido.determinarRecorridoPosibleSegunVelocidad(bonecrusher, tablero);
		
		recorrido.avanzar(bonecrusher, pos2, tablero);//Bonecrusher tiene una velocidad de uno, puede moverse al (9,8)
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//Bonecrusher se fue de la posicion (8,8)
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos2));//Bonecrusher esta en la posicion (9,8)
	}
	
	@Test
	public void test09UbicarAlgoformerModoAlternoMoverloYVerificarAcordeAlModo(){
		Tablero tablero = new Tablero();//Creamos nuevo tablero
		Recorrido recorrido = new Recorrido();
		Frenzy frenzy = new Frenzy();//Creamos a un Frenzy
		Posicion pos1 = new Posicion(1,1,0);
		Posicion pos2 = new Posicion(2,2,0);
		
		ArrayList<Posicion> rocosa = new ArrayList<Posicion>();
		rocosa.add(pos1);
		rocosa.add(pos2);
		
		tablero.establecerSuperficie(rocosa, new Rocosa());
		
		
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//No hay nada en (1,1)
		
		tablero.agregar(frenzy,pos1);//Lo ubicamos en la posicion (1,1); Esta en Modo Humanoide
		
		frenzy.transformar(tablero);
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		recorrido.determinarRecorridoPosibleSegunVelocidad(frenzy, tablero);
		
		recorrido.avanzar(frenzy, pos2, tablero);//Frenzy tiene una velocidad de dos, puede moverse al (2,2)
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//Frenzy se fue de la posicion (1,1)
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos2));//Frenzy esta en la posicion (2,2)
	}
	
	@Test
	public void test10UbicarAlgoformerModoAlternoMoverloYVerificarAcordeAlModo(){
		Tablero tablero = new Tablero();//Creamos nuevo tablero
		
		Posicion pos1 = new Posicion(11,11,0);
		Posicion pos2 = new Posicion(11,11,1);

		Posicion pos3 = new Posicion (12,11,1);
		Posicion pos4 = new Posicion (12,12,1);
		
		ArrayList<Posicion> nubes = new ArrayList<Posicion>();
		nubes.add(pos2);
		nubes.add(pos3);
		nubes.add(pos4);
		tablero.establecerSuperficie(nubes, new Nube());
		
		Recorrido recorrido = new Recorrido();
		Megatron megatron = new Megatron();//Creamos a un Megatron
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//No hay nada en (1,1)
		
		tablero.agregar(megatron,pos1);//Lo ubicamos en la posicion (1,1); Esta en Modo Humanoide
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));		
		megatron.transformar(tablero);//Se movio a la posicion 2
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos2));
		recorrido.determinarRecorridoPosibleSegunVelocidad(megatron, tablero);
		
		recorrido.avanzar(megatron, pos3, tablero);
		recorrido.avanzar(megatron, pos4, tablero);
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos2));
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos3));
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos4));
	}
	@Test
	public void test11UbicarAlgoformerModoAlternoMoverloYVerificarAcordeAlModo(){
		Tablero tablero = new Tablero();//Creamos nuevo tablero
		Recorrido recorrido = new Recorrido();
		OptimusPrime optimusPrime = new OptimusPrime();//Creamos a un OptimusPrime
		Posicion pos1 = new Posicion(1,1,0);
		Posicion pos2 = new Posicion(2,2,0);
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//No hay nada en (1,1)
		
		tablero.agregar(optimusPrime,pos1);//Lo ubicamos en la posicion (1,1); Esta en Modo Humanoide
		
		optimusPrime.transformar(tablero);

		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		recorrido.determinarRecorridoPosibleSegunVelocidad(optimusPrime, tablero);
		
		recorrido.avanzar(optimusPrime, pos2, tablero);//OptimusPrime tiene una velocidad de dos, puede moverse al (2,2)
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//OptimusPrime se fue de la posicion (1,1)
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos2));//OptimusPrime esta en la posicion (2,2)
	}
	
	@Test
	public void test12UbicarAlgoformerModoAlternoMoverloYVerificarAcordeAlModo(){
		Tablero tablero = new Tablero();//Creamos nuevo tablero
		Recorrido recorrido = new Recorrido();
		Ratchet ratchet = new Ratchet();//Creamos a un Ratchet
		Posicion pos1 = new Posicion(11,11,0);
		Posicion pos2 = new Posicion(11,11,1);
		Posicion pos3 = new Posicion (12,11,1);
		Posicion pos4 = new Posicion (12,12,1);
		
		ArrayList<Posicion> nubes = new ArrayList<Posicion>();
		nubes.add(pos2);
		nubes.add(pos3);
		nubes.add(pos4);
		
		tablero.establecerSuperficie(nubes, new Nube());
		
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//No hay nada en (1,1)
		
		tablero.agregar(ratchet,pos1);//Lo ubicamos en la posicion (1,1); Esta en Modo Humanoide
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		ratchet.transformar(tablero);//Se movio a pos2.

		Assert.assertTrue(tablero.casilleroEstaOcupado(pos2));
	
		recorrido.determinarRecorridoPosibleSegunVelocidad(ratchet, tablero);
		
		recorrido.avanzar(ratchet, pos3, tablero);//ratchet tiene una velocidad de uno, puede moverse al (1,2)
		recorrido.avanzar(ratchet, pos4, tablero);
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//ratchet se fue de la posicion (1,1)
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos2));//ratchet esta en la posicion (1,2)
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos3));
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos4));
	}
	
	@Test
	public void test13ubicarAlgoFormerHumanoideTransformarYverificarTransformacionEnAmbasDireccione(){// Se crea Automaticamente en Modo Humanoide
		Tablero tablero = new Tablero();//Creamos nuevo tablero
		Recorrido recorrido = new Recorrido();
		Bumblebee bumblebee = new Bumblebee();
		Posicion pos1 = new Posicion(18,18,0);
		Posicion pos2 = new Posicion(19,19,0);
		
		ArrayList<Posicion> rocosas = new ArrayList<Posicion>();
		rocosas.add(pos1);
		rocosas.add(pos2);
		tablero.establecerSuperficie(rocosas, new Rocosa());
		
		tablero.agregarBonus(new BonusNulo(), pos1);
		tablero.agregarBonus(new BonusNulo(), pos2);
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//No hay nada en (1,1)
        Assert.assertFalse(tablero.casilleroEstaOcupado(pos2));//No hay nada en (8,8)
		
		tablero.agregar(bumblebee,pos1);//Lo ubicamos en la posicion (8,8); Esta en Modo Humanoide
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		recorrido.determinarRecorridoPosibleSegunVelocidad(bumblebee,  tablero);
		
		recorrido.avanzar(bumblebee, pos2, tablero);//Bumblebee tiene una velocidad de dos, puede moverse al (9,9)
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));//Bumblebee se fue de la posicion (8,8)
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos2));//Bumblebee esta en la posicion (9,9)  
		bumblebee.transformar(tablero);
		bumblebee.transformar(tablero);
		Assert.assertTrue(bumblebee.obtenerVida() == 350);
		Assert.assertTrue(bumblebee.obtenerAtaque() == 40);
		Assert.assertEquals(bumblebee.obtenerDistanciaDeAtaque(),1);
		Assert.assertEquals(bumblebee.obtenerVelocidad(),2);
	}
	
	@Test(expected = ObjetivoFueraDeRangoException.class)
	public void test14AutobotHumanoideAtacaADecepticonConDistanciaMayorALaDebida(){
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		Megatron megatron = new Megatron();
		Posicion pos1 = new Posicion (5,5,0);
		Posicion pos2 = new Posicion (5,9,0);
		
		tablero.agregar(optimusPrime,pos1);
		tablero.agregar(megatron, pos2);
		
		Assert.assertTrue(optimusPrime.obtenerVida() == 500);
		Assert.assertTrue(megatron.obtenerVida() == 550);
		
		optimusPrime.atacar(megatron, tablero);//Optimus en Humanoide no alcanza a atacarlo
	}
	
	@Test(expected = ObjetivoFueraDeRangoException.class)
	public void test15AutobotAlternoAtacaADecepticonConDistanciaMayorALaDebida(){
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		Megatron megatron = new Megatron();
		Posicion pos1 = new Posicion (5,5,0);
		Posicion pos2 = new Posicion (5,10,0);
				
		tablero.agregar(optimusPrime,pos1);
		tablero.agregar(megatron, pos2);
		
		optimusPrime.transformar(tablero);
		
		Assert.assertTrue(optimusPrime.obtenerVida() == 500);
		Assert.assertTrue(megatron.obtenerVida() == 550);
		
		optimusPrime.atacar(megatron, tablero);//Optimus en Humanoide no alcanza a atacarlo
		
	}
	
	@Test(expected = ObjetivoFueraDeRangoException.class)
	public void test16DecepticonHumanoideAtacaAAutobotConDistanciaMayorALaDebida(){
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		Megatron megatron = new Megatron();
		Posicion pos1 = new Posicion (5,5,0);
		Posicion pos2 = new Posicion (5,9,0);
			
		tablero.agregar(optimusPrime,pos1);
		tablero.agregar(megatron, pos2);
		
		Assert.assertTrue(optimusPrime.obtenerVida() == 500);
		Assert.assertTrue(megatron.obtenerVida() == 550);
		
		megatron.atacar(optimusPrime, tablero);//Megatron en Humanoide no alcanza a atacarlo
		
	}
	
	@Test(expected = ObjetivoFueraDeRangoException.class)
	public void test17AutobotAlternoAtacaADecepticonConDistanciaMayorALaDebida(){
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		Megatron megatron = new Megatron();
		Posicion pos1 = new Posicion (5,5,0);
		Posicion pos2 = new Posicion (5,8,0);
		
		
		tablero.agregar(optimusPrime,pos1);
		tablero.agregar(megatron, pos2);
		
		megatron.transformar(tablero);
		
		Assert.assertTrue(optimusPrime.obtenerVida() == 500);
		Assert.assertTrue(megatron.obtenerVida() == 550);
		
		megatron.atacar(optimusPrime, tablero);//Megatron en Humanoide no alcanza a atacarlo
	}
	
	@Test
	public void test18AutobotHumanoideAtacaADecepticonyDecepticonHumanoideAtacaAAutobot(){
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		Megatron megatron = new Megatron();
		Posicion pos1 = new Posicion (5,5,0);
		Posicion pos2 = new Posicion (5,7,0);
		
		tablero.agregar(optimusPrime,pos1);
		tablero.agregar(megatron, pos2);
		
		Assert.assertTrue(optimusPrime.obtenerVida() == 500);
		Assert.assertTrue(megatron.obtenerVida() == 550);
		
		optimusPrime.atacar(megatron, tablero);
		megatron.atacar(optimusPrime, tablero);
		
		Assert.assertTrue(optimusPrime.obtenerVida() == 490);
		Assert.assertTrue(megatron.obtenerVida() == 500);
		
	}
	
	@Test
	public void test19AutobotAlternoAtacaADecepticon(){
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		Megatron megatron = new Megatron();
		Posicion pos1 = new Posicion (5,5,0);
		Posicion pos2 = new Posicion (5,9,0);
		
		tablero.agregar(optimusPrime,pos1);
		tablero.agregar(megatron, pos2);
		
		optimusPrime.transformar(tablero);
		
		Assert.assertTrue(optimusPrime.obtenerVida() == 500);
		Assert.assertTrue(megatron.obtenerVida() == 550);
		
		optimusPrime.atacar(megatron, tablero);
						
		Assert.assertTrue(optimusPrime.obtenerVida() == 500);
		Assert.assertTrue(megatron.obtenerVida() == 535);
		
	}
	
	@Test
	public void test20DecepticonAlternoAtacaAAutobot(){
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		Megatron megatron = new Megatron();
		Posicion pos1 = new Posicion (5,5,0);
		Posicion pos2 = new Posicion (5,7,0);
		Posicion pos3 = new Posicion (5,7,1);
		
		Nube superficie = new Nube();
		ArrayList<Posicion> nubes = new ArrayList<Posicion>();
		tablero.establecerSuperficie(nubes, superficie);
		
		tablero.agregar(optimusPrime,pos1);
		tablero.agregar(megatron, pos2);
		
		megatron.transformar(tablero);
		Assert.assertTrue(megatron.obtenerPosicionEnTablero().equals(pos3));
				
		Assert.assertTrue(optimusPrime.obtenerVida() == 500);
		Assert.assertTrue(megatron.obtenerVida() == 550);
		
		megatron.atacar(optimusPrime, tablero);
				
		Assert.assertTrue(optimusPrime.obtenerVida() < 500);
		Assert.assertTrue(megatron.obtenerVida() == 550);
		
	}
	
}