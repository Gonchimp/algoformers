package fiuba.algo3.tests;

import java.util.ArrayList;
import org.junit.Assert;
import org.junit.Test;
import algoformers.Megatron;
import algoformers.OptimusPrime;
import bonus.BonusNulo;
import superficies.Espinas;
import superficies.Nebulosa;
import superficies.Nube;
import superficies.Pantano;
import superficies.Rocosa;
import superficies.Superficie;
import superficies.Tormenta;
import utilidadesDeJuego.Posicion;
import utilidadesDeJuego.Recorrido;
import utilidadesDeJuego.Tablero;

public class SuperficieTest {
	
	@Test
	public void  test01algoformersAtraviesanSinProblemaZonaRocosa(){
		
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		
		Posicion pos1 = new Posicion(0,0,0);
		Posicion pos2 = new Posicion(1,0,0);
		Posicion pos3 = new Posicion(2,0,0);
		
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		posiciones.add(pos1);
		posiciones.add(pos2);
		posiciones.add(pos3);
		
		Rocosa rocosa = new Rocosa();
		tablero.establecerSuperficie(posiciones, rocosa);
		
		tablero.agregar(optimusPrime, pos1);
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(optimusPrime, tablero);
		
		recorrido.avanzar(optimusPrime, pos2, tablero);
		recorrido.avanzar(optimusPrime, pos3, tablero);
				
		Assert.assertTrue(optimusPrime.obtenerPosicionEnTablero().equals(pos3));
		
		
		
		
	}
	
	@Test
	public void test02humanoideNoPuedeAtravesarPantano(){
		
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		
		Posicion pos1 = new Posicion(0,0,0);
		Posicion pos2 = new Posicion(1,0,0);
		Posicion pos3 = new Posicion(2,0,0);
				
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		
		posiciones.add(pos2);
		posiciones.add(pos3);
		
		Superficie pantano = new Pantano();
		tablero.establecerSuperficie(posiciones, pantano);
		
		tablero.agregar(optimusPrime, pos1);
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(optimusPrime, tablero);
		
		recorrido.avanzar(optimusPrime, pos2, tablero);
		recorrido.avanzar(optimusPrime, pos3, tablero);
		
		Assert.assertTrue(optimusPrime.obtenerPosicionEnTablero().equals(pos1));
		
	}
	
	@Test
	public void test03alternoTardaDobleEnPantano(){
		
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		
		Posicion pos1 = new Posicion(0,0,0);
		Posicion pos2 = new Posicion(1,0,0);
		Posicion pos3 = new Posicion(2,0,0);
		Posicion pos4 = new Posicion(3,0,0);
		Posicion pos5 = new Posicion(4,0,0);
		Posicion pos6 = new Posicion(5,0,0);
		
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		
		posiciones.add(pos2);
		posiciones.add(pos3);
		posiciones.add(pos4);
		posiciones.add(pos5);
		posiciones.add(pos6);
		
		Superficie pantano = new Pantano();
		tablero.establecerSuperficie(posiciones, pantano);
		
		tablero.agregar(optimusPrime, pos1);
		
		optimusPrime.transformar(tablero);
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(optimusPrime, tablero);
		recorrido.avanzar(optimusPrime, pos2, tablero);
		recorrido.avanzar(optimusPrime, pos3, tablero);
		recorrido.avanzar(optimusPrime, pos4, tablero);
		recorrido.avanzar(optimusPrime, pos5, tablero);
		recorrido.avanzar(optimusPrime, pos6, tablero);
				
		Assert.assertTrue(optimusPrime.obtenerPosicionEnTablero().equals(pos4));
	}
	
	@Test
	public void test04unidadesAereasAtraviesanSinProblemaPantano(){
		
		Tablero tablero = new Tablero();
		Megatron megatron = new Megatron();
		
		Posicion pos1 = new Posicion(0,0,0);
		Posicion pos2 = new Posicion(1,0,0);
		Posicion pos3 = new Posicion(2,0,0);
		Posicion pos1Aerea = new Posicion(0,0,1);
		Posicion pos4 = new Posicion(1,0,1);
		Posicion pos5 = new Posicion(2,0,1);
		Posicion pos6 = new Posicion(3,0,1);
		
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		posiciones.add(pos1);
		posiciones.add(pos2);
		posiciones.add(pos3);
		
		Superficie pantano = new Pantano();
		tablero.establecerSuperficie(posiciones, pantano);
		
		ArrayList<Posicion> nubes = new ArrayList<Posicion>();
		nubes.add(pos1Aerea);
		nubes.add(pos4);
		nubes.add(pos5);
		nubes.add(pos6);
		tablero.establecerSuperficie(nubes, new Nube());
		
		tablero.agregar(megatron, pos1);
		
		megatron.transformar(tablero);
		
		Assert.assertTrue(megatron.obtenerPosicionEnTablero().equals(pos1Aerea));
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(megatron, tablero);
		recorrido.avanzar(megatron, pos4, tablero);
		recorrido.avanzar(megatron, pos5, tablero);
		recorrido.avanzar(megatron, pos6, tablero);
		
		
		Assert.assertTrue(megatron.obtenerPosicionEnTablero().equals(pos6));
		
	}
	
	@Test
	public void test05zonaDeEspinasReduceLaVida(){
		
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		
		Posicion pos1 = new Posicion(0,0,0);
		Posicion pos2 = new Posicion(1,0,0);
		Posicion pos3 = new Posicion(2,0,0);
		
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		
		posiciones.add(pos2);
		posiciones.add(pos3);
		
		Superficie espinas = new Espinas();
		tablero.establecerSuperficie(posiciones, espinas);
		
		tablero.agregar(optimusPrime, pos1);
		
		double vidaAux = optimusPrime.obtenerVida();
		vidaAux = vidaAux*0.95; //resta 5%
		vidaAux =  vidaAux*0.95;	//resta 5%

		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(optimusPrime, tablero);
		recorrido.avanzar(optimusPrime, pos2, tablero);
		recorrido.avanzar(optimusPrime, pos3, tablero);

		Assert.assertTrue(optimusPrime.obtenerVida() == vidaAux);
		
	}
	@Test
	public void test06unidadesAereasNoSonAfectadasPorEspinas(){
		
		Tablero tablero = new Tablero();
		Megatron megatron = new Megatron();
		
		Posicion pos1 = new Posicion(0,0,0);
		Posicion pos2 = new Posicion(1,0,0);
		Posicion pos3 = new Posicion(2,0,0);
		Posicion pos4 = new Posicion(0,0,1);
		Posicion pos5 = new Posicion(1,0,1);
		Posicion pos6 = new Posicion(2,0,1);
		
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		posiciones.add(pos1);
		posiciones.add(pos2);
		posiciones.add(pos3);
		
		Superficie espinas = new Espinas();
		tablero.establecerSuperficie(posiciones, espinas);
		
		tablero.agregar(megatron, pos1);
		megatron.transformar(tablero);

		Assert.assertTrue(megatron.obtenerPosicionEnTablero().equals(pos4));
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(megatron, tablero);
		
		recorrido.avanzar(megatron, pos5, tablero);
		recorrido.avanzar(megatron, pos6, tablero);	
		
		Assert.assertTrue(megatron.obtenerVida() == 550);
		
	}
	
	@Test
	public void test07unidadesAereasAtraviesanNubesSinProblemas(){
		

		Tablero tablero = new Tablero();
		Megatron megatron = new Megatron();
		
		Posicion pos0 = new Posicion(0,0,0);
		Posicion pos1 = new Posicion(0,0,1);
		Posicion pos2 = new Posicion(1,0,1);
		Posicion pos3 = new Posicion(2,0,1);
		Posicion pos4 = new Posicion(1,0,1);
		Posicion pos5 = new Posicion(2,0,1);
		Posicion pos6 = new Posicion(1,0,1);
		
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		posiciones.add(pos1);
		posiciones.add(pos2);
		posiciones.add(pos3);
		posiciones.add(pos4);
		posiciones.add(pos5);
		posiciones.add(pos6);
		
		Superficie nube = new Nube();
		tablero.establecerSuperficie(posiciones, nube);
		
		tablero.agregar(megatron, pos0);
		
		megatron.transformar(tablero);
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(megatron, tablero);
		recorrido.avanzar(megatron, pos2, tablero);
		recorrido.avanzar(megatron, pos3, tablero);
		recorrido.avanzar(megatron, pos4, tablero);
		recorrido.avanzar(megatron, pos5, tablero);
		recorrido.avanzar(megatron, pos6, tablero);
		
		Assert.assertTrue(megatron.obtenerPosicionEnTablero().equals(pos6));
	
		
	}
	
	@Test
	public void test08nebulosaAtrapaPor3TurnosUnidadesAereas(){
		

		Tablero tablero = new Tablero();
		Megatron megatron = new Megatron();
		
		Posicion pos0 = new Posicion(0,0,0);
		Posicion pos1 = new Posicion(0,0,1);
		Posicion pos2 = new Posicion(1,0,1);
		Posicion pos3 = new Posicion(2,0,1);
		Posicion pos4 = new Posicion(1,0,1);
		Posicion pos5 = new Posicion(2,0,1);
		Posicion pos6 = new Posicion(1,0,1);
		
		ArrayList<Posicion> posicionesNube = new ArrayList<Posicion>();
		posicionesNube.add(pos1);
		posicionesNube.add(pos2);
		posicionesNube.add(pos3);
		ArrayList<Posicion> posicionesNebulosa = new ArrayList<Posicion>();
		posicionesNebulosa.add(pos4);
		posicionesNebulosa.add(pos5);
		posicionesNebulosa.add(pos6);
		
		tablero.agregarBonus(new BonusNulo(), pos0);
		tablero.agregarBonus(new BonusNulo(), pos1);
		tablero.agregarBonus(new BonusNulo(), pos2);
		tablero.agregarBonus(new BonusNulo(), pos3);
		tablero.agregarBonus(new BonusNulo(), pos4);
		tablero.agregarBonus(new BonusNulo(), pos5);
		tablero.agregarBonus(new BonusNulo(), pos6);
		
		
		tablero.establecerSuperficie(posicionesNube, new Nube());
		tablero.establecerSuperficie(posicionesNebulosa, new Nebulosa());
		
		tablero.agregar(megatron, pos0);
		
		megatron.transformar(tablero);
		
		Recorrido recorrido = new Recorrido();
		
		recorrido.determinarRecorridoPosibleSegunVelocidad(megatron, tablero);
		recorrido.avanzar(megatron, pos2, tablero);
		recorrido.avanzar(megatron, pos3, tablero);
		recorrido.avanzar(megatron, pos4, tablero);		
		
		Assert.assertTrue(megatron.obtenerPosicionEnTablero().equals(pos4)); //se metio en nebulosa
		
		Recorrido recorrido2 = new Recorrido();
		
		recorrido2.determinarRecorridoPosibleSegunVelocidad(megatron, tablero);
		recorrido.avanzar(megatron, pos5, tablero);	
		Assert.assertTrue(megatron.obtenerPosicionEnTablero().equals(pos4));
		megatron.descontarTurno();
		
		recorrido2.determinarRecorridoPosibleSegunVelocidad(megatron, tablero);
		recorrido2.avanzar(megatron, pos5, tablero);	
		Assert.assertTrue(megatron.obtenerPosicionEnTablero().equals(pos4));
		megatron.descontarTurno();

		recorrido2.determinarRecorridoPosibleSegunVelocidad(megatron, tablero);
		recorrido2.avanzar(megatron, pos5, tablero);	
		Assert.assertTrue(megatron.obtenerPosicionEnTablero().equals(pos4));
		megatron.descontarTurno();
		
		//pasaron los 3 turnos;vuelve a moverse
		recorrido2.determinarRecorridoPosibleSegunVelocidad(megatron, tablero);
		recorrido2.avanzar(megatron, pos5, tablero);	
		Assert.assertTrue(megatron.obtenerPosicionEnTablero().equals(pos5));
		megatron.descontarTurno();
		
	}
	
	@Test
	public void test09tormentaDebilitaParaSiempreAtaqueAereo(){
		Tablero tablero = new Tablero();
		Megatron megatron = new Megatron();
		Posicion pos0 = new Posicion(0,0,0);
		Posicion pos1 = new Posicion(0,0,1);
		Posicion pos2 = new Posicion(1,0,1);
		Posicion pos3 = new Posicion(2,0,1);
		
		ArrayList<Posicion> posicionNube = new ArrayList<Posicion>();
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		posicionNube.add(pos1);
		posiciones.add(pos2);
		posiciones.add(pos3);
		
		tablero.agregarBonus(new BonusNulo(), pos0);
		tablero.agregarBonus(new BonusNulo(), pos1);
		tablero.agregarBonus(new BonusNulo(), pos2);
		tablero.agregarBonus(new BonusNulo(), pos3);
		
		tablero.establecerSuperficie(posicionNube, new Nube());
		tablero.establecerSuperficie(posiciones, new Tormenta());
		
		tablero.agregar(megatron, pos0);
		
		megatron.transformar(tablero);
		double danioAux = megatron.obtenerAtaque();
		danioAux = (int) (danioAux*0.6); //resto 40%
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(megatron, tablero);
		
		recorrido.avanzar(megatron, pos2, tablero);	
		recorrido.avanzar(megatron, pos3, tablero);	

		Assert.assertTrue(megatron.obtenerAtaque() == danioAux);
		megatron.descontarTurno();
		megatron.descontarTurno();
		megatron.descontarTurno();
		megatron.descontarTurno();
		Assert.assertTrue(megatron.obtenerAtaque() == danioAux);
	}
	
	@Test
	public void test10tormentaNoVuelveADebilitarAtaqueAereo(){
		Tablero tablero = new Tablero();
		Megatron megatron = new Megatron();
		Posicion pos0 = new Posicion(0,0,0);
		Posicion pos1 = new Posicion(0,0,1);
		Posicion pos2 = new Posicion(1,0,1);
		Posicion pos3 = new Posicion(2,0,1);
		Posicion pos4 = new Posicion(3,0,1);
		
		ArrayList<Posicion> posicionNube = new ArrayList<Posicion>();
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		posicionNube.add(pos1);
		posicionNube.add(pos2);
		posiciones.add(pos2);
		posiciones.add(pos3);
		posiciones.add(pos4);

		tablero.establecerSuperficie(posicionNube, new Nube());
		tablero.establecerSuperficie(posiciones, new Tormenta());
		
		tablero.agregarBonus(new BonusNulo(), pos0);
		tablero.agregarBonus(new BonusNulo(), pos1);
		tablero.agregarBonus(new BonusNulo(), pos2);
		tablero.agregarBonus(new BonusNulo(), pos3);
		tablero.agregarBonus(new BonusNulo(), pos4);
		
		tablero.agregar(megatron, pos0);
		

		megatron.transformar(tablero);
		double danioAux = megatron.obtenerAtaque();
		danioAux = (int) (danioAux*0.6); //resto 40%
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(megatron, tablero);
		
		recorrido.avanzar(megatron, pos2, tablero);	
		recorrido.avanzar(megatron, pos3, tablero);	
		
		Assert.assertTrue(megatron.obtenerAtaque() == danioAux);
		
		
		
		Recorrido recorrido2 = new Recorrido();
		recorrido2.determinarRecorridoPosibleSegunVelocidad(megatron, tablero);
		
		recorrido2.avanzar(megatron, pos3, tablero);	
		recorrido2.avanzar(megatron, pos4, tablero);	
		
		Assert.assertTrue(megatron.obtenerAtaque() == danioAux);
	}
	
}
 