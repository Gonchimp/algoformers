package fiuba.algo3.tests;

import org.junit.Assert;
import org.junit.Test;
import utilidadesDeJuego.Juego;
import utilidadesDeJuego.Jugador;
import utilidadesDeJuego.Posicion;


public class JuegoTest {

	@Test
	public void test01CrearJuegoUbicarAlgoformersUbicarChispa(){
		Juego juego = new Juego();
		Assert.assertTrue(juego.existeChispa());
		Assert.assertEquals(new Posicion(0,0,0),juego.obtenerPosicionDeAutobot("Optimus Prime"));
		Assert.assertEquals(new Posicion(2,0,0),juego.obtenerPosicionDeAutobot("Bumblebee"));
		Assert.assertEquals(new Posicion(0,2,0),juego.obtenerPosicionDeAutobot("Ratchet"));
		Assert.assertEquals(new Posicion(20,20,0),juego.obtenerPosicionDeDecepticon("Megatron"));
		Assert.assertEquals(new Posicion(20,18,0),juego.obtenerPosicionDeDecepticon("Bonecrusher"));
		Assert.assertEquals(new Posicion(18,20,0),juego.obtenerPosicionDeDecepticon("Frenzy"));
	}

	@Test
	public void test02CrearJuegoYCambiarTurnos(){
		Juego juego = new Juego();
		Jugador anterior= juego.obtenerJugadorActivo();
		juego.cambiarTurno(); //cambio
		
		Assert.assertFalse(anterior.equals(juego.obtenerJugadorActivo()));
		juego.cambiarTurno(); //vuelvo al primer jugador activo
		Assert.assertTrue(anterior.equals(juego.obtenerJugadorActivo()));
		
	}
	
}