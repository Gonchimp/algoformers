package fiuba.algo3.tests;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import algoformers.Algoformer;
import algoformers.Bonecrusher;
import algoformers.Bumblebee;
import algoformers.Frenzy;
import algoformers.Megatron;
import algoformers.OptimusPrime;
import algoformers.Ratchet;
import bonus.BonusNulo;
import bonus.Fusion;
import excepciones.ImposibleTransformarException;
import superficies.Nube;
import superficies.Rocosa;
import utilidadesDeJuego.Casillero;
import utilidadesDeJuego.Jugador;
import utilidadesDeJuego.Posicion;
import utilidadesDeJuego.Recorrido;
import utilidadesDeJuego.Tablero;

public class AlgoformerTest {


	@Test
	public void test01CrearAOptimus(){ // Se crea Automaticamente en Modo Humanoide
		OptimusPrime optimusPrime = new OptimusPrime();
		Assert.assertTrue(optimusPrime.obtenerVida() == 500);
		Assert.assertTrue(optimusPrime.obtenerAtaque() == 50);
		Assert.assertTrue(optimusPrime.obtenerDistanciaDeAtaque() == 2);
		Assert.assertTrue(optimusPrime.obtenerVelocidad() == 2);
	}
	
	@Test
	public void test02CrearABumbeblee(){ // Se crea Automaticamente en Modo Humanoide
		Bumblebee bumblebee = new Bumblebee();
		Assert.assertTrue(bumblebee.obtenerVida() == 350);
		Assert.assertTrue(bumblebee.obtenerAtaque() == 40);
		Assert.assertTrue(bumblebee.obtenerDistanciaDeAtaque() == 1);
		Assert.assertTrue(bumblebee.obtenerVelocidad() == 2);
	}
	
	@Test
	public void test03CrearARachet(){ // Se crea Automaticamente en Modo Humanoide
		Ratchet ratchet = new Ratchet();
		Assert.assertTrue(ratchet.obtenerVida() == 150);
		Assert.assertTrue(ratchet.obtenerAtaque() == 5);
		Assert.assertTrue(ratchet.obtenerDistanciaDeAtaque() == 5);
		Assert.assertTrue(ratchet.obtenerVelocidad() == 1);
	}
	
	@Test
	public void test04CrearAMegatron(){ // Se crea Automaticamente en Modo Humanoide
		Megatron megatron = new Megatron();
		Assert.assertTrue(megatron.obtenerVida() == 550);
		Assert.assertTrue(megatron.obtenerAtaque() == 10);
		Assert.assertTrue(megatron.obtenerDistanciaDeAtaque() == 3);
		Assert.assertTrue(megatron.obtenerVelocidad() == 1);
	}
	
	@Test
	public void test05CrearABonecrusher(){ // Se crea Automaticamente en Modo Humanoide
		Bonecrusher bonecrusher = new Bonecrusher();
		Assert.assertTrue(bonecrusher.obtenerVida() == 200);
		Assert.assertTrue(bonecrusher.obtenerAtaque() == 30);
		Assert.assertTrue(bonecrusher.obtenerDistanciaDeAtaque() == 3);
		Assert.assertTrue(bonecrusher.obtenerVelocidad() == 1);
	}
	
	@Test
	public void test06CrearAFrenzy(){ // Se crea Automaticamente en Modo Humanoide
		Frenzy frenzy = new Frenzy();
		Assert.assertTrue(frenzy.obtenerVida() == 400);
		Assert.assertTrue(frenzy.obtenerAtaque() == 10);
		Assert.assertTrue(frenzy.obtenerDistanciaDeAtaque() == 5);
		Assert.assertTrue(frenzy.obtenerVelocidad() == 2);
	}
	
	@Test
	public void test07TransformarAOptimusEnSuModoAlterno(){// Se crea Automaticamente en Modo Humanoide
		OptimusPrime optimusPrime = new OptimusPrime();
		Tablero tablero = new Tablero();
		Posicion posicion = new Posicion(1,0,0);
		
		tablero.agregar(optimusPrime, posicion);
		
		optimusPrime.transformar(tablero);
		Assert.assertTrue(optimusPrime.obtenerVida() == 500);
		Assert.assertTrue(optimusPrime.obtenerAtaque() == 15);
		Assert.assertTrue(optimusPrime.obtenerDistanciaDeAtaque() == 4);
		Assert.assertTrue(optimusPrime.obtenerVelocidad() == 5);
	}
	
	@Test
	public void test08TransformarABumblebeeEnSuModoAlternoYVolverloATransformarEnSuModoHumanoide(){// Se crea Automaticamente en Modo Humanoide
		Bumblebee bumblebee = new Bumblebee();
		Tablero tablero = new Tablero();
		Posicion posicion = new Posicion(1,0,0);
		
		ArrayList<Posicion> rocosa = new ArrayList<Posicion>();
		rocosa.add(posicion);
		tablero.establecerSuperficie(rocosa, new Rocosa());
		tablero.agregarBonus(new BonusNulo(), posicion);
		tablero.agregar(bumblebee, posicion);
		
		bumblebee.transformar(tablero);
		Assert.assertTrue(bumblebee.obtenerVida() == 350);
		Assert.assertTrue(bumblebee.obtenerAtaque() == 20);
		Assert.assertTrue(bumblebee.obtenerDistanciaDeAtaque() == 3);
		Assert.assertTrue(bumblebee.obtenerVelocidad() == 5);
		bumblebee.transformar(tablero);
		Assert.assertTrue(bumblebee.obtenerVida() == 350);
		Assert.assertTrue(bumblebee.obtenerAtaque() == 40);
		Assert.assertTrue(bumblebee.obtenerDistanciaDeAtaque() == 1);
		Assert.assertTrue(bumblebee.obtenerVelocidad() == 2);
	}
	
	@Test
	public void test09MegatronNoPuedeDesplazarse2EnModoHumanoide(){
		Megatron megatron = new Megatron();
		Posicion pos1 = new Posicion (5,5,0);
		Posicion pos2 = new Posicion (5,7,0);
		Assert.assertFalse(pos1.rangoDisponible(pos2, megatron.obtenerVelocidad()));
	}
	
	@Test
	public void test10AlgoformerConModoAlternoTerrestreNoDebeMoverseDeTierraAAire(){
		OptimusPrime optimusPrime = new OptimusPrime();
		Posicion pos1 = new Posicion (15,15,0);//Posicion Tierra
		Posicion pos2 = new Posicion (15,15,1);//Posicion Aire
		Tablero tablero = new Tablero();
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos2));
		
		tablero.agregar(optimusPrime, pos1);
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos2));
		
		optimusPrime.transformar(tablero);
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos2));
	}
	
	@Test
	public void test11AlgoformerConModoAlternoAereoDebeMoverseDeTierraAAire(){
		Megatron megatron = new Megatron();
		Posicion pos1 = new Posicion (15,15,0);//Posicion Tierra
		Posicion pos2 = new Posicion (15,15,1);//Posicion Aire
		Tablero tablero = new Tablero();
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos2));
		
		tablero.agregar(megatron, pos1);
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos2));
		
		megatron.transformar(tablero);
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos1));
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos2));
	}
	
	@Test
	public void test12MegatronPuedeDesplazarse2EnModoAlterno(){
		Megatron megatron = new Megatron();
		Tablero tablero = new Tablero();
		Posicion pos1 = new Posicion (4,4,0);
		Posicion pos1Aerea = new Posicion(4,4,1);
		Posicion pos2 = new Posicion (4,6,1);
		
		ArrayList<Posicion> nube = new ArrayList<Posicion>();
		nube.add(pos2);
		nube.add(pos1Aerea);
		tablero.establecerSuperficie(nube, new Nube());
		
		tablero.agregarBonus(new BonusNulo(), pos1);
		tablero.agregarBonus(new BonusNulo(), pos2);
		
		tablero.agregar(megatron, pos1);
		
		megatron.transformar(tablero);
		Assert.assertTrue(megatron.rangoDisponible(pos2));
	}
	
	@Test
	public void test13algoformerNoAvanzaMasQueSuVelocidad(){
		Megatron megatron = new Megatron();
		Tablero tablero = new Tablero();
		Posicion pos1 = new Posicion (0,0,0);
		Posicion pos2 = new Posicion (1,0,0);
		Posicion pos3 = new Posicion (2,0,0);
		Posicion pos4 = new Posicion (3,0,0);
		
		ArrayList<Posicion> rocosas = new ArrayList<Posicion>();
		rocosas.add(pos1);
		rocosas.add(pos2);
		tablero.establecerSuperficie(rocosas, new Rocosa());
		
		tablero.agregarBonus(new BonusNulo(), pos1);
		tablero.agregarBonus(new BonusNulo(), pos2);
		
		tablero.agregar(megatron, pos1);
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(megatron, tablero);
		
		recorrido.avanzar(megatron, pos2, tablero);
		recorrido.avanzar(megatron, pos3, tablero);
		recorrido.avanzar(megatron, pos4, tablero);
					
		Assert.assertTrue(megatron.obtenerPosicionEnTablero().equals(pos2));
	}
	
	
	@Test
	public void test14Los3AutobotsSeUnenFormandoUnSuperion(){
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		Bumblebee bumblebee = new Bumblebee();
		Ratchet ratchet = new Ratchet();
		Posicion posOptimus = new Posicion (1,0,0);
		Posicion posBumblebee = new Posicion (2,2,0);
		Posicion posRatchet = new Posicion (1,3,0);
		
		
		tablero.agregar(optimusPrime, posOptimus);
		tablero.agregar(bumblebee, posBumblebee);
		tablero.agregar(ratchet, posRatchet);
		Jugador jugador=new Jugador(tablero,"Autobots");
		jugador.agregarAEquipo(bumblebee);
		jugador.agregarAEquipo(ratchet);
		jugador.agregarAEquipo(optimusPrime);
		
		Fusion fusion=new Fusion(jugador);		
		optimusPrime.combinarConAlgoformers(bumblebee,ratchet,tablero,fusion);
		optimusPrime.descontarTurno();
		optimusPrime.descontarTurno();
		Casillero casillero= tablero.getCasillero(jugador.obtenerPosicion("Superion"));
		Algoformer superion=casillero.obtenerAlgoformer();
		
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(superion.obtenerPosicionEnTablero()));
		Assert.assertEquals(posOptimus, superion.obtenerPosicionEnTablero());
		Assert.assertTrue(superion.obtenerVida() == 1000);
		Assert.assertTrue(superion.obtenerAtaque() == 100);
		Assert.assertTrue(superion.obtenerDistanciaDeAtaque() == 2);
		Assert.assertTrue(superion.obtenerVelocidad() == 3);
	}
	
	@Test
	public void test15CombinarAutobotsDeberiaQuitarlosDeSusPosiciones(){
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		Bumblebee bumblebee = new Bumblebee();
		Ratchet ratchet = new Ratchet();
		Posicion posOptimus = new Posicion (5,2,0);
		Posicion posBumblebee = new Posicion (5,4,0);
		Posicion posRatchet = new Posicion (5,6,0);
		
		
		tablero.agregar(optimusPrime, posOptimus);
		tablero.agregar(bumblebee, posBumblebee);
		tablero.agregar(ratchet, posRatchet);
		
		Jugador jugador=new Jugador(tablero,"Autobots");
		jugador.agregarAEquipo(bumblebee);
		jugador.agregarAEquipo(ratchet);
		jugador.agregarAEquipo(optimusPrime);
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(posOptimus));
		Assert.assertTrue(tablero.casilleroEstaOcupado(posBumblebee));
		Assert.assertTrue(tablero.casilleroEstaOcupado(posRatchet));
		
		Fusion fusion=new Fusion(jugador);
		
		optimusPrime.combinarConAlgoformers(bumblebee,ratchet,tablero,fusion);
		optimusPrime.descontarTurno();
		optimusPrime.descontarTurno();
		Casillero casillero= tablero.getCasillero(jugador.obtenerPosicion("Superion"));
		Algoformer superion=casillero.obtenerAlgoformer();
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(posOptimus));
		Assert.assertEquals(posOptimus,superion.obtenerPosicionEnTablero());
		Assert.assertFalse(tablero.casilleroEstaOcupado(posBumblebee));
		Assert.assertFalse(tablero.casilleroEstaOcupado(posRatchet));
	}
	@Test
	public void test16Los3DecepticonsSeUnenFormandoUnMenasor(){
		Tablero tablero = new Tablero();
		Megatron megatron = new Megatron();
		Bonecrusher bonecrusher = new Bonecrusher();
		Frenzy frenzy = new Frenzy();
		Posicion posMegatron= new Posicion (20,20,0);
		Posicion posBonecrusher= new Posicion (18,20,0);
		Posicion posFrenzy = new Posicion (18,19,0);
		
		tablero.agregar(megatron, posMegatron);
		tablero.agregar(bonecrusher, posBonecrusher);
		tablero.agregar(frenzy, posFrenzy);
		Jugador jugador=new Jugador(tablero,"Decepticons");
		jugador.agregarAEquipo(megatron);
		jugador.agregarAEquipo(bonecrusher);
		jugador.agregarAEquipo(frenzy);
		
		Fusion fusion=new Fusion(jugador);
		
		megatron.combinarConAlgoformers(bonecrusher,frenzy,tablero,fusion);
		megatron.descontarTurno();
		megatron.descontarTurno();
		Casillero casillero= tablero.getCasillero(jugador.obtenerPosicion("Menasor"));
		Algoformer menasor=casillero.obtenerAlgoformer();

		
		Assert.assertTrue(tablero.casilleroEstaOcupado(menasor.obtenerPosicionEnTablero()));
		Assert.assertEquals(posMegatron, menasor.obtenerPosicionEnTablero());
		Assert.assertTrue(menasor.obtenerVida() == 1150);
		Assert.assertTrue(menasor.obtenerAtaque() == 115);
		Assert.assertTrue(menasor.obtenerDistanciaDeAtaque() == 2);
		Assert.assertTrue(menasor.obtenerVelocidad() == 2);
	}
	
	@Test
	public void test17CombinarDecepticonsDeberiaQuitarlosDeSusPosiciones(){
		Tablero tablero = new Tablero();
		Megatron megatron = new Megatron();
		Bonecrusher bonecrusher = new Bonecrusher();
		Frenzy frenzy = new Frenzy();
		
	
		Posicion posMegatron= new Posicion (13,12,0);
		Posicion posBonecrusher= new Posicion (12,11,0);
		Posicion posFrenzy = new Posicion (11,10,0);
		
		tablero.agregar(megatron, posMegatron);
		tablero.agregar(bonecrusher, posBonecrusher);
		tablero.agregar(frenzy, posFrenzy);
		
		Jugador jugador=new Jugador(tablero,"Decepticons");
		jugador.agregarAEquipo(megatron);
		jugador.agregarAEquipo(bonecrusher);
		jugador.agregarAEquipo(frenzy);
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(posMegatron));
		Assert.assertTrue(tablero.casilleroEstaOcupado(posBonecrusher));
		Assert.assertTrue(tablero.casilleroEstaOcupado(posFrenzy));
		
		Fusion fusion =new Fusion(jugador);
		megatron.combinarConAlgoformers(bonecrusher,frenzy , tablero,fusion);
		megatron.descontarTurno();
		megatron.descontarTurno();
		Casillero casillero= tablero.getCasillero(jugador.obtenerPosicion("Menasor"));
		Algoformer menasor=casillero.obtenerAlgoformer();
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(posMegatron));
		Assert.assertEquals(posMegatron, menasor.obtenerPosicionEnTablero());
		Assert.assertFalse(tablero.casilleroEstaOcupado(posBonecrusher));
		Assert.assertFalse(tablero.casilleroEstaOcupado(posFrenzy));
	}
	
	@Test
	public void test18IntentarMoverUnAlgoformerAUnaPosicionFueraDelRangoDebeMoverHastaSuLimiteDeVelocidad(){
		Tablero tablero = new Tablero();
		Ratchet ratchet = new Ratchet();
		Posicion posicion1 = new Posicion(8,8,0);
		
		
		
		Posicion pos2 = new Posicion(8,9,0);
		Posicion pos3 = new Posicion(8,10,0);
		Posicion pos4 = new Posicion(8,11,0);
		Posicion pos5 = new Posicion(8,12,0);
		
		ArrayList<Posicion> rocosas = new ArrayList<Posicion>();
		rocosas.add(posicion1);
		rocosas.add(pos2);
		rocosas.add(pos3);
		rocosas.add(pos4);
		rocosas.add(pos5);
		tablero.establecerSuperficie(rocosas, new Rocosa());
		
		tablero.agregarBonus(new BonusNulo(), posicion1);
		tablero.agregarBonus(new BonusNulo(), pos2);
		tablero.agregarBonus(new BonusNulo(), pos3);
		tablero.agregarBonus(new BonusNulo(), pos4);
		tablero.agregarBonus(new BonusNulo(), pos5);
		
		tablero.agregar(ratchet, posicion1);
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(ratchet, tablero);
		
		recorrido.avanzar(ratchet, pos2, tablero);
		recorrido.avanzar(ratchet, pos3, tablero);
		recorrido.avanzar(ratchet, pos4, tablero);
		recorrido.avanzar(ratchet, pos5, tablero);
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(posicion1));
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos2));
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos3));
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos4));
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos5));
		
	}
	
	@Test
	public void test19IntentarMoverUnAlgofomerAUnCasilleroOcupadoNoDebeMover(){
		Tablero tablero =  new Tablero();
		OptimusPrime optimus = new OptimusPrime();
		Megatron megatron = new Megatron();
		Posicion pos1 = new Posicion(19,19,0);
		Posicion pos2 = new Posicion(20,19,0);
		
		tablero.agregar(optimus, pos1);
		tablero.agregar(megatron, pos2);
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(optimus, tablero);
		
		recorrido.avanzar(optimus, pos2, tablero);
		
		
		Assert.assertEquals(pos1, optimus.obtenerPosicionEnTablero());
	}
	@Test
	public void test20IntentarMoverUnAlgoformerAUnCasilleroOcupadoDebeMoverHastaLaPosicionAnterior(){
		Tablero tablero =  new Tablero();
		OptimusPrime optimus = new OptimusPrime();
		Megatron megatron = new Megatron();
		Posicion pos1 = new Posicion(10,10,0);
		Posicion pos2 = new Posicion(14,10,0);
		Posicion pos3 = new Posicion(11,10,0);
		Posicion pos4 = new Posicion(12,10,0);
		Posicion pos5 = new Posicion(13,10,0);
		
		tablero.agregar(optimus, pos1);
		tablero.agregar(megatron, pos2);
		
		optimus.transformar(tablero);
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(optimus, tablero);
		
		recorrido.avanzar(optimus, pos3, tablero);
		recorrido.avanzar(optimus, pos4, tablero);
		recorrido.avanzar(optimus, pos5, tablero);
		recorrido.avanzar(optimus, pos2, tablero);
		
				
		Assert.assertEquals(pos5, optimus.obtenerPosicionEnTablero());
	}
	
	@Test(expected = ImposibleTransformarException.class)
	public void test21IntentarTransformarUnAlgoformerAereoConOtroPosicionadoArribaDebeLanzarError(){
		Tablero tablero =  new Tablero();
		Ratchet ratchet = new Ratchet();
		Megatron megatron = new Megatron();
		Posicion pos1 = new Posicion(10,10,0);
		Posicion pos2 = new Posicion(11,10,0);
		Posicion pos2Aerea = new Posicion(11,10,1);
		Posicion pos3 = new Posicion(10,10,1);
		
		ArrayList<Posicion> rocosas = new ArrayList<Posicion>();
		rocosas.add(pos1);
		rocosas.add(pos2);
		
		ArrayList<Posicion> nubes = new ArrayList<Posicion>();
		nubes.add(pos2Aerea);
		nubes.add(pos3);
		
		tablero.establecerSuperficie(rocosas, new Rocosa());
		tablero.establecerSuperficie(nubes, new Nube());
				
		tablero.agregar(megatron, pos1);
		tablero.agregar(ratchet, pos2);
		
		ratchet.transformar(tablero);
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(ratchet, tablero);
		
		recorrido.avanzar(ratchet, pos3, tablero);
				
		megatron.transformar(tablero);
	}
	
	@Test(expected = ImposibleTransformarException.class)
	public void test22IntentarTransformarUnAlgoformerAereoConOtroPosicionadoAbajoDebeLanzarError(){
		Tablero tablero =  new Tablero();
		Ratchet ratchet = new Ratchet();
		Megatron megatron = new Megatron();
		Posicion pos1 = new Posicion(19,19,0);
		Posicion pos2 = new Posicion(20,19,0);
		Posicion pos2Aerea = new Posicion(20,19,1);
		Posicion pos3 = new Posicion(19,19,1);
		
		ArrayList<Posicion> rocosas = new ArrayList<Posicion>();
		rocosas.add(pos1);
		rocosas.add(pos2);
		
		ArrayList<Posicion> nubes = new ArrayList<Posicion>();
		nubes.add(pos2Aerea);
		nubes.add(pos3);
		
		tablero.establecerSuperficie(rocosas, new Rocosa());
		tablero.establecerSuperficie(nubes, new Nube());
		
		tablero.agregar(megatron, pos1);
		tablero.agregar(ratchet, pos2);
		
		ratchet.transformar(tablero);
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(ratchet, tablero);
		
		recorrido.avanzar(ratchet, pos3, tablero);
				
		ratchet.transformar(tablero);
	}
	
	@Test
	public void test23IntentarMoverAlgoformerDeTierraAAireNoDebeMover(){
		Tablero tablero =  new Tablero();
		Ratchet ratchet = new Ratchet();
		Posicion pos1 = new Posicion(19,19,0);
		Posicion pos2 = new Posicion(20,19,1);
				
		tablero.agregar(ratchet, pos1);
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(ratchet, tablero);
				
		recorrido.avanzar(ratchet, pos2, tablero);
				
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
		Assert.assertFalse(tablero.casilleroEstaOcupado(pos2));
	}
	

	@Test
	public void test24SepararSuperionDebeBorrarloDelTableroYRedistribuirALosAutobots(){
		Tablero tablero = new Tablero();
		OptimusPrime optimusPrime = new OptimusPrime();
		Bumblebee bumblebee = new Bumblebee();
		Ratchet ratchet = new Ratchet();
		Posicion posOptimus = new Posicion (5,2,0);
		Posicion posBumblebee = new Posicion (5,4,0);
		Posicion posRatchet = new Posicion (5,6,0);

		tablero.agregar(optimusPrime, posOptimus);
		tablero.agregar(bumblebee, posBumblebee);
		tablero.agregar(ratchet, posRatchet);
		Jugador jugador = new Jugador(tablero, "Autobots");
		
		jugador.agregarAEquipo(optimusPrime);
		jugador.agregarAEquipo(bumblebee);
		jugador.agregarAEquipo(ratchet);
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(posOptimus));
		Assert.assertTrue(tablero.casilleroEstaOcupado(posBumblebee));
		Assert.assertTrue(tablero.casilleroEstaOcupado(posRatchet));
		
		Fusion fusion=new Fusion(jugador);
		
		optimusPrime.combinarConAlgoformers(bumblebee,ratchet,tablero,fusion);
		optimusPrime.descontarTurno();
		optimusPrime.descontarTurno();
		
		Casillero casillero= tablero.getCasillero(jugador.obtenerPosicion("Superion"));
		Algoformer superion = casillero.obtenerAlgoformer();
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(posOptimus));
		Assert.assertEquals(posOptimus, superion.obtenerPosicionEnTablero());
		
		superion.separarse(tablero, jugador);
		
		// Las nuevas posiciones de los autobos serian (5,3,0),(5,1,0),(6,2,0)
		
		Posicion nuevaPos1= new Posicion(5,1,0);
		Posicion nuevaPos2= new Posicion(5,3,0);
		Posicion nuevaPos3= new Posicion(6,2,0);
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(nuevaPos1));
		Assert.assertTrue(tablero.casilleroEstaOcupado(nuevaPos2));
		Assert.assertTrue(tablero.casilleroEstaOcupado(nuevaPos3));

	}
	
	@Test
	public void test25SepararMenasorDebeBorrarloDelTableroYRedistribuirALosDecepticons(){

		Tablero tablero = new Tablero();
		Megatron megatron = new Megatron();
		Bonecrusher bonecrusher = new Bonecrusher();
		Frenzy frenzy = new Frenzy();
		Posicion posMegatron= new Posicion (17,15,0);
		Posicion posBonecrusher= new Posicion (18,16,0);
		Posicion posFrenzy = new Posicion (19,17,0);

		
		tablero.agregar(megatron, posMegatron);
		tablero.agregar(bonecrusher, posBonecrusher);
		tablero.agregar(frenzy, posFrenzy);
		
		Jugador jugador = new Jugador(tablero, "Decepticons");
		jugador.agregarAEquipo(megatron);
		jugador.agregarAEquipo(bonecrusher);
		jugador.agregarAEquipo(frenzy);
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(posMegatron));
		Assert.assertTrue(tablero.casilleroEstaOcupado(posBonecrusher));
		Assert.assertTrue(tablero.casilleroEstaOcupado(posFrenzy));
		Fusion fusion=new Fusion(jugador);
		
		megatron.combinarConAlgoformers(bonecrusher,frenzy ,tablero,fusion);
		megatron.descontarTurno();
		megatron.descontarTurno();
		
		Assert.assertTrue(tablero.casilleroEstaOcupado(posMegatron));
		Assert.assertEquals(posMegatron,jugador.obtenerPosicion("Menasor"));
	
		// Las nuevas posiciones de los autobos serian (17,14,0), (17,16,0), (18,15,0)
		
		Casillero casillero= tablero.getCasillero(jugador.obtenerPosicion("Menasor"));
		Algoformer menasor=casillero.obtenerAlgoformer();

		menasor.separarse(tablero, jugador);
		
		Posicion nuevaPos1= new Posicion(17,14,0);
		Posicion nuevaPos2= new Posicion(17,16,0);
		Posicion nuevaPos3= new Posicion(18,15,0);
				
		Assert.assertTrue(tablero.casilleroEstaOcupado(nuevaPos1));
		Assert.assertTrue(tablero.casilleroEstaOcupado(nuevaPos2));
		Assert.assertTrue(tablero.casilleroEstaOcupado(nuevaPos3));

		}
	
}
