package fiuba.algo3.tests;

import java.util.ArrayList;
import org.junit.Assert;
import org.junit.Test;

import algoformers.Bumblebee;
import algoformers.Megatron;
import algoformers.OptimusPrime;
import algoformers.Ratchet;
import superficies.Rocosa;
import utilidadesDeJuego.Juego;
import utilidadesDeJuego.Jugador;
import utilidadesDeJuego.Posicion;
import utilidadesDeJuego.Tablero;
import utilidadesDeJuego.Turno;

public class JugadorTest {
	
	@Test
	public void test01jugadorMueveAlgoformers(){
		
	Tablero tablero = new Tablero();
	Jugador jugador = new Jugador(tablero,"jorge");
	
	OptimusPrime optimusPrime = new OptimusPrime();
	jugador.agregarAEquipo(optimusPrime);
	Posicion pos1 = new Posicion (0,0,0);
	Posicion pos2 = new Posicion (0,1,0);
	
	ArrayList<Posicion> rocosa = new ArrayList<Posicion>();
	rocosa.add(pos1);
	rocosa.add(pos2);
	tablero.establecerSuperficie(rocosa, new Rocosa());
	
	tablero.agregar(optimusPrime, pos1);
	
	jugador.establecerAlgoformerActivo("Optimus Prime");
	jugador.mover();
	jugador.moverArriba();
	
	Assert.assertTrue(optimusPrime.obtenerPosicionEnTablero().equals(pos2));
	
	
	}
	
	@Test
	public void test02jugadorAtacaConAlgoformers(){
		
		Tablero tablero = new Tablero();
		Jugador jugador = new Jugador(tablero,"Jorge");
		
		OptimusPrime optimusPrime = new OptimusPrime();
		
		jugador.agregarAEquipo(optimusPrime);
		Posicion pos1 = new Posicion (0,0,0);
		Posicion pos2 = new Posicion (0,2,0);
		
		Megatron megatron = new Megatron();

		Assert.assertTrue(megatron.obtenerVida() == 550);
		
		tablero.agregar(optimusPrime, pos1);
		tablero.agregar(megatron, pos2);
		jugador.establecerAlgoformerActivo("Optimus Prime");
		
		jugador.atacar(megatron);

		Assert.assertTrue(megatron.obtenerVida() == 500);
	}
	
	@Test
	public void test03CambiarTurnos(){
		Juego juego=new Juego();
		Tablero tablero = new Tablero();
		Jugador jugador1= new Jugador(tablero,"jorge");
		Jugador	jugador2= new Jugador(tablero,"Pedro");
		
		
		
		ArrayList<Jugador> jugadores= new ArrayList<Jugador>();
		jugadores.add(jugador1);
		jugadores.add(jugador2);
		Turno turno= new Turno(jugadores);
		
		Jugador primeroEnJugar = turno.obtenerJugadorActivo();
		Jugador segundoEnJugar = turno.obtenerJugadorNoActivo();
		
		turno.cambiarTurno(juego);
		Assert.assertEquals(turno.obtenerJugadorActivo(),segundoEnJugar);
		Assert.assertEquals(turno.obtenerJugadorNoActivo(),primeroEnJugar);
		
	}
	
	@Test
	public void test04JugadorIntentaMoverAlgoformerMasDeLoDebidoNoDebeMover(){
		Tablero tablero = new Tablero();
		Jugador jugador1= new Jugador(tablero,"Martin");
		Posicion posicion = new Posicion(15,15,0);
		Posicion posicion2 = new Posicion (16,15,0);
		Rocosa superficie = new Rocosa();
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		posiciones.add(posicion);
		posiciones.add(posicion2);
		tablero.establecerSuperficie(posiciones, superficie);
			
		Megatron megatron = new Megatron();
		jugador1.agregarAEquipo(megatron);
		
		tablero.agregar(megatron, posicion);
		jugador1.establecerAlgoformerActivo("Megatron");
		jugador1.mover();//se movio
		jugador1.moverDerecha();//no se movio
		
		Assert.assertEquals(posicion2,megatron.obtenerPosicionEnTablero());
	}
	
	@Test
	public void test05JugadorCombinaSusAutobots(){
		Tablero tablero = new Tablero();
		Jugador jugador1= new Jugador(tablero,"Barbara");
		Posicion posOptimus = new Posicion(15,15,0);
		Posicion posBumblebee = new Posicion (16,15,0);
		Posicion posRatchet = new Posicion (16,16,0);
		
		OptimusPrime optimus = new OptimusPrime();
		Bumblebee bumblebee = new Bumblebee();
		Ratchet ratchet = new Ratchet();
		
		jugador1.agregarAEquipo(optimus);
		jugador1.agregarAEquipo(bumblebee);
		jugador1.agregarAEquipo(ratchet);
		
		tablero.agregar(optimus, posOptimus);
		tablero.agregar(bumblebee, posBumblebee);
		tablero.agregar(ratchet, posRatchet);
		
		jugador1.establecerAlgoformerActivo("Optimus Prime");
		jugador1.combinar();
		jugador1.descontarTurnoModificadoresAlgoformers();
		jugador1.descontarTurnoModificadoresAlgoformers();
		
		Assert.assertFalse(tablero.casilleroEstaOcupado(posBumblebee));
		Assert.assertFalse(tablero.casilleroEstaOcupado(posRatchet));
		Assert.assertTrue(tablero.casilleroEstaOcupado(posOptimus));
	}
	
	
}
