package fiuba.algo3.tests;

import java.util.ArrayList;
import org.junit.Assert;
import org.junit.Test;

import utilidadesDeJuego.Juego;
import utilidadesDeJuego.Jugador;
import utilidadesDeJuego.Tablero;
import utilidadesDeJuego.Turno;



public class TurnoTest {

		@Test
		public void test01cambiarTurno(){
			Juego juego=new Juego();
			Tablero tablero = new Tablero();
			Jugador jugador1 = new Jugador(tablero,"Florencia");
			Jugador jugador2 = new Jugador(tablero,"Gonzalo");
			ArrayList<Jugador> lista = new ArrayList<Jugador>();
			lista.add(jugador1);
			lista.add(jugador2);
			
			Turno turno = new Turno(lista);
			
			Jugador jugador = turno.obtenerJugadorActivo();

			turno.cambiarTurno(juego);
			Jugador otroJugador = turno.obtenerJugadorActivo();
			
			Assert.assertFalse(jugador == otroJugador);
			
		}
}
