package fiuba.algo3.tests;

import org.junit.Assert;
import org.junit.Test;
import algoformers.Megatron;
import algoformers.OptimusPrime;
import excepciones.AlgoformerNoExisteEnTableroException;
import excepciones.PosicionOcupadaException;
import utilidadesDeJuego.Chispa;
import utilidadesDeJuego.Posicion;
import utilidadesDeJuego.Tablero;

public class TableroTest {
	
	@Test
	public void test01CrearTablero21x21x2(){
		Tablero tablero = new Tablero();
		Assert.assertEquals(tablero.obtenerTamanio(),882);
	}
	
	@Test
	public void test02CrearTableroDebeIncializarConVacio(){
		Tablero tablero = new Tablero();
		for (int i = 0; i<21; i++){
			for (int j=0; j<21;j++){
				for(int k = 0; k < 2; k++){
				Posicion posicion = new Posicion(i,j,k);
				Assert.assertFalse(tablero.casilleroEstaOcupado(posicion));
				}
			}
		}
	}
	
	@Test
	public void test03AgregarAOptimus(){
		Tablero tablero = new Tablero();
		OptimusPrime optimus = new OptimusPrime();
		Posicion pos1 = new Posicion (8,8,0);
				
		tablero.agregar(optimus,pos1);
		Assert.assertTrue(tablero.casilleroEstaOcupado(pos1));
	}
	
	
	@Test
	public void test04UbicarChispa(){
		Tablero tablero = new Tablero();
		Posicion posicion = new Posicion (12,12,0);
		Assert.assertFalse(tablero.casilleroEstaOcupado(posicion));
		Chispa chispa= new Chispa();
		tablero.agregarChispa(chispa);
		Assert.assertTrue(tablero.existeChispa());
	}
	
	@Test(expected = PosicionOcupadaException.class)
	public void test05UbicarAlgoformerEnPosicionDondeYaExisteOtroAlgoformerDebeLanzarError(){
		Tablero tablero = new Tablero();
		Posicion posicion = new Posicion (5,5,0);
		OptimusPrime optimus = new OptimusPrime();
		Megatron megatron = new Megatron();
		
		tablero.agregar(optimus, posicion);
		tablero.agregar(megatron, posicion);
	}
	
	@Test(expected = AlgoformerNoExisteEnTableroException.class)
	public void test06NoPuedeEliminarseUnAlgoformerQueNoEsteEnTablero(){
		Tablero tablero = new Tablero();
		OptimusPrime optimus = new OptimusPrime();
		tablero.eliminar(optimus);
	}
	
	@Test
	public void test07AgregarUnAlgoformerAlTableroYLuegoEliminarlo(){
		Tablero tablero = new Tablero();
		Posicion posicion = new Posicion (5,5,0);
		OptimusPrime optimus = new OptimusPrime();
		
		tablero.agregar(optimus, posicion);
		Assert.assertTrue(tablero.casilleroEstaOcupado(posicion));
		
		tablero.eliminar(optimus);
		Assert.assertFalse(tablero.casilleroEstaOcupado(posicion));
	}
	
}
