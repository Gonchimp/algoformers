package fiuba.algo3.tests;

import org.junit.Assert;
import org.junit.Test;
import utilidadesDeJuego.Posicion;
import utilidadesDeJuego.Recorrido;
import java.util.ArrayList;

public class RecorridoTest {
	
	@Test
	public void test01CrearUnRecorridoYAgregarlePosiciones(){
		Recorrido recorrido = new Recorrido();
		Posicion posicion1 = new Posicion(1,1,0);
		Posicion posicion2 = new Posicion(1,2,0);
		
		recorrido.agregarPosicion(posicion1);
		recorrido.agregarPosicion(posicion2);
		
		Assert.assertEquals(2,recorrido.obtenerCantidadDePosiciones());
	}
	
	@Test
	public void test02CrearUnRecorridoATravesDeUnaListaDePosiciones(){
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		Posicion pos1 = new Posicion(0,0,0);
		Posicion pos2 = new Posicion(1,0,0);
		Posicion pos3 = new Posicion(1,1,0);
		
		posiciones.add(pos1);
		posiciones.add(pos2);
		posiciones.add(pos3);
		
		Recorrido recorrido = new Recorrido(posiciones);
		Assert.assertEquals(3,recorrido.obtenerCantidadDePosiciones());
		Assert.assertEquals(pos1,recorrido.obtenerPosicion(0));
		Assert.assertEquals(pos2,recorrido.obtenerPosicion(1));
		Assert.assertEquals(pos3,recorrido.obtenerPosicion(2));
		
	}

}
