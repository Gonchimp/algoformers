package fiuba.algo3.tests;

import java.util.ArrayList;
import org.junit.Assert;
import org.junit.Test;
import algoformers.Bonecrusher;
import algoformers.Bumblebee;
import algoformers.Megatron;
import algoformers.OptimusPrime;
import algoformers.Ratchet;
import bonus.BonusNulo;
import superficies.Espinas;
import superficies.Nebulosa;
import superficies.Nube;
import superficies.Pantano;
import superficies.Rocosa;
import superficies.Superficie;
import superficies.Tormenta;
import utilidadesDeJuego.Posicion;
import utilidadesDeJuego.Recorrido;
import utilidadesDeJuego.Tablero;

public class Entrega2Test {

	@Test
	public void test01LlenarConZonaRocosaNoDebeAfectarAlgoformer(){
		Tablero tablero = new Tablero();//Creamos nuevo tablero
		Recorrido recorrido = new Recorrido();
		Bumblebee bumblebee = new Bumblebee();//Creamos a un Bumblebeee
		Posicion pos1 = new Posicion(8,8,0);
		Posicion pos2 = new Posicion(8,9,0);
		Rocosa superficie= new Rocosa();
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		
		posiciones.add(pos1);
		posiciones.add(pos2);
		
		tablero.agregarBonus(new BonusNulo(), pos1);
		tablero.agregarBonus(new BonusNulo(), pos2);
		
		tablero.establecerSuperficie(posiciones, superficie);
		
		double vidaAnterior= bumblebee.obtenerVida();
		double ataque= bumblebee.obtenerAtaque();
		int velocidad= bumblebee.obtenerVelocidad();
		
		tablero.agregar(bumblebee, pos1);
		
		recorrido.agregarPosicion(pos2);
		recorrido.determinarRecorridoPosibleSegunVelocidad(bumblebee, tablero);
		recorrido.avanzar(bumblebee, pos2, tablero);
		
		Assert.assertTrue(bumblebee.obtenerVida()==vidaAnterior);
		Assert.assertTrue(bumblebee.obtenerAtaque()==ataque);
		Assert.assertTrue(bumblebee.obtenerVelocidad()==velocidad);
		Assert.assertTrue(bumblebee.obtenerPosicionEnTablero().equals(pos2));
	}
	
	@Test
	public void test02humanoideNoPuedeAtravesarPantano(){
		
		Tablero tablero = new Tablero();
		Bumblebee bumblebee = new Bumblebee();
		
		Posicion pos1 = new Posicion(0,0,0);
		Posicion pos2 = new Posicion(1,0,0);
		Posicion pos3 = new Posicion(1,1,0);
				
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		
		posiciones.add(pos2);
		posiciones.add(pos3);
		
		Superficie pantano = new Pantano();
		tablero.establecerSuperficie(posiciones, pantano);
		
		tablero.agregar(bumblebee, pos1);
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(bumblebee, tablero);
		
		recorrido.avanzar(bumblebee, pos2, tablero);
		recorrido.avanzar(bumblebee, pos3, tablero);
		
		Assert.assertTrue(bumblebee.obtenerPosicionEnTablero().equals(pos1));
		
	}
	@Test
	public void test03AlgoformerEnAlternoTerrestreTardaDobleEnPantano(){
		
		Tablero tablero = new Tablero();
		Bonecrusher bonecrusher= new Bonecrusher();
		
		Posicion pos1 = new Posicion(0,0,0);
		Posicion pos2 = new Posicion(1,0,0);
		Posicion pos3 = new Posicion(2,0,0);
		
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		
		posiciones.add(pos2);
		posiciones.add(pos3);
		
		Superficie pantano = new Pantano();
		tablero.establecerSuperficie(posiciones, pantano);
		
		tablero.agregar(bonecrusher, pos1);
		
		bonecrusher.transformar(tablero);
				
		Recorrido recorrido1 = new Recorrido();
		
		recorrido1.determinarRecorridoPosibleSegunVelocidad(bonecrusher, tablero);//Tiene 8 movimentos
		
		recorrido1.avanzar(bonecrusher, pos2, tablero);//Se reduce 2 movimientos
		recorrido1.avanzar(bonecrusher, pos3, tablero);//Se reduce 2 movimientos
		
		Assert.assertTrue(bonecrusher.getMovRestantes() == 4);//Deberia tener 4 movimientos restantes
	}
	
	@Test
	public void test04LlenarZonaConPantanoNoDebeAfectarAlgoformerAereo(){
		
		Tablero tablero = new Tablero();
		Megatron megatron= new Megatron();
		
		Posicion pos1 = new Posicion(0,0,0);
		Posicion pos1Aerea = new Posicion(0,0,1);
		Posicion pos2 = new Posicion(1,0,1);
		Posicion pos3 = new Posicion(2,0,1);
		Posicion pos4 = new Posicion(1,0,0);
		Posicion pos5 = new Posicion(2,0,0);
		
		tablero.agregarBonus(new BonusNulo(), pos1);
		tablero.agregarBonus(new BonusNulo(), pos2);
		tablero.agregarBonus(new BonusNulo(), pos3);
		
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		ArrayList<Posicion> posicionesNube = new ArrayList<Posicion>();

		posicionesNube.add(pos1Aerea);
		posicionesNube.add(pos2);
		posicionesNube.add(pos3);
		tablero.establecerSuperficie(posicionesNube, new Nube());
		
		posiciones.add(pos1);
		posiciones.add(pos4);
		posiciones.add(pos5);
		tablero.establecerSuperficie(posiciones, new Pantano());
		
		tablero.agregar(megatron, pos1);	// megatron queda en posicion (0,0,0)
		megatron.transformar(tablero);		//megatron queda en posicion (0,0,1)
		
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(megatron, tablero);
			
		double vidaAnterior= megatron.obtenerVida();
		double ataque= megatron.obtenerAtaque();
		int velocidad= megatron.obtenerVelocidad();
		
		recorrido.avanzar(megatron, pos2, tablero);
		recorrido.avanzar(megatron, pos3, tablero);
		
		Assert.assertTrue(megatron.obtenerVida()==vidaAnterior);
		Assert.assertTrue(megatron.obtenerAtaque()==ataque);
		Assert.assertTrue(megatron.obtenerVelocidad()==velocidad);
		Assert.assertTrue(megatron.obtenerPosicionEnTablero().equals(pos3));
	}
	
		
	@Test
	public void test05LlenarConZonaEspinasYAlgoformerPierde5PorcientoDeVidaPorCadaCasilleroConEspinas(){
		//veo que se haya reducido un 5 porciento la vida al pasar por un solo casillero con Espinas
		Tablero tablero = new Tablero();//Creamos nuevo tablero
		Recorrido recorrido = new Recorrido();
		OptimusPrime optimusPrime = new OptimusPrime();//Creamos a un OptimusPrime(se crea en modo Humanoide(terrestre)
		Posicion pos1 = new Posicion(7,7,0);
		Posicion pos2 = new Posicion(7,8,0);
		Posicion pos3 = new Posicion(7,9,0);
		Espinas superficie= new Espinas();
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		
		posiciones.add(pos2);
		posiciones.add(pos3);
		
		tablero.agregar(optimusPrime, pos1);
		tablero.establecerSuperficie(posiciones, superficie);
		
		recorrido.determinarRecorridoPosibleSegunVelocidad(optimusPrime, tablero);
		
		double vidaAnterior= optimusPrime.obtenerVida();
		double vidaAnteriorReducida5=vidaAnterior*0.95;
		double vidaAnteriorReducidaOtraVez = vidaAnteriorReducida5*0.95;

		//redujo 2 veces la vida un 5 porciento porque paso por 2 casilleros con pantano
		
		recorrido.avanzar(optimusPrime, pos2, tablero);
		recorrido.avanzar(optimusPrime, pos3, tablero);
		Assert.assertTrue(optimusPrime.obtenerVida()==vidaAnteriorReducidaOtraVez);
		
		
	}
	
	@Test
	public void test06LlenarConZonaEspinasYAlgoformerNoPierdeVidaAlPasarEnModoAlternoAereo(){
		//veo que se haya reducido un 5 porciento la vida al pasar por un solo casillero con Espinas
		Tablero tablero = new Tablero();//Creamos nuevo tablero
		Recorrido recorrido = new Recorrido();
		Ratchet ratchet= new Ratchet();
		Posicion pos1 = new Posicion(7,7,0);
		Posicion pos1Aerea = new Posicion(7,7,1);
		Posicion pos2 = new Posicion(7,8,1);
		Posicion pos3 = new Posicion(7,9,1);
		Posicion pos4 = new Posicion(7,8,0);
		Posicion pos5 = new Posicion(7,9,0);
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		ArrayList<Posicion> posiciones2 = new ArrayList<Posicion>();
		Espinas superficie= new Espinas();
		Nube superficie2 = new Nube();
		posiciones.add(pos1Aerea);
		posiciones.add(pos2);
		posiciones.add(pos3);
		posiciones2.add(pos4);
		posiciones2.add(pos5);
		tablero.establecerSuperficie(posiciones, superficie2);
		tablero.establecerSuperficie(posiciones2, superficie);
		
		//Creamos a un Ratchet(se crea en modo Humanoide(terrestre)); se puede transformar a un modo alterno aereo
		tablero.agregar(ratchet, pos1);
		ratchet.transformar(tablero);
		
		recorrido.determinarRecorridoPosibleSegunVelocidad(ratchet, tablero);
		recorrido.avanzar(ratchet, pos2, tablero);
		recorrido.avanzar(ratchet, pos3, tablero);
		
		Assert.assertEquals(ratchet.obtenerVida(),150,00);// no se redujo la vida
		ratchet.transformar(tablero); //se transforma a la zona con espinas (TIERRA)
		Assert.assertTrue(ratchet.obtenerPosicionEnTablero().equals(pos5));
		Assert.assertTrue(ratchet.obtenerVida() != 150.00); // se reduce la vida
	}
	
	@Test
	public void test07LlenarConZonaNubesoNoDebeAfectarAlgoformerEnModoAlternoAereo(){
		Tablero tablero = new Tablero();//Creamos nuevo tablero
		Recorrido recorrido = new Recorrido();
		Ratchet ratchet = new Ratchet();//Creamos a un ratchet
		Posicion pos1 = new Posicion(8,8,0);
		Posicion pos2 = new Posicion(8,9,0);
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		Nube superficie= new Nube();
		
		posiciones.add(pos1);
		posiciones.add(pos2);
		tablero.establecerSuperficie(posiciones, superficie);
		
		tablero.agregar(ratchet, pos1);
		ratchet.transformar(tablero); //tranformamos ratchet a modo alterno aereo
		
		recorrido.determinarRecorridoPosibleSegunVelocidad(ratchet, tablero);
		recorrido.avanzar(ratchet, pos2, tablero);
		
		double vidaAnterior= ratchet.obtenerVida();
		double ataque= ratchet.obtenerAtaque();
		int velocidad= ratchet.obtenerVelocidad();
		
		Assert.assertTrue(ratchet.obtenerVida()==vidaAnterior);
		Assert.assertTrue(ratchet.obtenerAtaque()==ataque);
		Assert.assertTrue(ratchet.obtenerVelocidad()==velocidad);
		
	}
	
	@Test
	public void test08UnidadAereaQuePasaPorNebulosaQuedaAtrapada3TurnosSinMoverse(){
		

		Tablero tablero = new Tablero();
		Megatron megatron = new Megatron();
		
		Posicion pos0 = new Posicion(0,0,0);
		Posicion pos1 = new Posicion(0,0,1);
		Posicion pos2 = new Posicion(1,0,1);
		Posicion pos3 = new Posicion(2,0,1);
		Posicion pos4 = new Posicion(3,0,1);
		
		ArrayList<Posicion> posicionesNube = new ArrayList<Posicion>();
		posicionesNube.add(pos1);
		posicionesNube.add(pos2);
		posicionesNube.add(pos4);
		ArrayList<Posicion> posicionesNebulosa = new ArrayList<Posicion>();
		posicionesNebulosa.add(pos3);
		
		tablero.establecerSuperficie(posicionesNube, new Nube());
		tablero.establecerSuperficie(posicionesNebulosa, new Nebulosa());
		
		tablero.agregarBonus(new BonusNulo(), pos0);
		tablero.agregarBonus(new BonusNulo(), pos1);
		tablero.agregarBonus(new BonusNulo(), pos2);
		tablero.agregarBonus(new BonusNulo(), pos3);
		tablero.agregarBonus(new BonusNulo(), pos4);
		
		tablero.agregar(megatron, pos0);
		
		megatron.transformar(tablero);
		
		Recorrido recorrido = new Recorrido();
		recorrido.determinarRecorridoPosibleSegunVelocidad(megatron, tablero);
		
		recorrido.avanzar(megatron, pos2, tablero);
		recorrido.avanzar(megatron, pos3, tablero);
		recorrido.avanzar(megatron, pos4, tablero);
			
		Assert.assertTrue(megatron.obtenerPosicionEnTablero().equals(pos3)); //se metio en nebulosa
		
		recorrido.determinarRecorridoPosibleSegunVelocidad(megatron, tablero);
		recorrido.avanzar(megatron, pos4, tablero);
		Assert.assertTrue(megatron.obtenerPosicionEnTablero().equals(pos3));
		megatron.descontarTurno();
		
		recorrido.determinarRecorridoPosibleSegunVelocidad(megatron, tablero);
		recorrido.avanzar(megatron, pos4, tablero);
		Assert.assertTrue(megatron.obtenerPosicionEnTablero().equals(pos3));
		megatron.descontarTurno();

		recorrido.determinarRecorridoPosibleSegunVelocidad(megatron, tablero);
		recorrido.avanzar(megatron, pos4, tablero);
		Assert.assertTrue(megatron.obtenerPosicionEnTablero().equals(pos3));
		megatron.descontarTurno();
		
		//pasaron los 3 turnos;vuelve a moverse
		recorrido.determinarRecorridoPosibleSegunVelocidad(megatron, tablero);
		recorrido.avanzar(megatron, pos4, tablero);
		Assert.assertTrue(megatron.obtenerPosicionEnTablero().equals(pos4));
		megatron.descontarTurno();
		
	}
	
	@Test
	public void test09LlenarConZonaTormentaYAlgoformerEnModoALternoAereoBajaSuAtaque(){
		//veo que se haya reducido un 40 porciento la capacidad de ataque
		Tablero tablero = new Tablero();//Creamos nuevo tablero
		Recorrido recorrido = new Recorrido();
		Ratchet ratchet = new Ratchet();
		Posicion pos1 = new Posicion(7,7,0);
		Posicion pos2 = new Posicion(7,7,1);
		Posicion pos3 = new Posicion(7,8,1);
		
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		ArrayList<Posicion> posicionNube = new ArrayList<Posicion>();
		
		posicionNube.add(pos2);
		posiciones.add(pos3);
		tablero.establecerSuperficie(posicionNube, new Nube());
		tablero.establecerSuperficie(posiciones, new Tormenta());
		
		tablero.agregarBonus(new BonusNulo(), pos1);
		tablero.agregarBonus(new BonusNulo(), pos2);
		tablero.agregarBonus(new BonusNulo(), pos3);
		
		tablero.agregar(ratchet, pos1);
		ratchet.transformar(tablero);
		Assert.assertTrue(ratchet.obtenerPosicionEnTablero().equals(pos2));
		
		recorrido.determinarRecorridoPosibleSegunVelocidad(ratchet, tablero);
		
		double ataque=ratchet.obtenerAtaque();
		double ataqueReducido40=ataque*0.6;
		
		//ataquereducido40 representa al poder de ataque del algoformer reducido un 40 porciento
		recorrido.avanzar(ratchet, pos3, tablero);
		
		Assert.assertTrue(ratchet.obtenerPosicionEnTablero().equals(pos3));
		Assert.assertTrue(ratchet.obtenerAtaque()==ataqueReducido40);
	}
	
	@Test
	public void test10AlgoformerEnAereoQueYaPasoPorTormentaNoBajaSuAtaque(){
		//veo que no se vuelva a  reducir un 40 porciento la capacidad de ataque
		Tablero tablero = new Tablero();//Creamos nuevo tablero
		Recorrido recorrido = new Recorrido();
		Ratchet ratchet = new Ratchet();
		Posicion pos0= new Posicion(7,7,0);
		Posicion pos1= new Posicion(7,7,1);
		Posicion pos2 = new Posicion(7,8,1);
		Posicion pos3 = new Posicion(7,9,1);
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		ArrayList<Posicion> nube = new ArrayList<Posicion>();
		nube.add(pos1);
		posiciones.add(pos2);
		posiciones.add(pos3);
		
		tablero.establecerSuperficie(posiciones,  new Tormenta());
		tablero.establecerSuperficie(nube, new Nube());
		
		tablero.agregarBonus(new BonusNulo(), pos0);
		tablero.agregarBonus(new BonusNulo(), pos1);
		tablero.agregarBonus(new BonusNulo(), pos2);
		tablero.agregarBonus(new BonusNulo(), pos3);
		
		recorrido.agregarPosicion(pos2);
		recorrido.agregarPosicion(pos3);
		recorrido.determinarRecorridoPosibleSegunVelocidad(ratchet, tablero);
		
		tablero.agregar(ratchet, pos0);
		ratchet.transformar(tablero);
		
		double ataque=ratchet.obtenerAtaque();
		double ataqueReducido40=ataque*0.6;
       //ataquereducido40 representa al poder de ataque del algoformer reducido un 40 porciento
				
		recorrido.avanzar(ratchet, pos2, tablero); //pasa por la tormenta por primera vez
		Assert.assertTrue(ratchet.obtenerAtaque()==ataqueReducido40); //verifico que redujo el ataque 
		
		ataque= ratchet.obtenerAtaque();
		ataqueReducido40= ataque*0.6;
	
		recorrido.avanzar(ratchet, pos3, tablero); //vuelvo a pasar por la tormenta
		Assert.assertFalse(ratchet.obtenerAtaque()==ataqueReducido40); //verifico que no redujo el ataque 
	
	}
}
