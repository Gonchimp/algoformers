package fiuba.algo3.tests;

import org.junit.Assert;
import org.junit.Test;
import excepciones.PosicionInvalidaException;
import utilidadesDeJuego.Posicion;

public class PosicionTest {
	
	@Test 
	public void test01CrearUnaPosicion(){
	Posicion posicion = new Posicion(1,2,0);
	Assert.assertEquals(1, posicion.getX());
	Assert.assertEquals(2, posicion.getY());
	Assert.assertEquals(0, posicion.getZ());
	}
	
	@Test(expected = PosicionInvalidaException.class)
	public void test02CrearPosicionConCoordenadasInvalidasDebeLanzarError(){
	Posicion posicion = new Posicion(-2,-3,-4);
	}
	
	@Test
	public void test03VerificarQueUnaPosicionEsteEnUnRangoPosibleDeOtraPosicion(){
	Posicion pos1 = new Posicion(1,2,0);
	Posicion pos2 = new Posicion (5,6,0); 
	//Ambas estan a un rango de 4 
	Assert.assertTrue(pos1.rangoDisponible(pos2, 4));
	}
	
	@Test
	public void test04VerificarQueUnaPosicionNoEsteEnUnRangoPosibleDeOtraPosicion(){
	Posicion pos1 = new Posicion(1,2,0);
	Posicion pos2 = new Posicion (5,6,0); 
	//Ambas estan a un rango de 4 
	Assert.assertFalse(pos1.rangoDisponible(pos2, 3));
	}

}
