package vista;

import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import utilidadesDeJuego.Juego;


public class TableroVista extends GridPane {
 
 private int coordenadaZ;
 private Color color1;
 private Color color2;
 private VistaDeAlgoformers vistaDeAlgoformers;
 private Juego juego;
  
 private static final int BOARD_DIM = 21;
    private static final int SQUARES = 32;
    
 
 public TableroVista(Color color1, Color color2,int coordenadaZ,Juego juego){
  super();
  this.juego = juego;
  this.color1 = color1;
  this.color2 = color2;
  this.coordenadaZ = coordenadaZ;
  this.setTablero();
  this.vistaDeAlgoformers = new VistaDeAlgoformers(this,this.juego);
 }
 
 private void setTablero(){
  for(int i = 0; i < BOARD_DIM; i++){
            RowConstraints rc = new RowConstraints();
            rc.setMinHeight(SQUARES);
            rc.setMaxHeight(SQUARES);
            rc.setPrefHeight(SQUARES);
            rc.setValignment(VPos.CENTER);
            this.getRowConstraints().add(rc);

            ColumnConstraints cc = new ColumnConstraints();
            cc.setMinWidth(SQUARES);
            cc.setMaxWidth(SQUARES);
            cc.setPrefWidth(SQUARES);
            cc.setHalignment(HPos.CENTER);
            this.getColumnConstraints().add(cc);
            
       }
     
     this.setearColores();
 }
 
 public void setearColores(){
  Color[] sqColors = new Color[]{this.color1, this.color2};
        for (int i = 0; i < BOARD_DIM; i ++){
            for (int j = 0; j < BOARD_DIM; j++){
                this.add(new Rectangle(SQUARES,SQUARES,sqColors[(i+j)%2]),i,j);
            }
        }
 }
 
 public void actualizar(ImageView imagen, Image espinas,Image rocas,Image pantano,
       Image nube ,Image nebulosa,Image tormenta,Image burbuja, Image dobleCanion, Image flash, Image chispa){
	this.setImagenes(espinas, rocas, pantano, nube, nebulosa, tormenta, burbuja, dobleCanion, flash, chispa);
    this.vistaDeAlgoformers.actualizarVista();
 }
 
 private void setSuperficieAire(String name,Image tormenta, Image nebulosa, Image nube, Integer i, Integer j) {
  if(name=="Tormenta"){
   ImageView imageTormenta=new ImageView(tormenta);
         imageTormenta.setFitWidth(30);
         imageTormenta.setFitHeight(30);
   this.add(imageTormenta,i,j);
  }
  if(name=="Nebulosa"){
   ImageView imageNebulosa=new ImageView(nebulosa);
         imageNebulosa.setFitWidth(30);
         imageNebulosa.setFitHeight(30);
   this.add(imageNebulosa,i,j);
  }
  if(name=="Nube"){
   ImageView imagenNube=new ImageView(nube);
         imagenNube.setFitWidth(30);
         imagenNube.setFitHeight(30);
   this.add(imagenNube,i,j);
  }
  
 }

 private void setSuperficieTierra(String name,Image pantano, Image rocas, Image espinas, Integer i, Integer j) {
  if(name=="Pantano"){
   ImageView imagePanta=new ImageView(pantano);
         imagePanta.setFitWidth(30);
         imagePanta.setFitHeight(30);
   this.add(imagePanta,i,j);
  }
  if(name=="Rocosa"){
   ImageView imageRoca=new ImageView(rocas);
         imageRoca.setFitWidth(30);
         imageRoca.setFitHeight(30);
   this.add(imageRoca,i,j);
  }
  if(name=="Espinas"){
   ImageView imagenEsp=new ImageView(espinas);
         imagenEsp.setFitWidth(30);
         imagenEsp.setFitHeight(30);
   this.add(imagenEsp,i,j);
  }
  
 }

 private void setBonus(String name, Image burbuja,Image dobleCanion, Image flash, Integer i,Integer j) {
	 if(name=="Flash"){
		   ImageView imageFlash=new ImageView(flash);
		   		imageFlash.setFitWidth(30);
		   		imageFlash.setFitHeight(30);
		   this.add(imageFlash,i,j);
		  }
	if(name=="DobleCanion"){
		   ImageView imageCanon=new ImageView(dobleCanion);
		   		imageCanon.setFitWidth(30);
		   		imageCanon.setFitHeight(30);
		   this.add(imageCanon,i,j);
		  }
	if(name=="Burbuja"){
		   ImageView imagenBurbuja=new ImageView(burbuja);
		   		imagenBurbuja.setFitWidth(30);
		   		imagenBurbuja.setFitHeight(30);
		   this.add(imagenBurbuja,i,j);
		  }
   }
 
 private void setImagenes(Image espinas,Image rocas, Image pantano,
       Image nube ,Image nebulosa,Image tormenta,Image burbuja,Image dobleCanion, Image flash, Image chispa){
  String name="";
  String nameBonus="";
  for(int i=0;i<BOARD_DIM;i++){
   for(int j=0;j<BOARD_DIM;j++){
    name=juego.obtenerSuperficieEnPos(i, j, this.coordenadaZ);
    nameBonus=juego.obtenerBonusEnPos(i, j, this.coordenadaZ);
    if(this.coordenadaZ ==0){
     this.setSuperficieTierra(name,pantano,rocas,espinas,i,j);
    }
    else{
     this.setSuperficieAire(name,tormenta,nebulosa,nube,i,j);
    } 
    if(nameBonus=="Flash" || nameBonus=="DobleCanion"|| nameBonus=="Burbuja" ){
     this.setBonus(nameBonus,burbuja,dobleCanion,flash,i,j);
    }
   }
  }
  ImageView imagechispa=new ImageView(chispa);
  imagechispa.setFitWidth(30);
  imagechispa.setFitHeight(30);
  if(this.coordenadaZ==0){
	  this.add(imagechispa,10,10);
  }
	  this.vistaDeAlgoformers.actualizarVista();
 }
 
 public int obtenerCoordenadaZ(){
  return this.coordenadaZ;
 }
}