package vista;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import EventoVista.BotonAtacarHandler;
import EventoVista.BotonAtrasHandler;
import EventoVista.BotonCambiarTableroHandler;
import EventoVista.BotonCombinarHandler;
import EventoVista.BotonMoverHandler;
import EventoVista.BotonSepararHandler;
import EventoVista.BotonTransformarHandler;
import algoformers.Algoformer;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import utilidadesDeJuego.Jugador;


public class ContenedorOpciones extends VBox {
	
	private VBox anterior;
	private Jugador jugador;
	private ContenedorPrincipal contenedorPrincipal;
		
	public ContenedorOpciones(ContenedorPrincipal contenedorPrincipal,VBox anterior) throws IOException{
		this.contenedorPrincipal = contenedorPrincipal;
		this.anterior = anterior;
		this.setOpciones();
	}
	
	private void setOpciones() throws IOException{
		
		this.jugador = this.contenedorPrincipal.getJuego().obtenerJugadorActivo();
	    LinkedList<Algoformer> equipo = this.jugador.obtenerListaDeAlgoformers();
		Button botonMover= new Button("Mover");
	    Button botonAtacar = new Button("Atacar");
	    Button botonTransformar = new Button("Transformar");
	    Button botonCombinar = new Button("Combinar");
	    Button botonSeparar = new Button("Separar");
	    Button botonCambiarTablero = new Button("Cambiar tablero");
	    Button botonAtras = new Button("Atras");	       
	    
	    BotonMoverHandler botonMoverHandler = new BotonMoverHandler(this.contenedorPrincipal,this,this.jugador);
	    BotonAtacarHandler botonAtacarHandler = new BotonAtacarHandler(this.contenedorPrincipal,this,this.jugador);
	    BotonAtrasHandler botonAtrasHandler = new BotonAtrasHandler(this.contenedorPrincipal,this.anterior);
	    BotonTransformarHandler botonTransformarHandler = new BotonTransformarHandler(this.contenedorPrincipal,this.jugador);
	    BotonCambiarTableroHandler botonCambiarTableroHandler = new BotonCambiarTableroHandler(this.contenedorPrincipal);
	    BotonCombinarHandler botonCombinarHandler = new BotonCombinarHandler(this.contenedorPrincipal, this.jugador);
	    BotonSepararHandler botonSepararHandler = new BotonSepararHandler(this.contenedorPrincipal, this.jugador);
	    
	    botonMover.setOnAction(botonMoverHandler);
	    botonAtras.setOnAction(botonAtrasHandler);
	    botonAtacar.setOnAction(botonAtacarHandler);
	    botonTransformar.setOnAction(botonTransformarHandler);
	  	botonCambiarTablero.setOnAction(botonCambiarTableroHandler);
	  	botonCombinar.setOnAction(botonCombinarHandler);
	  	botonSeparar.setOnAction(botonSepararHandler);
	  	

	    double ataque = this.jugador.obtenerAlgoformerActivo().obtenerAtaque();
	    double velocidad = this.jugador.obtenerAlgoformerActivo().obtenerVelocidad();
	    if(ataque == 0){
	    	botonAtacar.setDisable(true);
	    }
	    if(velocidad == 0){
	    	botonMover.setDisable(true);
	    }
	  	if(equipo.size()<3){
	  		botonCombinar.setDisable(true);
	  	}
	  	if(equipo.size()>1){
	  		botonSeparar.setDisable(true);
	  	}
	  
	  	
	    ArrayList<Button> botones = new ArrayList<Button>();
	    botones.add(botonMover);
	    botones.add(botonAtacar);
	    botones.add(botonTransformar);
	    botones.add(botonCombinar);
	    botones.add(botonSeparar);
	    botones.add(botonCambiarTablero);
	    botones.add(botonAtras);
	    
	    this.setStyle("-fx-background-color: #336699;");
	    this.setPadding(new Insets(20, 20, 20, 20));
	    this.setAlignment(Pos.CENTER);
	    this.setSpacing(20);
	    this.setPadding(new Insets(25));
	    this.getChildren().addAll(botones);
	    }
	
}


