package vista;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import EventoVista.BotonAbajoDerechaHandler;
import EventoVista.BotonAbajoHandler;
import EventoVista.BotonAbajoIzquierdaHandler;
import EventoVista.BotonArribaDerechaHandler;
import EventoVista.BotonArribaHandler;
import EventoVista.BotonArribaIzquierdaHandler;
import EventoVista.BotonAtrasHandler;
import EventoVista.BotonDerechaHandler;
import EventoVista.BotonIzquierdaHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import utilidadesDeJuego.Jugador;

public class ContenedorMovimiento extends GridPane {
	
	private ContenedorPrincipal contenedorPrincipal;
	private VBox anterior;
	
	private Jugador jugador;
	
	public ContenedorMovimiento(ContenedorPrincipal contenedorPrincipal,VBox anterior) throws IOException{
		this.contenedorPrincipal = contenedorPrincipal;
		this.anterior = anterior;
		this.setMovimientos();
	}

	private void setMovimientos() throws IOException{
		this.jugador = contenedorPrincipal.getJuego().obtenerJugadorActivo();
		
		Button botonArriba= new Button("",this.crearImagen("arriba.jpg"));
		botonArriba.setStyle("-fx-base: white;");
		Button botonAbajo = new Button("",this.crearImagen("abajo.jpg"));
		botonAbajo.setStyle("-fx-base: white;");
	    Button botonDerecha = new Button("",this.crearImagen("derecha.jpg"));
	    botonDerecha.setStyle("-fx-base: white;");
	    Button botonIzquierda = new Button("",this.crearImagen("izquierda.jpg"));
	    botonIzquierda.setStyle("-fx-base: white;");
	    Button botonArribaDerecha= new Button("",this.crearImagen("arribaderecha.jpg"));
	    botonArribaDerecha.setStyle("-fx-base: white;");
	    Button botonArribaIzquierda = new Button("",this.crearImagen("arribaizquierda.jpg"));
	    botonArribaIzquierda.setStyle("-fx-base: white;");
	    Button botonAbajoDerecha = new Button("",this.crearImagen("abajoderecha.jpg"));
	    botonAbajoDerecha.setStyle("-fx-base: white;");
	    Button botonAbajoIzquierda = new Button("",this.crearImagen("abajoizquierda.jpg"));
	    botonAbajoIzquierda.setStyle("-fx-base: white;");
	    Button botonAtras = new Button("Atras");
	    
	    BotonAtrasHandler botonAtrasHandler = new BotonAtrasHandler(this.contenedorPrincipal,this.anterior);
	    BotonAbajoHandler botonAbajoHandler = new BotonAbajoHandler(this.contenedorPrincipal,this.jugador);
	    BotonArribaHandler botonArribaHandler = new BotonArribaHandler(this.contenedorPrincipal,this.jugador);
	    BotonDerechaHandler botonDchaHandler = new BotonDerechaHandler(this.contenedorPrincipal,this.jugador);
	    BotonIzquierdaHandler botonIzqHandler = new BotonIzquierdaHandler(this.contenedorPrincipal,this.jugador);
	    BotonAbajoDerechaHandler botonAbajoDerechaaHandler = new BotonAbajoDerechaHandler(this.contenedorPrincipal,this.jugador);
	    BotonAbajoIzquierdaHandler botonAbajoIzquierdaHandler = new BotonAbajoIzquierdaHandler(this.contenedorPrincipal,this.jugador);
	    BotonArribaIzquierdaHandler botonArribaIzquierdaHandler = new BotonArribaIzquierdaHandler(this.contenedorPrincipal,this.jugador);
	    BotonArribaDerechaHandler botonArribaDerechaHandler = new BotonArribaDerechaHandler(this.contenedorPrincipal,this.jugador);
	    
	    botonAtras.setOnAction(botonAtrasHandler);
	    botonAbajo.setOnAction(botonAbajoHandler);
	    botonArriba.setOnAction(botonArribaHandler);
	    botonDerecha.setOnAction(botonDchaHandler);
	    botonIzquierda.setOnAction(botonIzqHandler);
	    botonArribaDerecha.setOnAction(botonArribaDerechaHandler);
	    botonArribaIzquierda.setOnAction(botonArribaIzquierdaHandler);
	    botonAbajoDerecha.setOnAction(botonAbajoDerechaaHandler);
	    botonAbajoIzquierda.setOnAction(botonAbajoIzquierdaHandler);
	    
	    this.setStyle("-fx-background-color: #336699;");
	    this.setAlignment(Pos.CENTER);
	    this.add(botonArribaIzquierda, 0, 0);
	    this.add(botonArriba, 1, 0);
	    this.add(botonArribaDerecha,2,0);
	    this.add(botonIzquierda, 0, 1);
	    this.add(botonAtras, 1, 1);
	    this.add(botonDerecha,2,1);
	    this.add(botonAbajoIzquierda, 0, 2);
	    this.add(botonAbajo, 1, 2);
	    this.add(botonAbajoDerecha,2,2);
	}
	
	private ImageView crearImagen(String direccion) throws IOException{
		InputStream is=Files.newInputStream(Paths.get("res/images/"+direccion));
        Image img =new Image(is);
        is.close();
        ImageView imgView=new ImageView(img);
        imgView.setFitWidth(35);
        imgView.setFitHeight(35);
        return imgView;
	}
	
	

}