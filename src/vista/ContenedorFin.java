package vista;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import utilidadesDeJuego.Juego;

public class ContenedorFin extends BorderPane{
	private Juego juego;
	private Stage stage;
	private ContenedorJugadores jugadores;
	
	public ContenedorFin(Stage stage,ImageView image,Juego juego){
		this.stage = stage;
		this.juego = juego;
		this.setMenu();
		this.setFondo(image);
		this.setGanadores();
	}
	
	private void setMenu(){
		Menu fileMenu = new Menu("Menu");
        MenuItem newFile = new MenuItem("Exit...");
        newFile.setOnAction(e -> stage.close());
        fileMenu.getItems().add(newFile);
        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(fileMenu);
        this.setTop(menuBar);
	}
	
	private void setFondo(ImageView image){
		this.getChildren().add(image);
	}
	
	private void setGanadores(){
		this.jugadores = new ContenedorJugadores(juego);       
		this.getChildren().add(jugadores);
     }

}
