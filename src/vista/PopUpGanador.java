package vista;

import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.*;

public class PopUpGanador {
	
	public static void display(String titulo, String mensaje, BackgroundImage fondo){
		
		Stage window = new Stage();
		
		window.initModality(Modality.APPLICATION_MODAL);
		window.setTitle(titulo);
		window.setMinWidth(350);
		window.setMaxWidth(350);
		window.setMinHeight(500);
		window.setMaxHeight(500);
		
		Label label = new Label();
		label.setText(mensaje);
		label.setTextFill(Color.WHITE);
		label.setFont(Font.font("courier new", FontWeight.BOLD,20));

		Button salirButton = new Button ("Salir");
		
		salirButton.setOnAction(e -> window.close());
		
		VBox layout = new VBox(10);
		layout.getChildren().addAll(label, salirButton);
		layout.setAlignment(Pos.CENTER);
		layout.setBackground(new Background(fondo));
		layout.autosize();
		
		Scene scene = new Scene(layout);
		window.setScene(scene);
		window.showAndWait();
	}

}
