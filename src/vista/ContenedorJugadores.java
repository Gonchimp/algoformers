package vista;

import java.util.ArrayList;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import utilidadesDeJuego.Juego;

public class ContenedorJugadores extends VBox {
	
	public ContenedorJugadores(Juego juego){
		this.setCartelDeFin(juego);
	}
	private void setCartelDeFin(Juego juego){
		Label etiquetaGanador = new Label();
		etiquetaGanador.setText(juego.obtenerGanador());
		etiquetaGanador.setFont(Font.font("courier new", FontWeight.SEMI_BOLD, 40));
		etiquetaGanador.setTextFill(Color.GOLD);
		Label etiquetaPerdedor = new Label();
		etiquetaPerdedor.setText(juego.obtenerPerdedor());
		etiquetaPerdedor.setFont(Font.font("courier new", FontWeight.SEMI_BOLD, 40));
		etiquetaPerdedor.setTextFill(Color.SILVER);
		
		ArrayList<Label> etiquetas = new ArrayList<Label>();
		etiquetas.add(etiquetaGanador);
		etiquetas.add(etiquetaPerdedor);
		
		this.setPadding(new Insets(20, 20, 20, 20));
		this.setAlignment(Pos.CENTER);
//        this.setSpacing(20);
//        this.setPadding(new Insets(25));
        this.getChildren().addAll(etiquetas);
    
     }

}
