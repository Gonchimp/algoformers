package vista;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import algoformers.Algoformer;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import utilidadesDeJuego.Juego;


public class ContenedorPrincipal extends BorderPane {
	private static final int AIRE = 1;
    private static final int TIERRA = 0;
    
    private Stage stage;
    private ImageView imagen;
    private Image imagen1;
    private Image imagen2;
    private Image imagen3;
    private Image imagen4;
    private Image imagen5;
    private Image imagen6;
    private Image imagen7;
    private Image imagen8;
    private Image imagen9;
    private Image imagen10;
    private ContenedorEstadisticas estadisticas;
    private MenuBar menuBar;
    private TableroVista tableroInactivo;
    private TableroVista tableroActivo;
    private VBox contenedorActual;
    private Juego juego;
    private Scene proximaEscena;
       
    
    public ContenedorPrincipal(Stage stage,ImageView image,Juego juego,Image imageEsp,
    		Image imageRoc,Image imagePant,Image imageNube,Image imageNebulosa,Image imageTormenta,Image imageBurbuja,
    		Image imagenCanion, Image imagenFlash,Image chispa, Scene proximaEscena) throws IOException {
    	
    	this.juego = juego;
    	this.imagen = image;
    	this.imagen2=imageEsp;
    	this.imagen3=imageRoc;
    	this.imagen1=imagePant;
    	this.imagen4=imageNube;
    	this.imagen5=imageNebulosa;
    	this.imagen6=imageTormenta;
    	this.imagen7=imageBurbuja;
    	this.imagen8=imagenCanion;
    	this.imagen9=imagenFlash;
    	this.imagen10= chispa;
    	
    	this.stage = stage;
    	this.proximaEscena =proximaEscena;
        
        this.setFondo();
    	this.setTablero(imagen2,imagen3,imagen1);
    	this.setMenu();
    	this.setEstadisticas();
        this.setBotonera();
        
          	
    	
    }
    
    private void setTablero(Image imagen2,Image imagen3,Image imagen1){
    	tableroActivo = new TableroVista(Color.	BLACK,Color.BLACK,TIERRA,this.juego);
        this.setCenter(tableroActivo);
        tableroInactivo = new TableroVista(Color.WHITE,Color.WHITE,AIRE,this.juego);
    }
    
    private void setMenu(){
    	 Menu fileMenu = new Menu("Menu");
         fileMenu.getItems().add(new SeparatorMenuItem());
         MenuItem newFile2 = new MenuItem("Exit...");
         newFile2.setOnAction(e -> stage.close());
         fileMenu.getItems().add(newFile2);
         
         Menu optionMenu = new Menu("Opciones");
         MenuItem maximizar = new MenuItem("Maximize");
         MenuItem fullscreen = new MenuItem("FullScreen");
         maximizar.setOnAction(e -> stage.setMaximized(true));
         fullscreen.setOnAction(e -> stage.setFullScreen(true));
         optionMenu.getItems().addAll(maximizar, fullscreen);
         
    	 menuBar = new MenuBar();
         menuBar.getMenus().addAll(fileMenu, optionMenu);
         this.setTop(menuBar);
    }
    
    private void setFondo(){
    	this.setStyle("-fx-background-color: #336699;");
    }
        
    public void actualizarBotonera(VBox contenedor){
	   this.contenedorActual = contenedor;
	   this.setLeft(contenedorActual);
	   this.actualizarTablero();
	}
    
    public void actualizarBotonera(GridPane contenedorCuadrado){
    	this.setLeft(contenedorCuadrado);
    	this.actualizarTablero();
    }
   
    private void setBotonera() throws IOException{
    	this.contenedorActual = new ContenedorAlgoformer(this);
    	this.setLeft(contenedorActual);
    	this.actualizarTablero();
    }
    
    public void actualizarTablero(){
    	
    	tableroActivo.getChildren().clear();
    	tableroActivo.setearColores();
    	tableroActivo.actualizar(imagen,imagen2,imagen3,imagen1,imagen4, imagen5, imagen6,imagen7,imagen8, imagen9, imagen10);
    	this.setCenter(tableroActivo);
    }
    
    public void cambiarTablero(){
    	TableroVista auxiliar = tableroInactivo;
    	tableroInactivo = tableroActivo;
    	tableroActivo = auxiliar;
    }

    public void terminarTurno() throws IOException{
    	this.contenedorActual.getChildren().clear();
    	this.juego.cambiarTurno();
    	this.setBotonera();
    	PopUpTurno.display("TURNO", juego.obtenerJugadorActivo().obtenerNombre());
    	
    }
    
    public Juego getJuego(){
    	return this.juego;
    }
    
    private void setEstadisticas(){
    	estadisticas = new ContenedorEstadisticas();
    	this.setRight(estadisticas);
    }
    
    public void actualizarEstadisticas(Algoformer algoformer){
    	this.estadisticas.getChildren().clear();
    	this.estadisticas.establecerAlgoformer(algoformer);
    	this.estadisticas.actualizarEstadisticas();
    	this.setRight(estadisticas);
    }
    
    public TableroVista getTablero(){
    	return this.tableroActivo;
    }
    
    public MenuBar getMenuBar(){
    	return this.menuBar;
    }
    
    public void actualizarJuego() {
    	if(this.juego.verificarFinDeJuego()){
    		this.stage.setScene(proximaEscena);
    		
    		String ganador = this.juego.obtenerGanador();
    		
    		BackgroundImage fondo = null;
    		
    		if(ganador == "DECEPTICONS"){
    			InputStream img = null;
				try {
					img = Files.newInputStream(Paths.get("Res/images/DecepticonWin.jpg"));
				} catch (IOException e) {
				}
    			Image imagen = new Image(img);
    			fondo = new BackgroundImage(imagen, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
      		          BackgroundSize.DEFAULT);
        		        
    		}
    		if(ganador == "AUTOBOTS"){
    			InputStream img = null;
				try {
					img = Files.newInputStream(Paths.get("Res/images/AutobotWin.jpg"));
				} catch (IOException e) {
				}
    			Image imagen = new Image(img);
    			fondo = new BackgroundImage(imagen, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,  BackgroundPosition.CENTER,
      		          BackgroundSize.DEFAULT);
    		}
            
    		PopUpGanador.display("GAME OVER", "Ganaron los: " + ganador, fondo);
    		String musicFile = "Res/sounds/ganador.wav";       
    	    Media sound = new Media(new File(musicFile).toURI().toString());
    	    MediaPlayer mediaPlayer = new MediaPlayer(sound);
    	    mediaPlayer.play();
    	}
    }
    
}
