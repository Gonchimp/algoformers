package vista;

import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class ContenedorInicio extends BorderPane{
	
	private Stage stage;
	public ContenedorInicio(Stage stage,Image image,Scene escenaJuego){
		this.stage = stage;
		this.setFondo(image);
		this.setMenu(escenaJuego);
	}
	
	private void setMenu(Scene proximaEscena){
		Menu fileMenu = new Menu("Menu");
        MenuItem newFile = new MenuItem("NewGame...");
        newFile.setOnAction(e -> stage.setScene(proximaEscena));
        fileMenu.getItems().add(newFile);
        fileMenu.getItems().add(new SeparatorMenuItem());
        MenuItem newFile2 = new MenuItem("Exit...");
        newFile2.setOnAction(e -> stage.close());
        fileMenu.getItems().add(newFile2);
        
        
        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(fileMenu);
        
        this.setTop(menuBar);
	}
	
	private void setFondo(Image image){
		
		BackgroundImage backgroundImagen = new BackgroundImage(image,BackgroundRepeat.REPEAT,BackgroundRepeat.REPEAT,BackgroundPosition.DEFAULT,BackgroundSize.DEFAULT);
		Background background = new Background(backgroundImagen);
		this.setBackground(background);
		this.stage.setMaximized(true);
	}
}
