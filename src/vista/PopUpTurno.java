package vista;

import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.*;

public class PopUpTurno {
	
	public static void display(String titulo, String mensaje){
		
		Stage window = new Stage();
		
		window.initModality(Modality.APPLICATION_MODAL);
		window.setTitle(titulo);
		window.setMinWidth(300);
		window.setMaxWidth(300);
		window.setMinHeight(150);
		window.setMaxHeight(150);
	
		
		Label label = new Label();
		label.setText(mensaje);
		label.setTextFill(Color.BLACK);
		label.setFont(Font.font("courier new", FontWeight.SEMI_BOLD,20));

		Button aceptarButton = new Button ("Aceptar");
		
		aceptarButton.setOnAction(e -> window.close());
		
		VBox layout = new VBox(10);
		layout.getChildren().addAll(label, aceptarButton);
		layout.setAlignment(Pos.CENTER);
		layout.autosize();
		
		Scene scene = new Scene(layout);
		window.setScene(scene);
		window.showAndWait();
	}
	
}