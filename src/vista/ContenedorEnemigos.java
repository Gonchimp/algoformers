package vista;

import java.util.ArrayList;
import java.util.LinkedList;

import EventoVista.BotonAtrasHandler;
import EventoVista.BotonCambiarTableroHandler;
import EventoVista.BotonEnemigoHandler;
import algoformers.Algoformer;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import utilidadesDeJuego.Juego;
import utilidadesDeJuego.Jugador;

public class ContenedorEnemigos extends VBox {
	
	private ContenedorPrincipal contenedorPrincipal;
	private VBox anterior;
	private Jugador atacante;
	
	public ContenedorEnemigos(ContenedorPrincipal contenedorPrincipal,Jugador jugador,VBox anterior){
		this.contenedorPrincipal = contenedorPrincipal;
		this.anterior = anterior;
		this.atacante = jugador;
		this.setAlgoformers(contenedorPrincipal.getJuego());
	}
	
	private void setAlgoformers(Juego juego){
		Jugador jugador = juego.obtenerJugadorNoActivo();
		LinkedList<Algoformer> algoformers = jugador.obtenerListaDeAlgoformers();
		ArrayList<Button> botones = new ArrayList<Button>();
		
		for(Algoformer algoformer : algoformers){
			Button botonEnemigo= new Button(algoformer.obtenerNombre());
			BotonEnemigoHandler botonEnemigoHandler = new BotonEnemigoHandler(this.atacante, algoformer);
			botonEnemigo.setOnAction(botonEnemigoHandler);
			botones.add(botonEnemigo);
			}
		
    	
    	Button botonAtras = new Button("Atras");
    	Button botonCambiarTablero = new Button("Cambiar tablero");
	        	    	
    	BotonAtrasHandler botonAtrasHandler = new BotonAtrasHandler(this.contenedorPrincipal,this.anterior);
    	BotonCambiarTableroHandler botonCambiarTableroHandler = new BotonCambiarTableroHandler(this.contenedorPrincipal);
	        	    	
       	botonAtras.setOnAction(botonAtrasHandler);
    	botonCambiarTablero.setOnAction(botonCambiarTableroHandler);
	  	    	
       	botones.add(botonCambiarTablero);  
       	botones.add(botonAtras);
	       	
    	this.setStyle("-fx-background-color: #336699;");
        this.setPadding(new Insets(20, 20, 20, 20));
        this.setAlignment(Pos.CENTER);
        this.setSpacing(20);
        this.setPadding(new Insets(25));
        this.getChildren().addAll(botones);
	}

	
}
