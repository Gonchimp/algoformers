package vista;

import javafx.scene.paint.Color;
import utilidadesDeJuego.Juego;
import utilidadesDeJuego.Posicion;
import javafx.scene.shape.Circle;

public class VistaDeAlgoformers {
	
	private static final Color colorOptimus = Color.BLUE;
	private static final Color colorBumblebee = Color.YELLOW;
	private static final Color colorRatchet = Color.GREEN;
	private static final Color colorMegatron = Color.RED;
	private static final Color colorBonecrusher = Color.BLUEVIOLET;
	private static final Color colorFrenzy = Color.WHITE;
	private static final Color colorSuperion = Color.AQUA;
	private static final Color colorMenasor = Color.CORAL;
	private static final int CIRCULO = 10;
	private TableroVista tablero;
	private Juego juego;
	
	public VistaDeAlgoformers(TableroVista tablero, Juego juego){
		this.tablero = tablero;
		this.juego = juego;
	}
	
	public void actualizarVista(){
		Posicion posOptimus = juego.obtenerPosicionDeAutobot("Optimus Prime");
		if(posOptimus.getZ() == tablero.obtenerCoordenadaZ()){
			this.tablero.add(new Circle(CIRCULO,colorOptimus), posOptimus.getX(),posOptimus.getY());
		}
		Posicion posBumblebee = juego.obtenerPosicionDeAutobot("Bumblebee");
		if(posBumblebee.getZ() == tablero.obtenerCoordenadaZ()){
			this.tablero.add(new Circle(CIRCULO,colorBumblebee), posBumblebee.getX(),posBumblebee.getY());
		}
		Posicion posRatchet = juego.obtenerPosicionDeAutobot("Ratchet");
		if(posRatchet.getZ() == tablero.obtenerCoordenadaZ()){
			this.tablero.add(new Circle(CIRCULO,colorRatchet), posRatchet.getX(),posRatchet.getY());
		}
		Posicion posMegatron = juego.obtenerPosicionDeDecepticon("Megatron");
		if(posMegatron.getZ() == tablero.obtenerCoordenadaZ()){
			this.tablero.add(new Circle(CIRCULO,colorMegatron), posMegatron.getX(),posMegatron.getY());
		}
		Posicion posBonecrusher = juego.obtenerPosicionDeDecepticon("Bonecrusher");
		if(posBonecrusher.getZ() == tablero.obtenerCoordenadaZ()){
			this.tablero.add(new Circle(CIRCULO,colorBonecrusher), posBonecrusher.getX(),posBonecrusher .getY());
		}
		Posicion posFrenzy = juego.obtenerPosicionDeDecepticon("Frenzy");
		if(posFrenzy.getZ() == tablero.obtenerCoordenadaZ()){
			this.tablero.add(new Circle(CIRCULO,colorFrenzy), posFrenzy.getX(),posFrenzy.getY());
		}
		Posicion posSuperion = juego.obtenerPosicionDeAutobot("Superion");
		if(posSuperion.getZ() == tablero.obtenerCoordenadaZ()){
			this.tablero.add(new Circle(CIRCULO,colorSuperion), posSuperion.getX(),posSuperion.getY());
		}
		Posicion posMenasor = juego.obtenerPosicionDeDecepticon("Menasor");
		if(posMenasor.getZ() == tablero.obtenerCoordenadaZ()){
			this.tablero.add(new Circle(CIRCULO,colorMenasor), posMenasor.getX(),posMenasor.getY());
		}
	}
	
}
