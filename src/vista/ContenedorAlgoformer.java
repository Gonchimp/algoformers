package vista;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import EventoVista.BotonAlgoformerHandler;
import EventoVista.BotonCambiarTableroHandler;
import EventoVista.BotonTerminarTurnoHandler;
import algoformers.Algoformer;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;

import javafx.scene.layout.VBox;

import utilidadesDeJuego.Juego;
import utilidadesDeJuego.Jugador;



public class ContenedorAlgoformer extends VBox {

	private ContenedorPrincipal contenedorPrincipal;
			
	public ContenedorAlgoformer(ContenedorPrincipal contenedorPrincipal) throws IOException{
		this.contenedorPrincipal = contenedorPrincipal;
		this.setAlgoformers(contenedorPrincipal.getJuego());
	}
	
	private void setAlgoformers(Juego juego) throws IOException{
		Jugador jugador = juego.obtenerJugadorActivo();
		LinkedList<Algoformer> algoformers = jugador.obtenerListaDeAlgoformers();
		ArrayList<Button> botones = new ArrayList<Button>();
		
		for(Algoformer algoformer : algoformers){
		Button botonAlgoformer= new Button(algoformer.obtenerNombre());
		BotonAlgoformerHandler botonAlgoformerHandler = new BotonAlgoformerHandler(this.contenedorPrincipal,this,
				jugador, algoformer.obtenerNombre());
		botonAlgoformer.setOnAction(botonAlgoformerHandler);
		botones.add(botonAlgoformer);
		}
    	
    	Button botonCambiarTablero = new Button("Cambiar tablero");
	    Button botonTerminarTurno = new Button("Terminar Turno");
    	    	
    	BotonCambiarTableroHandler botonCambiarTableroHandler = new BotonCambiarTableroHandler(this.contenedorPrincipal);
	    BotonTerminarTurnoHandler botonTerminarTurnoHandler = new BotonTerminarTurnoHandler(this.contenedorPrincipal);

    	botonCambiarTablero.setOnAction(botonCambiarTableroHandler);
	  	botonTerminarTurno.setOnAction(botonTerminarTurnoHandler);
    	
      	botones.add(botonCambiarTablero);  
	    botones.add(botonTerminarTurno);
    	
    	this.setStyle("-fx-background-color: #336699;");
        this.setPadding(new Insets(20, 20, 20, 20));
        this.setAlignment(Pos.CENTER);
        this.setSpacing(20);
        this.setPadding(new Insets(25));
        this.getChildren().addAll(botones);
	}
	
	
	
}
