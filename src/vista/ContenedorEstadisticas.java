package vista;

import java.util.ArrayList;

import algoformers.Algoformer;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;


public class ContenedorEstadisticas extends VBox{
	
	private static final Color colorOptimus = Color.BLUE;
	private static final Color colorBumblebee = Color.YELLOW;
	private static final Color colorRatchet = Color.DARKGREEN;
	private static final Color colorMegatron = Color.RED;
	private static final Color colorBonecrusher = Color.BLUEVIOLET;
	private static final Color colorFrenzy = Color.WHITE;
	private static final Color colorSuperion = Color.AQUA;
	private static final Color colorMenasor = Color.CORAL;
	private static final Color info = Color.BLACK;
	
	private Algoformer algoformer;
	
	public ContenedorEstadisticas(){
		this.setEstadisticas();
	}
	
	private void setEstadisticas(){
		Label labelOptimus = new Label();
		labelOptimus.setText("Optimus Prime");
		labelOptimus.setTextFill(colorOptimus);
		labelOptimus.setFont(Font.font("courier new", FontWeight.SEMI_BOLD, 20));
		Label labelBumblebee = new Label();
		labelBumblebee.setText("Bumblebee");
		labelBumblebee.setTextFill(colorBumblebee);
		labelBumblebee.setFont(Font.font("courier new", FontWeight.SEMI_BOLD, 20));
		Label labelRatchet = new Label();
		labelRatchet.setText("Ratchet");
		labelRatchet.setTextFill(colorRatchet);
		labelRatchet.setFont(Font.font("courier new", FontWeight.SEMI_BOLD, 20));
		Label labelMegatron = new Label();
		labelMegatron.setText("Megatron");
		labelMegatron.setTextFill(colorMegatron);
		labelMegatron.setFont(Font.font("courier new", FontWeight.SEMI_BOLD, 20));
		Label labelBonecrusher = new Label();
		labelBonecrusher.setText("Bonecrusher");
		labelBonecrusher.setTextFill(colorBonecrusher);
		labelBonecrusher.setFont(Font.font("courier new", FontWeight.SEMI_BOLD, 20));
		Label labelFrenzy = new Label();
		labelFrenzy.setText("Frenzy");
		labelFrenzy.setTextFill(colorFrenzy);
		labelFrenzy.setFont(Font.font("courier new", FontWeight.SEMI_BOLD, 20));
		Label labelSuperion = new Label();
		labelSuperion.setText("Superion");
		labelSuperion.setTextFill(colorSuperion);
		labelSuperion.setFont(Font.font("courier new", FontWeight.SEMI_BOLD, 20));
		Label labelMenasor = new Label();
		labelMenasor.setText("Menasor");
		labelMenasor.setTextFill(colorMenasor);
		labelMenasor.setFont(Font.font("courier new", FontWeight.SEMI_BOLD, 20));
		
		ArrayList<Label> etiquetas = new ArrayList<Label>();
		etiquetas.add(labelOptimus);
		etiquetas.add(labelBumblebee);
		etiquetas.add(labelRatchet);
		etiquetas.add(labelMegatron);
		etiquetas.add(labelBonecrusher);
		etiquetas.add(labelFrenzy);
		etiquetas.add(labelSuperion);
		etiquetas.add(labelMenasor);
		
		this.setPadding(new Insets(20, 20, 20, 20));
        this.setAlignment(Pos.TOP_RIGHT);
        this.setSpacing(20);
        this.setPadding(new Insets(25));
		this.getChildren().addAll(etiquetas);
	}
	
	public void establecerAlgoformer(Algoformer algoformer){
		this.algoformer = algoformer;
	}
	
	public void actualizarEstadisticas(){
		
		this.setEstadisticas();
		
		Label nombre = new Label();
		nombre.setText(this.algoformer.obtenerNombre());
		nombre.setTextFill(info);
		nombre.setFont(Font.font("courier new", FontWeight.SEMI_BOLD, 20));
		Label modo = new Label();
		modo.setText(this.algoformer.pedirModoActivo().obtenerNombreModo());
		modo.setTextFill(info);
		modo.setFont(Font.font("courier new", FontWeight.SEMI_BOLD, 20));
		Label vida = new Label();
		vida.setText("VIDA : " + String.valueOf(this.algoformer.obtenerVida()));
		vida.setTextFill(info);
		vida.setFont(Font.font("courier new", FontWeight.SEMI_BOLD, 20));
		Label ataque = new Label();
		ataque.setText("ATAQUE : " + String.valueOf(this.algoformer.obtenerAtaque()));
		ataque.setTextFill(info);
		ataque.setFont(Font.font("courier new", FontWeight.SEMI_BOLD, 20));
		Label distanciaAtaque = new Label();
		distanciaAtaque.setText("DISTANCIA DE ATAQUE :"+String.valueOf(this.algoformer.obtenerDistanciaDeAtaque()));
		distanciaAtaque.setTextFill(info);
		distanciaAtaque.setFont(Font.font("courier new", FontWeight.SEMI_BOLD, 20));
		Label velocidad = new Label();
		velocidad.setText("VELOCIDAD :" + String.valueOf(this.algoformer.obtenerVelocidad()));
		velocidad.setTextFill(info);
		velocidad.setFont(Font.font("courier new", FontWeight.SEMI_BOLD, 20));
		Label movimientosRestantes = new Label();
		movimientosRestantes.setText("MOV. RESTANTES :" + String.valueOf(this.algoformer.getMovRestantes()));
		movimientosRestantes.setTextFill(info);
		movimientosRestantes.setFont(Font.font("courier new", FontWeight.SEMI_BOLD, 20));
		
		
		ArrayList<Label> etiquetas = new ArrayList<Label>();
		etiquetas.add(nombre);
		etiquetas.add(modo);
		etiquetas.add(vida);
		etiquetas.add(ataque);
		etiquetas.add(distanciaAtaque);
		etiquetas.add(velocidad);
		etiquetas.add(movimientosRestantes);
		
		this.getChildren().addAll(etiquetas);
	}
	
}
