package vista;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import utilidadesDeJuego.Juego;

public class Aplicacion extends Application {
	
	Stage window;
	private Juego juego;
    
	public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
    	
        window = primaryStage;
        window.setTitle("Transformers");
        
        this.crearModelo();
        
        InputStream is2=Files.newInputStream(Paths.get("res/images/descarga3.jpg"));
        Image img2 =new Image(is2);
        is2.close();
        ImageView imgView2=new ImageView(img2);
        imgView2.setFitWidth(30);
        imgView2.setFitHeight(30);
        
        //////////////////nuevo Superficies/////////////
        InputStream is0=Files.newInputStream(Paths.get("res/images/espinas.jpg"));
        Image img0 =new Image(is0);
        is0.close();

        
        InputStream is1=Files.newInputStream(Paths.get("res/images/pantano.jpg"));
        Image img1 =new Image(is1);
        is1.close();
        
        InputStream is3=Files.newInputStream(Paths.get("res/images/rocosa.jpg"));
        Image img3 =new Image(is3);
        is3.close();
        
        InputStream is4=Files.newInputStream(Paths.get("res/images/nube.jpg"));
        Image img4 =new Image(is4);
        is4.close();

        
        InputStream is5=Files.newInputStream(Paths.get("res/images/nebulosa.jpg"));
        Image img5 =new Image(is5);
        is5.close();
        
        InputStream is6=Files.newInputStream(Paths.get("res/images/tormenta.jpg"));
        Image img6 =new Image(is6);
        is6.close();
        
        InputStream is7=Files.newInputStream(Paths.get("res/images/burbuja.png"));
        Image img7 =new Image(is7);
        is7.close();
        
        InputStream is8=Files.newInputStream(Paths.get("res/images/canionDoble.png"));
        Image img8 =new Image(is8);
        is8.close();
        
        InputStream is9=Files.newInputStream(Paths.get("res/images/flash.png"));
        Image img9 =new Image(is9);
        is9.close();
        
        InputStream is10=Files.newInputStream(Paths.get("res/images/chispa.jpg"));
        Image chispa=new Image(is10);
        is10.close();

        /////////////////////////////////////////////////
        
        InputStream is11=Files.newInputStream(Paths.get("res/images/fin.jpg"));
        Image fin=new Image(is11);
        is11.close();
        
        ImageView imagenFin=new ImageView(fin);
              
        ContenedorFin contenedorFin = new ContenedorFin(window,imagenFin,juego);
        Scene escenaFin = new Scene(contenedorFin,800,600);
        
        ContenedorPrincipal contenedorPrincipal = new ContenedorPrincipal(window,imgView2,juego,img0,img3,img1,img4,img5,img6,img7,img8,img9,chispa,escenaFin);
        Scene escenaJuego = new Scene (contenedorPrincipal,800,600);
        
        InputStream is=Files.newInputStream(Paths.get("res/images/transformers-background.jpg"));
        Image img =new Image(is);
        is.close();
        
        ContenedorInicio contenedorInicio = new ContenedorInicio(window,img,escenaJuego);
        Scene escenaInicio = new Scene(contenedorInicio,800, 600);
            
        window.setScene(escenaInicio);
        window.show();
        
        
    }
    
    private void crearModelo(){
    	this.juego = new Juego();
      }
	
}