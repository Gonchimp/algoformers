package EventoVista;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import vista.ContenedorPrincipal;

public class BotonCambiarTableroHandler implements EventHandler<ActionEvent>{

	private ContenedorPrincipal contenedorPrincipal;
	
	public BotonCambiarTableroHandler(ContenedorPrincipal cp){
		this.contenedorPrincipal = cp;
	}

	@Override
	public void handle(ActionEvent event) {
		this.contenedorPrincipal.cambiarTablero();
		this.contenedorPrincipal.actualizarTablero();
	}
}
