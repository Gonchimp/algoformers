package EventoVista;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.VBox;
import utilidadesDeJuego.Jugador;
import vista.ContenedorOpciones;
import vista.ContenedorPrincipal;


public class BotonAlgoformerHandler implements EventHandler<ActionEvent>{

	private ContenedorPrincipal contenedorPrincipal;
	private ContenedorOpciones siguiente;
	private Jugador jugador;
	private String nombre;
	private VBox anterior;
	
	public BotonAlgoformerHandler (ContenedorPrincipal contenedorPrincipal,VBox anterior,
									Jugador jugador,String nombreDeAlgoformer) throws IOException{
		this.contenedorPrincipal = contenedorPrincipal;
		this.jugador = jugador;
		this.anterior = anterior;
		this.nombre = nombreDeAlgoformer;
	}
	
	@Override
	public void handle(ActionEvent event) {
		this.jugador.establecerAlgoformerActivo(this.nombre);
		this.contenedorPrincipal.actualizarEstadisticas(jugador.obtenerAlgoformerActivo());
		try {
			this.siguiente = new ContenedorOpciones (this.contenedorPrincipal, this.anterior);
		} catch (IOException e) {
		}
		this.contenedorPrincipal.actualizarBotonera(siguiente);
	}

}
