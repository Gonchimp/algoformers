package EventoVista;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import java.io.File;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import utilidadesDeJuego.Jugador;
import vista.ContenedorPrincipal;

public class BotonArribaIzquierdaHandler implements EventHandler<ActionEvent>{
	
	private Jugador jugador;
	private ContenedorPrincipal contenedorPrincipal;
		
	public BotonArribaIzquierdaHandler(ContenedorPrincipal cp,Jugador jugador){
		this.contenedorPrincipal = cp;
		this.jugador = jugador;
		
	}

	@Override
	public void handle(ActionEvent event) {
		this.jugador.moverInferiorIzquierda();
		this.contenedorPrincipal.actualizarTablero();
		String musicFile = "Res/sounds/mover.mp3";      
	    Media sound = new Media(new File(musicFile).toURI().toString());
	    MediaPlayer mediaPlayer = new MediaPlayer(sound);
	    mediaPlayer.play();
		this.contenedorPrincipal.actualizarEstadisticas(jugador.obtenerAlgoformerActivo());
		this.contenedorPrincipal.actualizarJuego();
		
	}

}