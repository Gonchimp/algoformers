package EventoVista;

import java.io.File;

import excepciones.ImposibleTransformarException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import utilidadesDeJuego.Jugador;
import vista.ContenedorPrincipal;


public class BotonTransformarHandler implements EventHandler<ActionEvent> {
	 private  Jugador jugador;
	 private ContenedorPrincipal contenedorPrincipal;
	
	 public BotonTransformarHandler(ContenedorPrincipal cp, Jugador jugador){
		this.jugador = jugador;
		this.contenedorPrincipal = cp;
	}
	
	@Override
	public void handle(ActionEvent event) {
		try{
		this.jugador.transformar();
		String musicFile = "Res/sounds/transformar.mp3";     
	    Media sound = new Media(new File(musicFile).toURI().toString());
	    MediaPlayer mediaPlayer = new MediaPlayer(sound);
	    mediaPlayer.play();
		}
		catch (ImposibleTransformarException e){
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Información de juego");
			alert.setHeaderText("Ha ocurrido un problema al transformar el algoformer");
			alert.setContentText("Verifique que el algoformer tenga suficiente espacio para Transformarse."
					+ "O bien si el algoformer requiere de otros para transformarse, verifique que esten lo suficientemente cerca");
			alert.showAndWait();
		}
		this.contenedorPrincipal.actualizarEstadisticas(jugador.obtenerAlgoformerActivo());
		this.contenedorPrincipal.actualizarTablero();
		this.contenedorPrincipal.actualizarJuego();
	}

}
