package EventoVista;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import java.io.File;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import utilidadesDeJuego.Jugador;
import vista.ContenedorPrincipal;

public class BotonAbajoHandler implements EventHandler<ActionEvent>{
	
	private Jugador jugador;
	private ContenedorPrincipal contenedorPrincipal;
		
	public BotonAbajoHandler(ContenedorPrincipal cp,Jugador jugador){
		this.contenedorPrincipal = cp;
		this.jugador = jugador;
		
	}

	@Override
	public void handle(ActionEvent event) {
		this.jugador.moverArriba();
		String musicFile = "Res/sounds/mover.mp3";       
	    Media sound = new Media(new File(musicFile).toURI().toString());
	    MediaPlayer mediaPlayer = new MediaPlayer(sound);
	    mediaPlayer.play();
	    this.contenedorPrincipal.actualizarTablero();
		this.contenedorPrincipal.actualizarEstadisticas(jugador.obtenerAlgoformerActivo());
		this.contenedorPrincipal.actualizarJuego();
		}

}
