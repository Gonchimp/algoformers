package EventoVista;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.VBox;
import vista.ContenedorPrincipal;

public class BotonAtrasHandler implements EventHandler<ActionEvent>{

	private ContenedorPrincipal contenedorPrincipal;
	private VBox anterior;
	
	public BotonAtrasHandler(ContenedorPrincipal contenedorPrincipal,VBox anterior){
		this.contenedorPrincipal = contenedorPrincipal;
		this.anterior = anterior;
	}
	
	@Override
	public void handle(ActionEvent event) {
		contenedorPrincipal.actualizarBotonera(this.anterior);		
	}

}
