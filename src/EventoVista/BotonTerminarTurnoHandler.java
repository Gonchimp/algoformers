package EventoVista;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import vista.ContenedorPrincipal;

public class BotonTerminarTurnoHandler implements EventHandler<ActionEvent>{
	
	private ContenedorPrincipal contenedorPrincipal;
		
	
	public BotonTerminarTurnoHandler(ContenedorPrincipal cp){
		this.contenedorPrincipal = cp;
	}
	
	@Override
	public void handle(ActionEvent event) {
		try {
			this.contenedorPrincipal.terminarTurno();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.contenedorPrincipal.actualizarTablero();
		this.contenedorPrincipal.actualizarJuego();
	}

}
