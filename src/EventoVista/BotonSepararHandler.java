package EventoVista;

import java.io.File;
import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import utilidadesDeJuego.Jugador;
import vista.ContenedorAlgoformer;
import vista.ContenedorPrincipal;

public class BotonSepararHandler implements EventHandler<ActionEvent>{

	private ContenedorPrincipal contenedorPrincipal;
	private ContenedorAlgoformer siguiente;
	private Jugador jugador;
	
	
	public BotonSepararHandler(ContenedorPrincipal cp, Jugador jugador){
		this.contenedorPrincipal = cp;
		this.jugador = jugador;
		
	}
	
	@Override
	public void handle(ActionEvent event) {
		this.jugador.separar();
		String musicFile = "Res/sounds/separar.mp3";     
	    Media sound = new Media(new File(musicFile).toURI().toString());
	    MediaPlayer mediaPlayer = new MediaPlayer(sound);
	    mediaPlayer.play();
		try {
			this.siguiente = new ContenedorAlgoformer(this.contenedorPrincipal);
		} catch (IOException e) {
		}
		this.contenedorPrincipal.actualizarBotonera(siguiente);
	}

}


