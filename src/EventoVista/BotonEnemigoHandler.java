package EventoVista;

import java.io.File;

import algoformers.Algoformer;
import excepciones.ObjetivoFueraDeRangoException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import utilidadesDeJuego.Jugador;

public class BotonEnemigoHandler implements EventHandler<ActionEvent>{
	
	private Jugador atacante;
	private Algoformer enemigo;
	
	public BotonEnemigoHandler(Jugador atacante, Algoformer enemigo){
		this.atacante = atacante;
		this.enemigo = enemigo;
	}

	@Override
	public void handle(ActionEvent event) {
		try{
		this.atacante.atacar(this.enemigo);
		String musicFile = "Res/sounds/atacar.wav";     
	    Media sound = new Media(new File(musicFile).toURI().toString());
	    MediaPlayer mediaPlayer = new MediaPlayer(sound);
	    mediaPlayer.play();
		}
		catch (ObjetivoFueraDeRangoException e){
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Información de juego");
			alert.setHeaderText("Ha ocurrido un problema al atacar a tu enemigo");
			alert.setContentText("El enemigo se encuentre demasiado lejos, mira en las estadisticas tu rango de ataque");
			alert.showAndWait();
		}
	}
	
	

}
