package EventoVista;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.VBox;
import utilidadesDeJuego.Jugador;
import vista.ContenedorEnemigos;
import vista.ContenedorPrincipal;

public class BotonAtacarHandler implements EventHandler<ActionEvent>{
	
	private ContenedorPrincipal contenedorPrincipal;
	private ContenedorEnemigos siguiente;
	
	public BotonAtacarHandler(ContenedorPrincipal cp,VBox anterior,Jugador jugador){
		this.contenedorPrincipal = cp;
		this.siguiente = new ContenedorEnemigos(cp,jugador,anterior);
	}

	@Override
	public void handle(ActionEvent event) {
		this.contenedorPrincipal.actualizarBotonera(this.siguiente);
	}

}
