package EventoVista;

import java.io.File;
import java.io.IOException;

import excepciones.ImposibleTransformarException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import utilidadesDeJuego.Jugador;
import vista.ContenedorAlgoformer;
import vista.ContenedorPrincipal;

public class BotonCombinarHandler implements EventHandler<ActionEvent>{

	private ContenedorPrincipal contenedorPrincipal;
	private ContenedorAlgoformer siguiente;
	private Jugador jugador;
	
	
	public BotonCombinarHandler(ContenedorPrincipal cp, Jugador jugador){
		this.contenedorPrincipal = cp;
		this.jugador = jugador;
		
	}
	
	@Override
	public void handle(ActionEvent event) {
		try{
		this.jugador.combinar();
		String musicFile = "Res/sounds/combinar.mp3";     
	    Media sound = new Media(new File(musicFile).toURI().toString());
	    MediaPlayer mediaPlayer = new MediaPlayer(sound);
	    mediaPlayer.play();
		}
		catch (ImposibleTransformarException e){
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Información de juego");
			alert.setHeaderText("Ha ocurrido un problema al transformar el algoformer");
			alert.setContentText("Verifique que el algoformer tenga suficiente espacio para Transformarse."
					+ "O bien si el algoformer requiere de otros para transformarse, verifique que esten lo suficientemente cerca");
			alert.showAndWait();
		}
		try {
			this.siguiente = new ContenedorAlgoformer(this.contenedorPrincipal);
		} catch (IOException e) {
		}
		this.contenedorPrincipal.actualizarBotonera(siguiente);
	}

}
