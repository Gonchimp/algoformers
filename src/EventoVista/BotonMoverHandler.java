package EventoVista;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.VBox;
import utilidadesDeJuego.Jugador;
import vista.ContenedorMovimiento;
import vista.ContenedorPrincipal;

public class BotonMoverHandler implements EventHandler<ActionEvent>{

	private ContenedorPrincipal contenedorPrincipal;
	private ContenedorMovimiento siguiente;
	private Jugador jugador;
	
	
	public BotonMoverHandler(ContenedorPrincipal cp, VBox anterior,Jugador jugador) throws IOException{
		this.siguiente = new ContenedorMovimiento(cp,anterior);
		this.contenedorPrincipal = cp;
		this.jugador = jugador;
	}
	@Override
	public void handle(ActionEvent event) {
		this.jugador.mover();
		this.contenedorPrincipal.actualizarBotonera(this.siguiente);
	}

}
