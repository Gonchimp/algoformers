package algoformers;


import bonus.Fusion;
import excepciones.AtaqueAlMismoEquipoException;
import excepciones.ImposibleTransformarException;
import interfaces.Atacable;
import utilidadesDeJuego.Jugador;
import utilidadesDeJuego.Tablero;

public abstract class Autobot extends Algoformer {
	
	public Autobot(){
		super();
	}
	
	public abstract void recibirDanio(Decepticon decepticon);
		
    protected void recibirDanioAlgoformerUnico(Decepticon decepticon){
		if(! this.modificadores.burbujaActiva()){
			this.puntosDeVida -= decepticon.obtenerAtaque();
		}
        
	}
	
	public void recibirDanio(Autobot autobot){
		throw new AtaqueAlMismoEquipoException();
	}
	
	
	public void atacarAtacable(Atacable decepticon){
		decepticon.recibirDanio(this);
	}
	
	public void combinarConAlgoformers(Algoformer algoformer1, Algoformer algoformer2,Tablero tablero,Fusion fusion){
		if(this.fusionDisponible(algoformer1, algoformer2)){
		Superion superion = new Superion();
		superion.insertarAlgoformer(algoformer1);
		superion.insertarAlgoformer(algoformer2);
		superion.insertarAlgoformer(this);
		fusion.establecerAlgoformerDeFusion(superion);
		fusion.aplicar(tablero,this);
		}
		else{
			throw new ImposibleTransformarException();
		}
	}
	
	public void morirse(Jugador jugador, Tablero tablero){
		if(this.puntosDeVida <=0){
			jugador.eliminarAlgoformer(this.nombre);
			tablero.eliminar(this);
			jugador.abortar();
		}
	}
	
	public void separarse(Tablero tablero, Jugador jugador){
		
	}
}
