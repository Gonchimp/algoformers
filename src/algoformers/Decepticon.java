package algoformers;

import bonus.Fusion;
import excepciones.AtaqueAlMismoEquipoException;
import excepciones.ImposibleTransformarException;
import interfaces.Atacable;
import utilidadesDeJuego.Jugador;
import utilidadesDeJuego.Tablero;

public abstract class Decepticon extends Algoformer{
	
	public Decepticon(){
		super();
	}
	
	public abstract void recibirDanio(Autobot autobot);
	
	protected  void recibirDanioAlgoformerUnico(Autobot autobot){
		if(! this.modificadores.burbujaActiva()){
			this.puntosDeVida -= autobot.obtenerAtaque();
		}
        
	}
	
	public void recibirDanio(Decepticon decepticon){
		throw new AtaqueAlMismoEquipoException();
	}
	
	public void atacarAtacable(Atacable autobot){
		autobot.recibirDanio(this);
	}
	public void combinarConAlgoformers(Algoformer algoformer1, Algoformer algoformer2,Tablero tablero,Fusion fusion){
		if(this.fusionDisponible(algoformer1, algoformer2)){
		Menasor menasor= new Menasor();
		menasor.insertarAlgoformer(algoformer1);
		menasor.insertarAlgoformer(algoformer2);
		menasor.insertarAlgoformer(this);
		fusion.establecerAlgoformerDeFusion(menasor);
		fusion.aplicar(tablero,this);
		
		}
		else{
			throw new ImposibleTransformarException();
		}
	}
	
	public void morirse(Jugador jugador, Tablero tablero){
		if(this.puntosDeVida <=0){
			jugador.eliminarAlgoformer(this.nombre);
			tablero.eliminar(this);
			jugador.abortar();
		}
	}
	
	public void separarse(Tablero tablero, Jugador jugador){
		
	}

}
