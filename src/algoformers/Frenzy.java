package algoformers;

import utilidadesDeJuego.Jugador;

public class Frenzy extends Decepticon {

	private static String NOMBRE = "Frenzy";
	private static double VIDA = 400;
	private static double ATAQUE_HUMANOIDE = 10;
	private static int ATAQUE_HUMANOIDE_DISTANCIA = 5;
	private static int VELOCIDAD_HUMANOIDE = 2;
	private static double ATAQUE_ALTERNO = 25;
	private static int ATAQUE_ALTERNO_DISTANCIA = 2;
	private static int VELOCIDAD_ALTERNO = 6;

	public Frenzy(){
		super();
		this.nombre = NOMBRE;
		this.puntosDeVida = VIDA;
		this.modoActivo = new ModoHumanoide (ATAQUE_HUMANOIDE,ATAQUE_HUMANOIDE_DISTANCIA,VELOCIDAD_HUMANOIDE);
		this.modoInactivo = new ModoAlternoTerrestre (ATAQUE_ALTERNO,ATAQUE_ALTERNO_DISTANCIA,VELOCIDAD_ALTERNO);
	}
	
	
	public void modificarVidaPorcentual(double modificador) {
		this.modificarVidaPorcentualAlgoformerUnico(modificador);
	}


	@Override
	public void recibirDanio(Autobot autobot) {
		this.recibirDanioAlgoformerUnico(autobot);
		
	}
	
	@Override
	public void abortar(Jugador jugador) {
		this.abortarFusion();
		
	}
	
}
