package algoformers;

import utilidadesDeJuego.Jugador;

public class Ratchet extends Autobot {

	private static String NOMBRE = "Ratchet";
	private static double VIDA = 150;
	private static double ATAQUE_HUMANOIDE = 5;
	private static int ATAQUE_HUMANOIDE_DISTANCIA = 5;
	private static int VELOCIDAD_HUMANOIDE = 1;
	private static double ATAQUE_ALTERNO = 35;
	private static int ATAQUE_ALTERNO_DISTANCIA = 2;
	private static int VELOCIDAD_ALTERNO = 8;

	public Ratchet(){
		super();
		this.nombre = NOMBRE;
		this.puntosDeVida = VIDA;
		this.modoActivo = new ModoHumanoide (ATAQUE_HUMANOIDE,ATAQUE_HUMANOIDE_DISTANCIA,VELOCIDAD_HUMANOIDE);
		this.modoInactivo = new ModoAlternoAereo (ATAQUE_ALTERNO,ATAQUE_ALTERNO_DISTANCIA,VELOCIDAD_ALTERNO);
	}
	
	public void recibirDanio(Decepticon decepticon) {
		this.recibirDanioAlgoformerUnico(decepticon);
	}

	
	public void modificarVidaPorcentual(double modificador) {
		this.modificarVidaPorcentualAlgoformerUnico(modificador);
	}
	
	@Override
	public void abortar(Jugador jugador) {
		this.abortarFusion();
		
	}
	
}
