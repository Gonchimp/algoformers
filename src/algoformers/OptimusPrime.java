package algoformers;

import utilidadesDeJuego.Jugador;

public class OptimusPrime extends Autobot {
	
	private static String NOMBRE = "Optimus Prime";
	private static double VIDA = 500;
	private static double ATAQUE_HUMANOIDE = 50;
	private static int ATAQUE_HUMANOIDE_DISTANCIA = 2;
	private static int VELOCIDAD_HUMANOIDE = 2;
	private static double ATAQUE_ALTERNO = 15;
	private static int ATAQUE_ALTERNO_DISTANCIA = 4;
	private static int VELOCIDAD_ALTERNO = 5;
	
	public OptimusPrime(){
		super();
		this.nombre = NOMBRE;
		this.puntosDeVida = VIDA;
		this.modoActivo = new ModoHumanoide (ATAQUE_HUMANOIDE,ATAQUE_HUMANOIDE_DISTANCIA,VELOCIDAD_HUMANOIDE);
		this.modoInactivo = new ModoAlternoTerrestre (ATAQUE_ALTERNO,ATAQUE_ALTERNO_DISTANCIA,VELOCIDAD_ALTERNO);
	}

	
	public void recibirDanio(Decepticon decepticon) {
		this.recibirDanioAlgoformerUnico(decepticon);
	}
	
	public void modificarVidaPorcentual(double modificador) {
		this.modificarVidaPorcentualAlgoformerUnico(modificador);
	}
	
	@Override
	public void abortar(Jugador jugador) {
		this.abortarFusion();
		
	}
	
	
}
