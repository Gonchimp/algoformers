package algoformers;

import excepciones.MovimientoImposibleException;
import interfaces.Capturable;
import utilidadesDeJuego.Casillero;
import utilidadesDeJuego.Posicion;
import utilidadesDeJuego.Tablero;

public class ModoHumanoide extends Modo {
	
	public ModoHumanoide(double ataque,int distanciaDeAtaque,int velocidad){
	super(ataque,distanciaDeAtaque,velocidad);
	}
	
	public void efectosDeDesplazamientoAlTransformar(Algoformer algoformer,Tablero tablero){
		Posicion posicionEnTierra = algoformer.obtenerPosicionEnTablero().obtenerPosicionEnTierra(); 
		algoformer.mover(posicionEnTierra, tablero);
		Casillero casillero = tablero.getCasillero(algoformer.obtenerPosicionEnTablero());
		casillero.afectarAlgoformer(algoformer);
		algoformer.capturar(casillero.obtenerCaptura());
	  }
	
	public boolean puedeMoversePorPantano(){
		return false;
	}
	
	public void verificarMovimiento(Posicion posicion){
		if(posicion.getZ() == 1){
			throw new MovimientoImposibleException();
		}
	}
	
	public boolean confirmarTransformacion(Algoformer algoformer,Tablero tablero){
		Posicion posicionEnTierra = algoformer.obtenerPosicionEnTablero().obtenerPosicionEnTierra();
		if(tablero.casilleroEstaOcupado(posicionEnTierra)){
			if(algoformer.obtenerPosicionEnTablero().equals(posicionEnTierra)){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return true;
		}
	}
	
	public void capturar(Capturable capturable, Algoformer algoformer){
		algoformer.capturaChispa(capturable);
	}

	@Override
	public String obtenerNombreModo() {
		return "Modo : Humanoide";
	}

}
