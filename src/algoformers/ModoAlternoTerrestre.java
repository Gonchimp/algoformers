package algoformers;

import excepciones.MovimientoImposibleException;
import interfaces.Capturable;
import utilidadesDeJuego.Posicion;
import utilidadesDeJuego.Tablero;

public class ModoAlternoTerrestre extends Modo {
	
	
	public ModoAlternoTerrestre(double ataque, int distanciaDeAtaque, int velocidad){
		super(ataque,distanciaDeAtaque,velocidad);
	}
	
	public void efectosDeDesplazamientoAlTransformar(Algoformer algoformer,Tablero tablero){
	}
	
	public boolean puedeMoversePorPantano(){
		return true;
	}
	
	public void verificarMovimiento(Posicion posicion){
		if(posicion.getZ() == 1){
			throw new MovimientoImposibleException();
		}		
	}
	
	public boolean confirmarTransformacion(Algoformer algoformer,Tablero tablero){
		return true;
	}
	
	public void capturar(Capturable capturable, Algoformer algoformer){
	
	}

	@Override
	public String obtenerNombreModo() {
		return "Modo : Alterno Terrestre";
	}
}
