package algoformers;

import utilidadesDeJuego.Jugador;

public class Megatron extends Decepticon {

	private static String NOMBRE = "Megatron";
	private static double VIDA = 550;
	private static double ATAQUE_HUMANOIDE = 10;
	private static int ATAQUE_HUMANOIDE_DISTANCIA = 3;
	private static int VELOCIDAD_HUMANOIDE = 1;
	private static double ATAQUE_ALTERNO = 55;
	private static int ATAQUE_ALTERNO_DISTANCIA = 2;
	private static int VELOCIDAD_ALTERNO = 8;

	public Megatron(){
		super();
		this.nombre = NOMBRE;
		this.puntosDeVida = VIDA;
		this.modoActivo = new ModoHumanoide (ATAQUE_HUMANOIDE,ATAQUE_HUMANOIDE_DISTANCIA,VELOCIDAD_HUMANOIDE);
		this.modoInactivo = new ModoAlternoAereo (ATAQUE_ALTERNO,ATAQUE_ALTERNO_DISTANCIA,VELOCIDAD_ALTERNO);
	}
	
	
	public void modificarVidaPorcentual(double modificador) {
		this.modificarVidaPorcentualAlgoformerUnico(modificador);
	}


	
	public void recibirDanio(Autobot autobot) {
		this.recibirDanioAlgoformerUnico(autobot);
		
	}
	
	@Override
	public void abortar(Jugador jugador) {
		this.abortarFusion();
		
	}
}
