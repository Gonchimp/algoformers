package algoformers;

import utilidadesDeJuego.Jugador;

public class Bumblebee extends Autobot {

	private static String NOMBRE = "Bumblebee";
	private static double VIDA = 350;
	private static double ATAQUE_HUMANOIDE = 40;
	private static int ATAQUE_HUMANOIDE_DISTANCIA = 1;
	private static int VELOCIDAD_HUMANOIDE = 2;
	private static double ATAQUE_ALTERNO = 20;
	private static int ATAQUE_ALTERNO_DISTANCIA = 3;
	private static int VELOCIDAD_ALTERNO = 5;

	public Bumblebee (){
		super();
		this.nombre = NOMBRE;
		this.puntosDeVida = VIDA;
		this.modoActivo = new ModoHumanoide (ATAQUE_HUMANOIDE,ATAQUE_HUMANOIDE_DISTANCIA,VELOCIDAD_HUMANOIDE);
		this.modoInactivo = new ModoAlternoTerrestre (ATAQUE_ALTERNO,ATAQUE_ALTERNO_DISTANCIA,VELOCIDAD_ALTERNO);
	}
	
	public void recibirDanio(Decepticon decepticon) {
		this.recibirDanioAlgoformerUnico(decepticon);
	}
	
	public void modificarVidaPorcentual(double modificador) {
		this.modificarVidaPorcentualAlgoformerUnico(modificador);
	}

	@Override
	public void abortar(Jugador jugador) {
		this.abortarFusion();
		
	}

}
