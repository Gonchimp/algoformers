package algoformers;

import excepciones.MovimientoImposibleException;
import interfaces.Capturable;
import utilidadesDeJuego.Posicion;
import utilidadesDeJuego.Tablero;

public class ModoAlternoAereo extends Modo {
	
	public ModoAlternoAereo(double ataque, int distanciaDeAtaque, int velocidad){
		super(ataque,distanciaDeAtaque,velocidad);
	}
	
	public void efectosDeDesplazamientoAlTransformar(Algoformer algoformer,Tablero tablero){
		Posicion posicionEnAire = algoformer.obtenerPosicionEnTablero().obtenerPosicionEnAire();
		algoformer.mover(posicionEnAire, tablero);
	}
	
	public boolean puedeMoversePorPantano() {
		return true;
	}
	
	public void verificarMovimiento(Posicion posicion){
		if(posicion.getZ() == 0){
			throw new MovimientoImposibleException();
		}
	}
	
	public boolean confirmarTransformacion(Algoformer algoformer,Tablero tablero){
	if(tablero.casilleroEstaOcupado(algoformer.obtenerPosicionEnTablero().obtenerPosicionEnAire())){
		return false;
	}
	else{
		return true;
	}
	}
	
	public void capturar(Capturable capturable, Algoformer algoformer){
	
	}

	@Override
	public String obtenerNombreModo() {
		return "Modo : Alterno Aereo";
	}
	
}
