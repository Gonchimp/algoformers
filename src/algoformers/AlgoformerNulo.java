package algoformers;

import bonus.Fusion;
import interfaces.Atacable;
import utilidadesDeJuego.Jugador;
import utilidadesDeJuego.PosicionNula;
import utilidadesDeJuego.Tablero;

public class AlgoformerNulo extends Algoformer {
	
	public  AlgoformerNulo(){
		super();
		this.puntosDeVida=0;
		this.modoActivo=new ModoHumanoide(0,0,0);
		this.modoInactivo=new ModoHumanoide(0,0,0);
		this.nombre="Ninguno";
		this.posicion=new PosicionNula();
	}
	
	@Override
	public void recibirDanio(Decepticon decepticon) {
	
		
	}

	@Override
	public void recibirDanio(Autobot autobot) {
	
		
	}

	@Override
	public void combinarConAlgoformers(Algoformer algoformer1, Algoformer algoformer2, Tablero tablero,Fusion fusion) {
		
	}

	@Override
	public void atacarAtacable(Atacable atacable) {
		
		
	}
	
	public void morirse(Jugador jugador, Tablero tablero){
		
	}

	@Override
	public void modificarVidaPorcentual(double modificador) {
		
		
	}

	@Override
	public void separarse(Tablero tablero, Jugador jugador) {
	
		
	}

	@Override
	public void abortar(Jugador jugador) {

		
	}

}
