package algoformers;

import interfaces.Capturable;
import modificadores.Modificadores;
import superficies.Superficie;
import utilidadesDeJuego.Posicion;
import utilidadesDeJuego.Tablero;

public abstract class Modo { //implements Modificable
	
	protected double ataque;
	protected int distanciaDeAtaque;
	protected int velocidad;
	protected static int MOVIMIENTOS_TRANSFORMACION = 1;
	
	protected Modo(double ataque2, int distanciaDeAtaque, int velocidad){
		
		this.ataque = ataque2;
		this.distanciaDeAtaque = distanciaDeAtaque;
		this.velocidad = velocidad;
	}
	
	public double obtenerAtaque(Modificadores modificadores){
		double ataqueModificado = modificadores.modificarAtaque(ataque);	
		return ataqueModificado;
	}
	
	public int obtenerDistanciaDeAtaque(){		
		return distanciaDeAtaque;
	}
	
	public int obtenerVelocidad(Modificadores modificadores){
		double velocidadModificada = modificadores.modificarVelocidad(velocidad);	
		return (int) velocidadModificada;
	}
	
	public abstract void efectosDeDesplazamientoAlTransformar(Algoformer algoformer, Tablero tablero);
	
	
	public boolean puedeMoversePor(Superficie superficie){
		return superficie.puedeMoverse(this);
	}
	
	
	public abstract boolean puedeMoversePorPantano();

	
	protected abstract void verificarMovimiento(Posicion posicion);
	
	public abstract boolean confirmarTransformacion(Algoformer algoformer, Tablero tablero);
	
	public abstract void capturar(Capturable capturable, Algoformer algoformer);
	
	public abstract String obtenerNombreModo();
}
