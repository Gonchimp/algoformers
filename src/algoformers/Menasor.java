package algoformers;

import java.util.ArrayList;
import java.util.Stack;

import interfaces.Combinable;
import utilidadesDeJuego.Jugador;
import utilidadesDeJuego.Posicion;
import utilidadesDeJuego.PosicionNula;
import utilidadesDeJuego.Tablero;

public class Menasor extends Decepticon implements Combinable{
	
	private static String NOMBRE = "Menasor";
	private static double ATAQUE = 115;
	private static int ATAQUE_DISTANCIA = 2;
	private static int VELOCIDAD= 2;
	private ArrayList<Algoformer> algoformers;
	
;
	
	public Menasor (){
		super();
		this.nombre = NOMBRE;
		this.puntosDeVida = 1;
		this.modoActivo = new ModoHumanoide(ATAQUE,ATAQUE_DISTANCIA,VELOCIDAD);
		this.modoInactivo = this.modoActivo;
		this.algoformers = new ArrayList<Algoformer>();
		this.posicion = new PosicionNula();
	}
	
	public void insertarAlgoformer(Algoformer algoformer){
		this.algoformers.add(algoformer);
	}
	
	public void ubicar(Tablero tablero){
		
		//El menasor se ubicara en la posicion del decepticon que llama a los demas para combinarse
		for(Algoformer algoformer : this.algoformers){
			this.posicion = algoformer.obtenerPosicionEnTablero();
			tablero.eliminar(algoformer);
			}
		tablero.agregar(this,this.posicion);
	}
	
	public void absorber(Jugador jugador){
		this.puntosDeVida = 0;
		for(Algoformer algoformer : this.algoformers){
			this.puntosDeVida += algoformer.obtenerVida();
			jugador.eliminarAlgoformer(algoformer.obtenerNombre());
		}
		jugador.agregarAEquipo(this);
	}
	
	public Stack<Algoformer> getAlgoformers(){
		Stack<Algoformer> algoformers = new Stack<Algoformer>();
		for(Algoformer algoformer : this.algoformers){
			algoformers.push(algoformer);
		}
		return algoformers;
	}
	
	public void separarse(Tablero tablero, Jugador jugador){
		
		Posicion posicionMenasor = this.obtenerPosicionEnTablero();
		Stack<Posicion> posiciones = posicionMenasor.obtenerAreaDeSeparacion();
		Stack<Algoformer> decepticons = this.getAlgoformers();
		
		while(!decepticons.empty() && !posiciones.empty()){
			Posicion pos = posiciones.pop();
			if(!tablero.casilleroEstaOcupado(pos)){
				Algoformer algoformer =decepticons.pop();
				tablero.agregar(algoformer,pos);
				jugador.agregarAEquipo(algoformer);
				}
			}
		tablero.eliminar(this);
		jugador.eliminarAlgoformer(this.nombre);
	}

	public void modificarVidaPorcentual(double modificador) {
		this.puntosDeVida = (this.puntosDeVida)*modificador;
        for(Algoformer algoformer : this.algoformers){
        	algoformer.modificarVidaPorcentual(modificador);
        }
	}

	public void recibirDanio(Autobot autobot) {
		if(this.modificadores.burbujaActiva()){
			return;
		}
		double ataqueDividido  = autobot.obtenerAtaque() / 3;
        this.puntosDeVida -= autobot.obtenerAtaque();
        for(Algoformer algoformer : this.algoformers){
        	algoformer.puntosDeVida -= ataqueDividido;
        }
	}
	

	@Override
	public void abortar(Jugador jugador) {
		jugador.eliminarAlgoformer(NOMBRE);
		
	}

}