package algoformers;

import bonus.Fusion;
import excepciones.ImposibleTransformarException;
import excepciones.ObjetivoFueraDeRangoException;
import interfaces.Afectable;
import interfaces.Agregable;
import interfaces.Atacable;
import interfaces.Capturable;
import interfaces.Movible;
import modificadores.ModificadorAtaque;
import modificadores.ModificadorFusion;
import modificadores.ModificadorVelocidad;
import modificadores.ModificadorVida;
import modificadores.Modificadores;
import superficies.Superficie;
import utilidadesDeJuego.Jugador;
import utilidadesDeJuego.Posicion;
import utilidadesDeJuego.PosicionNula;
import utilidadesDeJuego.Tablero;

public abstract class Algoformer implements Agregable, Movible, Atacable, Afectable {
	
	private static int RANGO_DE_FUSION = 4;
	
	protected String nombre;
	protected double puntosDeVida;
	protected Modo modoActivo;
	protected Modo modoInactivo;
	protected Posicion posicion;
	protected Modificadores modificadores;
	private int movRestantes;

	
	public Algoformer(){		
		this.modificadores = new Modificadores();
		this.posicion=new PosicionNula();
		this.movRestantes = 0;

	}
	
	//Algoformer se transforma en un tablero
	//Verifica que pueda transformarse, segun a que parte del tablero deba moverse, segun su modo
	
	public void transformar(Tablero tablero){
		if(this.transformacionDisponible(tablero)){
		Modo modoAuxiliar= this.modoInactivo;
		this.modoInactivo = this.modoActivo;
		this.modoActivo = modoAuxiliar;
		this.modoActivo.efectosDeDesplazamientoAlTransformar(this,tablero);
		}
		else
		{
			throw new ImposibleTransformarException();
		}
	}
	
	//Corrobora que el algoformer pueda transformarse
	
	private boolean transformacionDisponible(Tablero tablero){
		return this.modoInactivo.confirmarTransformacion(this,tablero);
	}
	
	public String obtenerNombre(){
		return nombre;
	}
	
	public double obtenerVida(){
		return puntosDeVida;
	}
	
	public double obtenerAtaque(){
		return modoActivo.obtenerAtaque(this.modificadores);
	}
	
	public int obtenerDistanciaDeAtaque(){
		return modoActivo.obtenerDistanciaDeAtaque();
	}
	
	public int obtenerVelocidad(){
		return modoActivo.obtenerVelocidad(this.modificadores);
	}
	
	public boolean estaOcupado(){
		return true;
	}
	
	public boolean rangoDisponible(Posicion posFinal){
		return this.posicion.rangoDisponible(posFinal, this.obtenerVelocidad());
	}
	
	//Mueve al algoformer
	//Recibe una posicion y un tablero por el cual moverse
	//LA CLASE RECORRIDO ES RESPONSABLE DE VERIFICAR QUE EL MOVIMIENTO DEL ALGOFORMER SEA CORRECTO
	
	public void mover(Posicion posicionAMover,Tablero tablero) {
		tablero.moverDeHacia(this, this.posicion, posicionAMover);
		}
	
	//Chequea el movimiento sobre una superficie
	
	public boolean puedeMoversePor(Superficie superficie){
		return this.modoActivo.puedeMoversePor(superficie);
	}

	//Ataca a un algoformer
	//Recibe un algoformer enemigo
	//Ataca si el rango de ataque es correspondiente con la posicion del enemigo
	
	public void atacar(Algoformer algoformer, Tablero tablero){
		Posicion pos = algoformer.obtenerPosicionEnTablero();
		boolean disponible = this.posicion.rangoDisponible(pos, this.obtenerDistanciaDeAtaque());
		if(disponible) {
			Atacable enemigo = algoformer;
			this.atacarAtacable(enemigo);
		}
		else{
			throw new ObjetivoFueraDeRangoException();
			}
	}
	
	public Posicion obtenerPosicionEnTablero(){
		return this.posicion;
	}
	
	//Setea la posicion del algoformer en el tablero
	
	public void establecerPosicionEnTablero(Posicion pos){
		this.posicion = pos;
	}
	
	//Ataca al atacable
	
	public abstract void atacarAtacable(Atacable atacable);
	
	public Modo pedirModoActivo(){
		return this.modoActivo;
	}
	
	//El algoformer es afectado por la superficie
	
	public void recibirEfectoSuperficie(Superficie superficie){
		superficie.afectar(this);		
	}
	
	//Modifica la vida del Algoformer
	
	public abstract void modificarVidaPorcentual(double modificador);
	
	protected void modificarVidaPorcentualAlgoformerUnico(double modificador){
		this.puntosDeVida = (this.puntosDeVida)*modificador;
	}
	
	public void modificarAtaquePorcentual(double modificador){
	}


	public void agregarModificadorVelocidad(ModificadorVelocidad modificador) {
		this.modificadores.agregarModificadorVelocidad(modificador);
		
	}

	public void agregarModificadorAtaque(ModificadorAtaque modificador) {
		this.modificadores.agregarModificadorAtaque(modificador);
	}
	
	public void agregarModificadorFusion(ModificadorFusion modificador) {
		this.modificadores.agregarModificadorFusion(modificador);
		
	}
	public void descontarTurno(){
		this.modificadores.descontarTurnosPropiosDeModificadores();
	}


	public void agregarModificadorVida(ModificadorVida modificador) {
		this.modificadores.agregarModificadorVida(modificador);
		
	}
	
	//Chequea que el algoformer pueda unirse con otros dos algoformers
	//Los algoformers deben estar en el tablero a su rango de fusion correspondiente
	
	protected boolean fusionDisponible(Algoformer algoformer1, Algoformer algoformer2){
		boolean disponible = false;
		if( this.posicion.rangoDisponible(algoformer1.obtenerPosicionEnTablero(), RANGO_DE_FUSION) &&
			this.posicion.rangoDisponible(algoformer2.obtenerPosicionEnTablero(), RANGO_DE_FUSION)&&
			algoformer1.obtenerPosicionEnTablero().rangoDisponible(algoformer2.obtenerPosicionEnTablero(), RANGO_DE_FUSION))
		{
			disponible = true;
		}		
		return disponible;
	}
	
	public void capturar(Capturable capturable){
		this.modoActivo.capturar(capturable, this);
		
	}
	
	public abstract void morirse(Jugador jugador, Tablero tablero);
		
	public abstract void separarse(Tablero tablero, Jugador jugador);
	
	public void capturaChispa(Capturable capturable){
		capturable.serCapturado();
	}
	public abstract void abortar(Jugador jugador);
	
	protected void abortarFusion(){
		this.modificadores.eliminarModificadorFusion();
	}
	
	public abstract void combinarConAlgoformers(Algoformer algoformer1, Algoformer algoformer2,Tablero tablero,Fusion fusion);


	public void setMovRestantes(int cantidadDeMovimientos) {
		this.movRestantes = cantidadDeMovimientos;
	}
	public int getMovRestantes() {
		return this.movRestantes;
	}
}
