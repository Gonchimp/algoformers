package utilidadesDeJuego;


import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;

import algoformers.Algoformer;
import algoformers.AlgoformerNulo;
import bonus.Fusion;


public class Jugador {
	
	private String nombre;
	private LinkedList<Algoformer> equipo;
	private Tablero tablero;
	private Recorrido recorrido;
	private Algoformer algoformerActivo;
	private boolean heJugado;
	
	public Jugador(Tablero tablero,String nombre){
		
		this.nombre = nombre;
		this.tablero = tablero;
		this.recorrido = new Recorrido();
		this.equipo = new LinkedList<Algoformer>();
		this.algoformerActivo = new AlgoformerNulo();
		this.heJugado = false;
	}
	
	public String obtenerNombre(){
		return this.nombre;
	}
	
	public int cantidadDeMovimientos(){
		return this.recorrido.getCantidadDeMovimientos();
	}
	
	public void agregarAEquipo(Algoformer algoformer){
		equipo.add(algoformer);
	}
	
	public void atacar(Algoformer enemigo){
		if(!this.heJugado){
		this.algoformerActivo.atacar(enemigo, this.tablero);
		this.heJugado = true;
		}
	}
	
	//Actualiza el recorrido posible para el algoformer
	 
	 public void mover(){
		 if(!this.heJugado){
		 this.recorrido.determinarRecorridoPosibleSegunVelocidad(this.algoformerActivo, tablero);
		 this.heJugado = true;
		 }
	 }
	 
	 
	 public void moverArriba(){
		 Posicion posArriba = this.algoformerActivo.obtenerPosicionEnTablero().obtenerPosicionArriba();
		  recorrido.avanzar(this.algoformerActivo, posArriba, tablero);
	 }
	 
	 public void moverAbajo(){
		 Posicion posAbajo = this.algoformerActivo.obtenerPosicionEnTablero().obtenerPosicionAbajo();
		 recorrido.avanzar(this.algoformerActivo, posAbajo, tablero);
	 }
	 
	 public void moverIzquierda(){
		 Posicion posIzquierda = this.algoformerActivo.obtenerPosicionEnTablero().obtenerPosicionIzquierda();
		 recorrido.avanzar(this.algoformerActivo, posIzquierda, tablero);
	 }
	 
	 public void moverDerecha(){
		 Posicion posDerecha = this.algoformerActivo.obtenerPosicionEnTablero().obtenerPosicionDerecha();
		 recorrido.avanzar(this.algoformerActivo, posDerecha, tablero);
	 }
	 
	 public void moverSuperiorDerecha(){
		 Posicion posSuperiorDerecha = this.algoformerActivo.obtenerPosicionEnTablero().obtenerPosicionSuperiorDerecha();
		 recorrido.avanzar(this.algoformerActivo, posSuperiorDerecha, tablero);
	 }
	 
	 public void moverSuperiorIzquierda(){
		 Posicion posSuperiorIzquierda = this.algoformerActivo.obtenerPosicionEnTablero().obtenerPosicionSuperiorIzquierda();
		 recorrido.avanzar(this.algoformerActivo, posSuperiorIzquierda, tablero);
	 }
	 
	 public void moverInferiorIzquierda(){
		 Posicion posInferiorIzquierda = this.algoformerActivo.obtenerPosicionEnTablero().obtenerPosicionInferiorIzquierda();
		 recorrido.avanzar(this.algoformerActivo, posInferiorIzquierda, tablero);
	 }
	 
	 public void moverInferiorDerecha(){
		 Posicion posInferiorDerecha = this.algoformerActivo.obtenerPosicionEnTablero().obtenerPosicionInferiorDerecha();
		 recorrido.avanzar(this.algoformerActivo, posInferiorDerecha, tablero);
	 }
	 
	 public void transformar(){
		 if(!this.heJugado){
		 this.algoformerActivo.transformar(tablero);
		 this.quitarMovimientosRestantes();
		 this.heJugado = true;
		 }
	 }
	 
	 public Posicion obtenerPosicion(String nombreAlgoformer){
		 
		 for(Algoformer algoformer : this.equipo){
			 if (algoformer.obtenerNombre() == nombreAlgoformer){
				 return algoformer.obtenerPosicionEnTablero();
			 }
		 }
		 return new PosicionNula();
	 }
	 

	public LinkedList<Algoformer> obtenerListaDeAlgoformers(){
		 	return equipo;
	 }
	 
	 public void establecerAlgoformerActivo(String nombre){
		
		 for(Algoformer algoformer : this.equipo){
			 if (algoformer.obtenerNombre() == nombre){
				 this.algoformerActivo = algoformer;
				 algoformer.setMovRestantes(algoformer.obtenerVelocidad());
				 if(this.heJugado){
					 this.quitarMovimientosRestantes();
				 }
				 return;
			 }
		 }
		 this.algoformerActivo=new AlgoformerNulo();
		
	}
	
	 public Algoformer obtenerAlgoformerActivo(){
		return this.algoformerActivo;
	}
	
	 public void terminarTurno(Juego juego){
		if(this.equipo.size()==0){
			juego.finalDeJuego();
			juego.establecerPerdedor(this.nombre);
		}
		this.heJugado = false;
	}
	
	 //Verifica que los algoformers sigan vivos o no.
	 public void actualizarAlgoformers(){

		 Iterator <Algoformer> iterador = this.equipo.iterator();
		 while(iterador.hasNext()){
			  Algoformer algoformer = iterador.next();
			  algoformer.morirse(this, this.tablero);
		 }
	}

	 //Descuenta un turno de los bonus y/o efectos externos.
	
	 public void descontarTurnoModificadoresAlgoformers(){
		Iterator<Algoformer> iterador = this.equipo.iterator();
		  while(iterador.hasNext()){
			  iterador.next().descontarTurno();
		 }
	 }
	
	 public void eliminarAlgoformer(String nombre) {
		 Algoformer algoformerABorrar = new AlgoformerNulo();
		 for(Algoformer algoformer : this.equipo){
			if(algoformer.obtenerNombre() == nombre){
				algoformerABorrar = algoformer;
			}
		}
		this.equipo.remove(algoformerABorrar);
		this.algoformerActivo = new AlgoformerNulo(); }
	
	 public void combinar(){
		
		Stack<Algoformer> pilaAlgoformers = new Stack<Algoformer>();
		int i=0;
		while (i<this.equipo.size()){
			if (!this.equipo.get(i).equals(this.obtenerAlgoformerActivo())){
				pilaAlgoformers.push(this.equipo.get(i));
			}
			i++;
		}
		Algoformer algoformer1 = pilaAlgoformers.pop();
		Algoformer algoformer2 = pilaAlgoformers.pop();
		Fusion fusion = new Fusion(this);
		this.obtenerAlgoformerActivo().combinarConAlgoformers(algoformer1, algoformer2, tablero,fusion);
		this.quitarMovimientosRestantes();
		this.heJugado = true;
	}
	 
	
	
	 public void separar(){
		 this.algoformerActivo.separarse(this.tablero, this);
		 this.quitarMovimientosRestantes();
		 this.heJugado = true;
	 }

	public void abortar() {
		for(Algoformer algoformer : this.equipo){
			algoformer.abortar(this);
		}
	
	}
	
	public void quitarMovimientosRestantes(){
		for(Algoformer algoformer : this.equipo){
			algoformer.setMovRestantes(0);
		}
	}

}
