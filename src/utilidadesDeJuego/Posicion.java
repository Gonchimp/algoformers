package utilidadesDeJuego;

import java.util.Stack;

import excepciones.PosicionInvalidaException;

public class Posicion {
	
	private static int X_MIN = 0;
	private static int X_MAX = 21;
	private static int Y_MIN = 0;
	private static int Y_MAX = 21;
	private static int Z_MIN = 0;
	private static int Z_MAX = 2;


	protected int coordenadaX;
	protected int coordenadaY;
	protected int coordenadaZ;
	
	//Constructor
	public Posicion(int x, int y, int z){
		if(!this.posicionValida(x, y, z)){
			throw new PosicionInvalidaException();
		}
		else{
		this.coordenadaX = x;
		this.coordenadaY = y;
		this.coordenadaZ = z;
		}
	}

	//obtener la coordenada X
	public int getX(){
		return coordenadaX;
	}
	
	//obtener la coordenada Y
	public int getY(){
		return coordenadaY;
	}
	
	//obtener la coordenada X
	public int getZ(){
		return coordenadaZ;
	}
	
	
	//Verifica el rango de dos posiciones, en el plano XY.
	//Verifica que dos posiciones esten o no en una misma Area 
	public boolean rangoDisponible(Posicion posicionAEvaluar,int rango){
		boolean disponible=false;
		for (int i=(this.coordenadaX-rango);i<=(this.coordenadaX+rango);i++){
			for(int j=(this.coordenadaY-rango);j<=(this.coordenadaY+rango);j++){
				for(int k = Z_MIN; k < Z_MAX; k++){
					if(this.posicionValida(i, j, k)){
						Posicion posicion = new Posicion(i,j,k);
							if(posicion.equals(posicionAEvaluar)){
							disponible=true;
							}
						}
					}
				}
			}
		return disponible;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + coordenadaX;
		result = prime * result + coordenadaY;
		result = prime * result + coordenadaZ;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Posicion other = (Posicion) obj;
		if (coordenadaX != other.coordenadaX)
			return false;
		if (coordenadaY != other.coordenadaY)
			return false;
		if (coordenadaZ != other.coordenadaZ)
			return false;
		return true;
	}
	
	public Posicion obtenerPosicionArriba(){
		int x = this.coordenadaX;
		int y = this.coordenadaY + 1;
		int z= this.coordenadaZ;
		
		if(y < Y_MAX){
		Posicion posicionArriba = new Posicion(x,y,z);
		return posicionArriba;
		}
		else{
			return this;
		}
	}
	
	public Posicion obtenerPosicionAbajo(){
		int x = this.coordenadaX;
		int y = this.coordenadaY - 1;
		int z= this.coordenadaZ;
		
		if(y >= Y_MIN){
		Posicion posicionAbajo = new Posicion(x,y,z);
		return posicionAbajo;
		}
		else{
			return this;
		}
	}
	
	public Posicion obtenerPosicionIzquierda(){
		int x = this.coordenadaX - 1;
		int y = this.coordenadaY;
		int z= this.coordenadaZ;
		
		if(x >= X_MIN){
		Posicion posicionIzquierda = new Posicion(x,y,z);
		return posicionIzquierda;
		}
		else{
			return this;
		}
	}
	
	public Posicion obtenerPosicionDerecha(){
		int x = this.coordenadaX + 1;
		int y = this.coordenadaY;
		int z= this.coordenadaZ;
		
		if(x < X_MAX){
		Posicion posicionDerecha = new Posicion(x,y,z);
		return posicionDerecha;
		}
		else{
			return this;
		}
	}
	
	public Posicion obtenerPosicionSuperiorDerecha(){
		int x = this.coordenadaX + 1;
		int y = this.coordenadaY + 1;
		int z= this.coordenadaZ;
		
		if(x < X_MAX && y < Y_MAX){
		Posicion posicionSuperiorDerecha = new Posicion(x,y,z);
		return posicionSuperiorDerecha;
		}
		else{
			return this;
		}
	}
	
	public Posicion obtenerPosicionSuperiorIzquierda(){
		int x = this.coordenadaX - 1;
		int y = this.coordenadaY + 1;
		int z = this.coordenadaZ;
		
		if(x >= X_MIN && y < Y_MAX){
		Posicion posicionSuperiorIzquierda = new Posicion(x,y,z);
		return posicionSuperiorIzquierda;
		}
		else{
			return this;
		}
	}
	
	public Posicion obtenerPosicionInferiorDerecha(){
		int x = this.coordenadaX + 1;
		int y = this.coordenadaY - 1;
		int z= this.coordenadaZ;
		
		if(x < X_MAX && y >= Y_MIN){
		Posicion posicionInferiorDerecha = new Posicion(x,y,z);
		return posicionInferiorDerecha;
		}
		else{
			return this;
		}
	}
	
	public Posicion obtenerPosicionInferiorIzquierda(){
		int x = this.coordenadaX - 1;
		int y = this.coordenadaY - 1;
		int z= this.coordenadaZ;
		
		if(x >= X_MIN && y >= Y_MIN){
		Posicion InferiorIzquierda = new Posicion(x,y,z);
		return InferiorIzquierda;
		}
		else{
			return this;
		}
	}
	
	//Recibe una posicion valida cualquiera
	//Devuelve una posicion con las mismas coordenadas en X Y pero con Z = 1
	public Posicion obtenerPosicionEnAire(){
			  int coordenadaZ = 1;
			  Posicion posicionNueva = new Posicion(this.coordenadaX,this.coordenadaY,coordenadaZ);
			  return posicionNueva;
	}
		
	//Recibe una posicion valida cualquiera
	//Devuelve una posicion con las mismas coordenadas en X Y pero con Z = 0
	public Posicion obtenerPosicionEnTierra(){
			  int coordenadaZ = 0;
			  Posicion posicionNueva = new Posicion(this.coordenadaX,this.coordenadaY,coordenadaZ);
			  return posicionNueva;
	}
	
	private boolean posicionValida(int x, int y, int z){
		if(x < X_MIN || y< Y_MIN|| z < Z_MIN || x > X_MAX || y > Y_MAX || z > Z_MAX){
			return false;
		}
		else{
			return true;
		}
	}
	
	public Stack<Posicion> obtenerAreaDeSeparacion(){
		
		Stack<Posicion> area = new Stack<Posicion>();
		Posicion pos1 = this.obtenerPosicionAbajo();
		Posicion pos2 = this.obtenerPosicionArriba();
		Posicion pos3 = this.obtenerPosicionDerecha();
		Posicion pos4 = this.obtenerPosicionIzquierda();
		Posicion pos5 = pos1.obtenerPosicionAbajo();
		Posicion pos6 = pos2.obtenerPosicionArriba();
		Posicion pos7 = pos3.obtenerPosicionDerecha();
		Posicion pos8 = pos4.obtenerPosicionIzquierda();
			
		area.push(pos8);
		area.push(pos7);
		area.push(pos6);
		area.push(pos5);
		area.push(pos4);
		area.push(pos3);
		area.push(pos2);
		area.push(pos1);
		
		return area;
	}
}	
	

