package utilidadesDeJuego;

import java.util.ArrayList;
import algoformers.Algoformer;
import superficies.Superficie;

public class Recorrido {
	
	private static int X_MIN = 0;
	private static int X_MAX = 21;
	private static int Y_MIN = 0;
	private static int Y_MAX = 21;
	
	private ArrayList<Posicion> recorrido;
	private int cantidadDeMovimientos;
			
	public Recorrido(){
		recorrido = new ArrayList<Posicion> ();
		cantidadDeMovimientos = 0;
	}
	
	public Recorrido(ArrayList<Posicion> posiciones){
		recorrido = new ArrayList<Posicion>();
		recorrido.addAll(posiciones);
		cantidadDeMovimientos = 0;
	}
	
	public void agregarPosicion(Posicion posicion){
		this.recorrido.add(posicion);
	}
	
	public Posicion obtenerPosicion(int index){
		return this.recorrido.get(index);
	}
	
	public int obtenerCantidadDePosiciones(){
		return recorrido.size();
	}
	
	public void setCantidadDeMovimientos(int cantidadDeMovimientos){
		this.cantidadDeMovimientos = cantidadDeMovimientos;
	}
	
	public int getCantidadDeMovimientos(){
		return this.cantidadDeMovimientos;
	}
	
	public void determinarRecorridoPosibleSegunVelocidad(Algoformer algoformer,Tablero tablero){
		Posicion posicion = algoformer.obtenerPosicionEnTablero();
		this.agregarPosicion(posicion);
		int z = posicion.getZ();
		int velocidad = algoformer.obtenerVelocidad();
		algoformer.setMovRestantes(velocidad);
		for (int i=(posicion.getX()-velocidad);i<=(posicion.getX()+velocidad);i++){
			for(int j=(posicion.getY()-velocidad);j<=(posicion.getY()+velocidad);j++){
				if((i >= X_MIN && j >= Y_MIN)&&(i< X_MAX && j< Y_MAX) ){
					Posicion posicionNueva = new Posicion(i,j,z);
					Casillero casillero = tablero.getCasillero(posicionNueva);
					Superficie superficie = casillero.obtenerSuperficie();
					if(!casillero.estaOcupado() && algoformer.puedeMoversePor(superficie)){
						this.agregarPosicion(posicionNueva);
					}
				}
			}
		}
	}
	
	public void avanzar(Algoformer algoformer,Posicion posicion,Tablero tablero){
		this.setCantidadDeMovimientos(algoformer.getMovRestantes());
		if(this.recorrido.contains(posicion) && this.cantidadDeMovimientos > 0 && !(algoformer.obtenerPosicionEnTablero().equals(posicion))){
			algoformer.mover(posicion, tablero);
			tablero.getCasillero(posicion).obtenerSuperficie().descontarCantidadDeMovimientos(this);
			algoformer.setMovRestantes(this.cantidadDeMovimientos);
		}
	}

}
