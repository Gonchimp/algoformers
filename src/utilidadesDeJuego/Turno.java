package utilidadesDeJuego;

import java.util.ArrayList;
import java.util.Collections;

public class Turno {
	
	private Jugador jugadorActivo;
	private Jugador jugadorPasivo;
	
	public Turno(ArrayList<Jugador> listaJugadores){
		
		Collections.shuffle(listaJugadores);
		this.jugadorActivo = listaJugadores.get(0);
		this.jugadorPasivo = listaJugadores.get(1);
		
	}
	
	
	public void cambiarTurno(Juego juego){
		this.jugadorActivo.terminarTurno(juego);
		this.jugadorActivo.actualizarAlgoformers();
		this.jugadorPasivo.actualizarAlgoformers();
		Jugador jugadorAuxiliar = this.jugadorPasivo;
		this.jugadorPasivo = this.jugadorActivo;
		this.jugadorActivo = jugadorAuxiliar;
	}
	
	public Jugador obtenerJugadorActivo(){
		return this.jugadorActivo;
	}


	public Jugador obtenerJugadorNoActivo() {
		return this.jugadorPasivo;
	}
	
	

}
