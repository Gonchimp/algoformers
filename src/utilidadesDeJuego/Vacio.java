package utilidadesDeJuego;

import interfaces.Agregable;
import interfaces.Capturable;
import interfaces.Movible;
import superficies.Superficie;

public class Vacio implements Agregable, Capturable, Movible{
	
	public Vacio(){
		
	}
	
	public boolean estaOcupado(){
		return false;
	}

	
	public void mover(Posicion posicionAMover, Tablero tablero) {
	}

	
	public boolean puedeMoversePor(Superficie superficie) {
		
		return false;
	}
	
	public boolean existeChispa() {
		
		return false;
	}

	
	public void serCapturado() {
			
	}

}
