package utilidadesDeJuego;

import algoformers.Algoformer;
import bonus.Bonus;
import bonus.BonusNulo;
import interfaces.Agregable;
import interfaces.Capturable;
import interfaces.Movible;
import superficies.Superficie;

public class Casillero {  //deben haber dos tipos de casilleros, para poder llenarlos por defecto con nube y rocosa segun corresponda
	
	private Agregable agregado;
	private Movible movible;
	private Capturable capturable;
	private Superficie superficie;
	private Bonus bonus;
	
	//Constructor
	
	public Casillero(){
		this.agregarVacio();
		this.agregarBonusNulo();
		this.capturable = new Vacio();
	}
	
	//Setea el casillero con algo Vacio
	
	public void agregarVacio(){
		Vacio vacio = new Vacio();
		this.agregado = vacio;
		this.movible = vacio;
	}
	
	//Se coloca un algoformer en el casillero
	
	public void agregarAlgoformer(Algoformer algoformer){
		this.agregado = algoformer;
		this.movible = algoformer;
	}
	
	//Devuelve si el casillero esta siendo ocupado por algun objeto
	
	public boolean estaOcupado(){
		return this.agregado.estaOcupado();
	}
	
	//Devuelve el Algoformer que se encuentre en el casillero
	
	public Algoformer obtenerAlgoformer(){
		return (Algoformer) this.movible;		
	}

	//Pone una chispa en el casillero
	
	public void agregarChispa(Chispa chispa){
		this.capturable= chispa;
	}
	
	//Devuelve si la chispa existe en el casillero
	
	public boolean existeChispa(){
		return (this.capturable.existeChispa());
	}
	
	//Afecta al algoformer que se agrego al casillero segun la superficie Existente
	
	public void afectarAlgoformer(Algoformer algoformer){
		this.superficie.afectar(algoformer);
		this.bonus.aplicar(algoformer);
		this.bonus = new BonusNulo(); // "Elimina" el bonus

	}
	
	//Agrega una nueva superficie al casillero
	
	public void agregarSuperficie(Superficie superficieNueva){
		this.superficie= superficieNueva;
	}
	
	//Retorna la superficie 
	
	public Superficie obtenerSuperficie(){
		return this.superficie;
	}


	public void agregarBonus(Bonus bonusNuevo){
		this.bonus = bonusNuevo;
	}
	
	public void agregarBonusNulo(){
		this.bonus = new BonusNulo();
	}
	
	//retorna el bonus
	public Bonus obtenerBonus(){
			return this.bonus;
		}
	
	public Capturable obtenerCaptura(){
		return this.capturable;
	}
	
}

