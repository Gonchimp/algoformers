package utilidadesDeJuego;

import interfaces.Capturable;

public class Chispa implements Capturable{
	
	private Juego juego;
		
	public Chispa(){
		
	}
	
	public void entrarAlJuego(Juego juego){
		this.juego = juego;
	}
	
	public boolean existeChispa(){
		return true;
	}
	
	public void serCapturado(){
		this.juego.finalDeJuego();
		this.juego.establecerGanador(this.juego.obtenerJugadorActivo().obtenerNombre());
	}

}
