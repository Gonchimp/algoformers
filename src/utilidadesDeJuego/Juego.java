package utilidadesDeJuego;

import java.util.ArrayList;


import algoformers.Algoformer;
import algoformers.Bonecrusher;
import algoformers.Bumblebee;
import algoformers.Frenzy;
import algoformers.Megatron;
import algoformers.OptimusPrime;
import algoformers.Ratchet;


public class Juego {
    private Tablero tablero;
    private Jugador jugador1;
    private Jugador jugador2;
	private Turno turno;
	private boolean terminado;
	private Chispa chispa;
	private String ganador;
	private String perdedor;
	
	private static String AUTOBOTS = "AUTOBOTS"; 
	private static String DECEPTICONS = "DECEPTICONS"; 
    
    public Juego(){
		
    	this.terminado = false;
    	this.tablero= new Tablero();
        this.jugador1= new Jugador(tablero,AUTOBOTS);
        this.jugador2= new Jugador(tablero,DECEPTICONS);
        this.chispa = new Chispa();
        
        ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
        jugadores.add(jugador1);
        jugadores.add(jugador2);
    	this.turno= new Turno(jugadores);
    	
    	this.ubicarChispa();
    	this.asignarYUbicarEquipos();
    }
           
    private void ubicarChispa(){
    	this.chispa.entrarAlJuego(this);
    	this.tablero.agregarChispa(this.chispa);
      
    }
  
    public boolean existeChispa(){
    	return this.tablero.existeChispa();
    }
    
    public void ubicarAlgoformer(Algoformer algoformer, Posicion posicion){
    	this.tablero.agregar(algoformer,posicion);
    }

    private void asignarYUbicarEquipos(){
    	
      	//Algoformers
    	OptimusPrime optimusPrime= new OptimusPrime();
    	Bumblebee bumblebee= new Bumblebee();
    	Ratchet ratchet= new Ratchet();
    	Megatron megatron= new Megatron();
    	Bonecrusher bonecrusher= new Bonecrusher();
    	Frenzy frenzy= new Frenzy();
    	
    	this.ubicarAlgoformer(optimusPrime, new Posicion(0,0,0));
    	this.ubicarAlgoformer(bumblebee, new Posicion(2,0,0));
    	this.ubicarAlgoformer(ratchet, new Posicion(0,2,0));
    	this.jugador1.agregarAEquipo(optimusPrime);
    	this.jugador1.agregarAEquipo(bumblebee);
    	this.jugador1.agregarAEquipo(ratchet);
    	
    	
    	this.ubicarAlgoformer(megatron, new Posicion(20,20,0));
    	this.ubicarAlgoformer(bonecrusher, new Posicion(20,18,0));
    	this.ubicarAlgoformer(frenzy, new Posicion(18,20,0));
    	this.jugador2.agregarAEquipo(megatron);
    	this.jugador2.agregarAEquipo(bonecrusher);
    	this.jugador2.agregarAEquipo(frenzy);
    	
    }
    
    public Posicion obtenerPosicionDeAutobot(String nombreDeAlgoformer){
    	return jugador1.obtenerPosicion(nombreDeAlgoformer);
    }
    
    public Posicion obtenerPosicionDeDecepticon(String nombreDeAlgoformer){
    	return jugador2.obtenerPosicion(nombreDeAlgoformer);
    }

   public Jugador obtenerJugadorActivo(){
    	return (this.turno.obtenerJugadorActivo());
    }
    
    public void cambiarTurno(){
    	
    	this.turno.obtenerJugadorActivo().descontarTurnoModificadoresAlgoformers();
    	this.turno.cambiarTurno(this);
    }
    
   public Tablero obtenerTableroDeJuego(){
    	return this.tablero;
    }
   
    public String obtenerSuperficieEnPos(Integer x,Integer y,Integer z){
    	Posicion pos=new Posicion(x,y,z);
    	Casillero casillero=tablero.getCasillero(pos);
    	String name = casillero.obtenerSuperficie().getNombre();
    	return name;
    }
    
    public String obtenerBonusEnPos(Integer x,Integer y,Integer z){
    	Posicion pos=new Posicion(x,y,z);
    	Casillero casillero=tablero.getCasillero(pos);
    	String name = casillero.obtenerBonus().getName();
    	return name;
    }
    
    public boolean verificarFinDeJuego(){
    	return terminado;
    }

	public void finalDeJuego(){
		this.terminado  = true;
	}

	public Jugador obtenerJugadorNoActivo() {
		return this.turno.obtenerJugadorNoActivo();
	}
	
	public void establecerPerdedor(String perdedor){
		this.perdedor = perdedor;
		String nombre =this.jugador1.obtenerNombre();
		String nombre1 = this.jugador2.obtenerNombre();
		if(nombre.equals(perdedor)){
			this.ganador = nombre1;
			
		}
		else{
			this.ganador = nombre;
		}
		
	}
	
	public void establecerGanador(String ganador){
		this.ganador = ganador;
		String nombre =this.jugador1.obtenerNombre();
		String nombre1 = this.jugador2.obtenerNombre();
		if(nombre.equals(ganador)){
			this.perdedor = nombre1;
			
		}
		else{
			this.perdedor = nombre;
		}
	}
	
	public String obtenerGanador(){
		return this.ganador;
	}
	
	public String obtenerPerdedor(){
		return this.perdedor;
	}
	
		
	
}

