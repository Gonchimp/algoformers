package utilidadesDeJuego;

import java.util.HashMap;
import algoformers.Algoformer;
import bonus.Bonus;
import bonus.Burbuja;
import bonus.DobleCanion;
import bonus.Flash;
import excepciones.AlgoformerNoExisteEnTableroException;
import excepciones.PosicionInvalidaException;
import excepciones.PosicionOcupadaException;
import superficies.Espinas;
import superficies.Nebulosa;
import superficies.Nube;
import superficies.Pantano;
import superficies.Rocosa;
import superficies.Superficie;
import superficies.Tormenta;

import java.util.ArrayList;
import java.util.Collections;


public class Tablero {
	
	private static int X_MIN = 0;
	private static int X_MAX = 21;
	private static int Y_MIN = 0;
	private static int Y_MAX = 21;
	private static int Z_MIN = 0;
	private static int Z_MAX = 2;
	private static int CANT_PANTANO = 20;
	private static int CANT_ESPINAS = 20;
	private static int CANT_NEBULOSA = 20;
	private static int CANT_TORMENTA = 20;
	private static int CANT_FLASH = 10;
	private static int CANT_BURBUJA = 5;
	private static int CANT_CANION = 10;
	
	private HashMap <Posicion,Casillero> tablero;
	
	
	//Constructor
	//Crea un tablero de 25*25*2
	
	public Tablero(){
		Nube nube=new Nube();
		Rocosa rocosa=new Rocosa();
		
		this.tablero = new HashMap<Posicion,Casillero>();
		for(int i = X_MIN; i<X_MAX; i++){
			for(int j = Y_MIN; j<Y_MAX; j++){
				for (int k = Z_MIN; k<Z_MAX; k++){
					Posicion posicion = new Posicion (i,j,k);
					Casillero casillero = new Casillero();
					if(k==0){
						casillero.agregarSuperficie(rocosa);
					}
					else{
						casillero.agregarSuperficie(nube);
					}
					this.tablero.put(posicion, casillero);
				}
			}
		}
		this.confirgurarZonas();
	}
	
	
	
	//Recibe una posicion valida para asociar a un casillero
	//Retorna si el casillero esta siendo ocupado por algun objeto
	
	public boolean casilleroEstaOcupado(Posicion posicion){
		Casillero casillero = this.getCasillero(posicion);
		return casillero.estaOcupado();
	}
	
	//Recibe un algoformer no existente en tablero y una posicion valida
	//Asocia la posicion con el casillero correspondiente y agrega un algoformer
	//Si la posicion a agregar el algoformer esta ocupada, lanza excepcion
	
	public void agregar(Algoformer algoformer, Posicion pos){
		Casillero casillero = this.getCasillero(pos);
		if(casillero.estaOcupado()){
			throw new PosicionOcupadaException();
		}
		else{
		casillero.agregarAlgoformer(algoformer);
		algoformer.establecerPosicionEnTablero(pos);
		}
	}
	
	//Recibe un algoformer existente en el tablero
	//Lo borra de el casillero en el que esté
	//Si no existe el algoformer, lanza excepcion
	
	public void eliminar(Algoformer algoformer){
		try{
		Casillero casillero = tablero.get(algoformer.obtenerPosicionEnTablero());
		casillero.agregarVacio();
		algoformer.establecerPosicionEnTablero(new PosicionNula());
		}
		catch(NullPointerException e){
			throw new AlgoformerNoExisteEnTableroException();
		}
	}
	
	//Recibe una posicion valida
	//Ascia la posicion con el casillero correspondiente y lo retorna
	
	public Casillero getCasillero(Posicion posicion){
		try{	
		return tablero.get(posicion);
		}
		catch (NullPointerException e){
			throw new PosicionInvalidaException();
		}
	}
	
	//Devuelve el tamaño del tablero en 3D
	
	public int obtenerTamanio(){
		return tablero.size();
	}
	
	
	public boolean esPosValida(Posicion pos){
		return (tablero.containsKey(pos));
	}
	
	//Recibe un algoformer existente en tablero y dos posiciones validas, una con el algoformer y la otra vacia
	//La primera posicion es la del algoformer en tablero, la segunda es la posicion destino
	//Mueve al algoformer estableciendolo en la posicion dos y agregando un vacio en la primera posicion
	//Si la posicion destino esta ocupada no hace nada
	
	public void moverDeHacia(Algoformer algoformer,Posicion posInicio,Posicion posDestino){
			this.getCasillero(posInicio).agregarVacio();
			this.getCasillero(posDestino).agregarAlgoformer(algoformer);
			this.getCasillero(posDestino).afectarAlgoformer(algoformer);
			algoformer.establecerPosicionEnTablero(posDestino);
			algoformer.capturar(this.getCasillero(posDestino).obtenerCaptura());
	}
	
	//Agrega una chispa al tablero
	
	public void agregarChispa(Chispa chispa){
		Posicion posicion= new Posicion((X_MAX/2),(Y_MAX/2),Z_MIN);
		Casillero casillero= this.getCasillero(posicion);
		casillero.agregarChispa(chispa);
	
	}
	
	public boolean existeChispa(){
		boolean existe=false;
		int i=0;
		while((i<X_MAX) && !existe){
			int j=0;
			while((j<Y_MAX) && !existe){
				Posicion posicion = new Posicion (i,j,Z_MIN);
				Casillero casillero= this.tablero.get(posicion);
				existe=casillero.existeChispa();
				j++;
			}
			i++;
		}
		return existe;
	}
	
	public void establecerSuperficie(ArrayList<Posicion> posiciones, Superficie superficie) {

		for(int i=0; i<posiciones.size(); i++){
			Casillero casillero = this.getCasillero(posiciones.get(i));
			casillero.agregarSuperficie(superficie);
		}
		
	}

	public void agregarBonus(Bonus bonus, Posicion pos) {

		Casillero casillero = this.getCasillero(pos);
		if(casillero.estaOcupado()){
			throw new PosicionOcupadaException();
		}
		else{
		casillero.agregarBonus(bonus);
		}
	}
	
	private void confirgurarZonas(){
		// Agrega de forma completamente aleatoria las superficies
		// y los bonus.
		
		ArrayList<Posicion> posicionesTierra = new ArrayList<Posicion>();
		ArrayList<Posicion> posicionesAire = new ArrayList<Posicion>();
		ArrayList<Posicion> posicionesBonus = new ArrayList<Posicion>();
		for(int x=0; x<Tablero.X_MAX; x++){
			for(int y=0; y<Tablero.Y_MAX; y++){
				posicionesTierra.add(new Posicion(x,y,0));		
				posicionesAire.add(new Posicion(x,y,1));
				posicionesBonus.add(new Posicion(x,y,0));
				posicionesBonus.add(new Posicion(x,y,1));
				
			}
		}
		
		
		Collections.shuffle(posicionesTierra);
		ArrayList<Posicion> posicionesPantano = new ArrayList<Posicion>();
		for(int i = 0; i<Tablero.CANT_PANTANO; i++){
			Posicion pos = posicionesTierra.get(i);
			if(pos.equals(new Posicion(Tablero.X_MAX/2, Tablero.Y_MAX/2, 0))) //CHISPA
			{continue;}
			posicionesPantano.add(pos);
			posicionesTierra.remove(i);
		}
		this.establecerSuperficie(posicionesPantano, new Pantano());
		
		Collections.shuffle(posicionesTierra);
		ArrayList<Posicion> posicionesEspinas = new ArrayList<Posicion>();
		for(int i = 0; i<Tablero.CANT_ESPINAS; i++){
			Posicion pos = posicionesTierra.get(i);
			posicionesEspinas.add(pos);
			posicionesTierra.remove(i);
		}
		this.establecerSuperficie(posicionesEspinas, new Espinas());
		
		Collections.shuffle(posicionesAire);
		ArrayList<Posicion> posicionesNebulosa = new ArrayList<Posicion>();
		for(int i = 0; i<Tablero.CANT_NEBULOSA; i++){
			Posicion pos = posicionesAire.get(i);
			posicionesNebulosa.add(pos);
			posicionesAire.remove(i);
		}
		this.establecerSuperficie(posicionesNebulosa, new Nebulosa());
		
		Collections.shuffle(posicionesAire);
		ArrayList<Posicion> posicionesTormenta = new ArrayList<Posicion>();
		for(int i = 0; i<Tablero.CANT_TORMENTA; i++){
			Posicion pos = posicionesAire.get(i);
			posicionesTormenta.add(pos);
			posicionesAire.remove(i);
		}
		this.establecerSuperficie(posicionesTormenta, new Tormenta());
		
		

		Collections.shuffle(posicionesBonus);
		for(int i = 0; i<Tablero.CANT_FLASH ; i++){
			this.agregarBonus(new Flash(), posicionesBonus.get(i));
			posicionesBonus.remove(i);
		}

		Collections.shuffle(posicionesBonus);
		for(int i = 0; i<Tablero.CANT_CANION ; i++){
			this.agregarBonus(new DobleCanion(), posicionesBonus.get(i));
			posicionesBonus.remove(i);
		}

		Collections.shuffle(posicionesBonus);
		for(int i = 0; i<Tablero.CANT_BURBUJA ; i++){
			this.agregarBonus(new Burbuja(), posicionesBonus.get(i));
			posicionesBonus.remove(i);
		}
		
		
	
	}
	
	
}