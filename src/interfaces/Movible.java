package interfaces;

import superficies.Superficie;
import utilidadesDeJuego.Posicion;
import utilidadesDeJuego.Tablero;

public interface Movible {
      abstract void mover(Posicion posicionAMover,Tablero tablero); 
      
      abstract boolean puedeMoversePor(Superficie superficie);
}
