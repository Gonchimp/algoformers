package interfaces;

import superficies.Superficie;

public interface Afectable {
	
	public abstract void recibirEfectoSuperficie(Superficie superficie);
}
