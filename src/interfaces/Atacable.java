package interfaces;

import algoformers.Autobot;
import algoformers.Decepticon;

public interface Atacable  {
		
		abstract void recibirDanio(Decepticon decepticon);
		
		abstract void recibirDanio(Autobot autobot);

}
