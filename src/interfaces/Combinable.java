package interfaces;

import modificadores.ModificadorFusion;
import utilidadesDeJuego.Jugador;
import utilidadesDeJuego.Tablero;

public interface Combinable {
	
	public void ubicar(Tablero tablero);

	public void agregarModificadorFusion(ModificadorFusion modificador);
	
	public void absorber(Jugador jugador);
	}
