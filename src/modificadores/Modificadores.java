package modificadores;


import java.util.LinkedList;

public class Modificadores {
	//Conjunto de todos los modificadores que puede tener un algoformer
	
	
	private LinkedList<ModificadorAtaque>  ataque;
	private LinkedList<ModificadorVelocidad> velocidad;
	private LinkedList<ModificadorVida> vida;
	private LinkedList<ModificadorFusion> fusion;

	public Modificadores(){
		
		this.ataque = new LinkedList<ModificadorAtaque>();
		this.velocidad = new LinkedList<ModificadorVelocidad>();
		this.vida = new LinkedList<ModificadorVida>();
		this.fusion = new LinkedList<ModificadorFusion>();
		
	}
	
	
	public void agregarModificadorAtaque(ModificadorAtaque modificador){
		for(ModificadorAtaque mod : this.ataque){
			if(mod.equals(modificador)){
				return;
			}
		}
		this.ataque.add(modificador);
	}
	
	public void agregarModificadorVelocidad(ModificadorVelocidad modificador){
		for(ModificadorVelocidad mod : this.velocidad){
			if(mod.equals(modificador)){
				return;
			}
		}
		this.velocidad.add(modificador);
	}
	

	public void agregarModificadorVida(ModificadorVida modificador) {
		for(ModificadorVida mod : this.vida){
			if(mod.equals(modificador)){
				return;
			}
		}
		this.vida.add(modificador);
		
	}
	

	public void agregarModificadorFusion(ModificadorFusion modificador) {
		for(ModificadorFusion mod : this.fusion){
			if(mod.equals(modificador)){
				return;
			}
		}
		this.fusion.add(modificador);
				
	}


	public double modificarAtaque(double ataque) {
		double ataqueModificado = ataque;
		for(ModificadorAtaque mod : this.ataque){
			ataqueModificado = mod.modificarAtaque(ataqueModificado);
		}
		return ataqueModificado;
	}
	
	public int modificarVelocidad(int velocidad){
		int velocidadModificada = velocidad;
		for(ModificadorVelocidad mod : this.velocidad){
			velocidadModificada = mod.modificarVelocidad(velocidadModificada);
		}
		return velocidadModificada;
	}
	
	public boolean burbujaActiva(){
		boolean burbuja = false;
		for(ModificadorVida mod : this.vida){
			if(mod.esBurbuja()){
				burbuja = true;
			}
		}
		return burbuja;
	}
	
	
	public void descontarTurnosModificadores(LinkedList<Modificador> mods){
		for(Modificador mod :  mods){
			mod.descontarTurno();
			if(mod.obtenerTurnosRestantes() == 0){
				mods.remove(mod);
			}
		}
	}
	
	public void eliminarModificadorFusion(){
		for(Modificador mod : this.ataque){
			if(mod.obtenerNombre().equals("Fusion")){
				this.ataque.remove(mod);
			}
		}
		
		for(Modificador mod : this.velocidad){
			if(mod.obtenerNombre().equals("Fusion")){
				this.velocidad.remove(mod);
			}
		}
		this.fusion.clear();
	}
	
	@SuppressWarnings("unchecked")
	public void descontarTurnosPropiosDeModificadores(){
		
		this.descontarTurnosModificadores((LinkedList<Modificador>)(LinkedList<?>)this.ataque);
		this.descontarTurnosModificadores((LinkedList<Modificador>)(LinkedList<?>)this.velocidad);
		this.descontarTurnosModificadores((LinkedList<Modificador>)(LinkedList<?>)this.vida);
		this.descontarTurnosModificadores((LinkedList<Modificador>)(LinkedList<?>)this.fusion);
	}


	

}
