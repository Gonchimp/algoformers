package modificadores;

public abstract class Modificador {
	
	protected int turnosPropios;
	protected String nombreMod;
	
	public Modificador(int turnos, String nombre){		
		this.turnosPropios = turnos;
		this.nombreMod = nombre;
	}
	
	public int obtenerTurnosRestantes(){
		return turnosPropios;
	}
	
	public void descontarTurno() {
		this.turnosPropios = turnosPropios - 1;
		
	}
	
	public String obtenerNombre(){
		return this.nombreMod;
	}

}
