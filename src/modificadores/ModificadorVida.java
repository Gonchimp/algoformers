package modificadores;

public class ModificadorVida extends Modificador{

	
	public ModificadorVida(double modificador, int turnos, String nombreMod){
		
		super(turnos, nombreMod);
	
	}
	
	public boolean esBurbuja(){
		return (this.nombreMod == "Burbuja");
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModificadorVida other = (ModificadorVida) obj;
		if (nombreMod != other.nombreMod)
			return false;
		return true;
	}


}
