package modificadores;

public class ModificadorAtaque extends Modificador{
	
		double modificador;
		private String nombre;
		
		public ModificadorAtaque(double modificador, int turnos, String nombreMod){
			
			super(turnos, nombreMod);
			this.modificador = modificador;
			
		}
		
		public double modificarAtaque(double ataque){
			return ataque*modificador;
		}
		
		
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ModificadorAtaque other = (ModificadorAtaque) obj;
			if (nombre != other.nombre)
				return false;
			return true;
		}

}
