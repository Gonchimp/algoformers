package modificadores;


import interfaces.Combinable;
import utilidadesDeJuego.Jugador;
import utilidadesDeJuego.Tablero;

public class ModificadorFusion extends Modificador{
	
	private Tablero tablero;
	private Combinable superAlgoformer;
	private Jugador jugador;
	
	public ModificadorFusion(Jugador jugador, Tablero tablero, Combinable superAlgoformer, int turnos, String nombreMod){
		
		super(turnos, nombreMod);
		this.tablero = tablero;
		this.superAlgoformer = superAlgoformer;
		this.jugador = jugador;
		
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModificadorAtaque other = (ModificadorAtaque) obj;
		if (nombreMod != other.nombreMod)
			return false;
		return true;
	}

	@Override
	public void descontarTurno() {
		this.turnosPropios = turnosPropios - 1;
		if(this.turnosPropios == 0){
			this.superAlgoformer.absorber(jugador);
			this.superAlgoformer.ubicar(tablero);
			
		}
		
	}

}
