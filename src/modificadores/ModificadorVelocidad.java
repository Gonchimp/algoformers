package modificadores;

public class ModificadorVelocidad extends Modificador{

	private double modificador;
	private String nombre;

	public ModificadorVelocidad(double porcentaje, int turnos, String nombreMod) {
		super(turnos, nombreMod);
		this.modificador = porcentaje;
		this.nombre = nombreMod;
		
	}
	
	public int modificarVelocidad(int velocidad){
		return (int) (velocidad*modificador);
	}
	
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModificadorVelocidad other = (ModificadorVelocidad) obj;
		if (nombre != other.nombre)
			return false;
		return true;
	}

}
