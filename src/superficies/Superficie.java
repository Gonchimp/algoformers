package superficies;

import algoformers.Algoformer;
import algoformers.Modo;
import utilidadesDeJuego.Recorrido;

public abstract class Superficie {
	
	public abstract void afectar(Algoformer algoformer);
	
	public abstract void descontarCantidadDeMovimientos(Recorrido recorrido);
		
	public abstract boolean puedeMoverse(Modo modo);
	
	public abstract String getNombre();

}