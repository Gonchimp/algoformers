package superficies;

import algoformers.Algoformer;
import algoformers.Modo;
import utilidadesDeJuego.Recorrido;

public class Pantano extends Superficie{
	
	static int MOVIMIENTOS = 2;
//////////////////nuevo
	private String nombre;
	public Pantano(){
	this.nombre="Pantano";
	}
////////////////////////

	public void afectar(Algoformer algoformer){
	}
	
	public void descontarCantidadDeMovimientos(Recorrido recorrido){
		recorrido.setCantidadDeMovimientos(recorrido.getCantidadDeMovimientos() - MOVIMIENTOS);
	}
	
	public boolean puedeMoverse(Modo modo){
		return modo.puedeMoversePorPantano();
	}
	public String getNombre(){
		return this.nombre;
	}
}
