package superficies;

import algoformers.Algoformer;
import algoformers.Modo;
import modificadores.ModificadorVelocidad;
import utilidadesDeJuego.Recorrido;

public class Nebulosa extends Superficie{

	static int MODIFICADOR_VELOCIDAD = 0;
	static int TURNOS = 3;
	
//////////////////nuevo
private String nombre;
public Nebulosa(){
this.nombre="Nebulosa";
}
////////////////////////

	public void afectar(Algoformer algoformer){
		ModificadorVelocidad modificador = new ModificadorVelocidad(MODIFICADOR_VELOCIDAD,TURNOS, "Nebulosa");
		algoformer.agregarModificadorVelocidad(modificador);
	
	}

	public void descontarCantidadDeMovimientos(Recorrido recorrido){
		recorrido.setCantidadDeMovimientos(0);
	}
	
	public boolean puedeMoverse(Modo modo) {
		return true;
	}
	public String getNombre(){
		return this.nombre;
	}
	
}
