package superficies;

import algoformers.Algoformer;
import algoformers.Modo;
import modificadores.ModificadorAtaque;
import utilidadesDeJuego.Recorrido;

public class Tormenta extends Superficie{

	static double MODIFICADOR_ATAQUE = 0.6;
	static int TURNOS = -1;	//Dura toda la partida porque no llega a 0.
	static int MOVIMIENTOS = 1;
//////////////////nuevo
private String nombre;
public Tormenta(){
this.nombre="Tormenta";
}
////////////////////////
	
	public void afectar(Algoformer algoformer) {
		ModificadorAtaque modificador = new ModificadorAtaque(MODIFICADOR_ATAQUE, TURNOS, "Tormenta");
		algoformer.agregarModificadorAtaque(modificador);
	}
	
	public void descontarCantidadDeMovimientos(Recorrido recorrido){
		recorrido.setCantidadDeMovimientos(recorrido.getCantidadDeMovimientos() - MOVIMIENTOS);
	}

	public boolean puedeMoverse(Modo modo) {
		return true;
	}
	public String getNombre(){
		return this.nombre;
	}


		
}
