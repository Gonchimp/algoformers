package superficies;

import algoformers.Algoformer;
import algoformers.Modo;
import utilidadesDeJuego.Recorrido;

public class Espinas extends Superficie{
	
	static double danio = 0.95;
	
	//////////////////nuevo
	private String nombre;
	public Espinas(){
		this.nombre="Espinas";
	}
	////////////////////////
	public void afectar(Algoformer algoformer){
		algoformer.modificarVidaPorcentual(Espinas.danio);
	}
	
	public void descontarCantidadDeMovimientos(Recorrido recorrido){
		recorrido.setCantidadDeMovimientos(recorrido.getCantidadDeMovimientos() - 1);
	}

	public boolean puedeMoverse(Modo modo) {
		
		return true;
	}
	public String getNombre(){
		return this.nombre;
	}
}
