package bonus;

import algoformers.Algoformer;

public abstract class Bonus{

	public abstract void aplicar(Algoformer algoformer);
	
	public abstract String getName();
}