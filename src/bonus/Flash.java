package bonus;

import algoformers.Algoformer;
import modificadores.ModificadorVelocidad;

public class Flash extends Bonus{
	private String name;
	
	public Flash(){
		this.name="Flash";
	}

	
	public void aplicar(Algoformer algoformer) {
		
		ModificadorVelocidad modificador = new ModificadorVelocidad(3, 3, "Flash");
		algoformer.agregarModificadorVelocidad(modificador);
		
	}

	
	public String getName() {
		return this.name;
	}
	
}

