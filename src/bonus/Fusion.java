package bonus;

import algoformers.Algoformer;
import interfaces.Combinable;
import modificadores.ModificadorAtaque;
import modificadores.ModificadorFusion;
import modificadores.ModificadorVelocidad;
import utilidadesDeJuego.Jugador;
import utilidadesDeJuego.Tablero;

public class Fusion {
	
	private String nombre;
	private Jugador jugador;
	private Algoformer superAlgoformer;
	
	public Fusion(Jugador jugador){
		this.nombre = "Fusion";		
		this.jugador = jugador;
		
	}
	
	public void establecerAlgoformerDeFusion(Algoformer algoformer){
		this.superAlgoformer = algoformer;
	}
	
	public void aplicar(Tablero tablero,Algoformer algoformerQueLlamoLaFusion) {

		for(Algoformer algof : jugador.obtenerListaDeAlgoformers() ){
		ModificadorAtaque modificador = new ModificadorAtaque(0, 2, "Fusion");
		algof.agregarModificadorAtaque(modificador);
		ModificadorVelocidad modificador2 = new ModificadorVelocidad(0, 2, "Fusion");
		algof.agregarModificadorVelocidad(modificador2);
		}
		
		Combinable superAlgoformer = (Combinable) this.superAlgoformer;
		
		ModificadorFusion modificador3 = new ModificadorFusion(jugador,tablero, superAlgoformer, 2, "Fusion");
		algoformerQueLlamoLaFusion.agregarModificadorFusion(modificador3);
		
	}
	
	public String getName() {
		return this.nombre;
	}
	

}
