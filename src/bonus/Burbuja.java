package bonus;

import algoformers.Algoformer;
import modificadores.ModificadorVida;

public class Burbuja extends Bonus {
	
	private String nombre;

	public Burbuja(){
		this.nombre = "Burbuja";
	}

	
	public void aplicar(Algoformer algoformer) {

		ModificadorVida modificador = new ModificadorVida(1, 2, "Burbuja");
		algoformer.agregarModificadorVida(modificador);
	}


	@Override
	public String getName() {
		return this.nombre;
	}
	
	
}