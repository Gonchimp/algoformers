package bonus;

import algoformers.Algoformer;
import modificadores.ModificadorAtaque;

public class DobleCanion extends Bonus{
	private String name;
	
	public DobleCanion(){
		this.name="DobleCanion";
	}
	
	public void aplicar(Algoformer algoformer){
		
		ModificadorAtaque modificador = new ModificadorAtaque(2, 3, "DobleCanion");
		algoformer.agregarModificadorAtaque(modificador);
		
	}

	
	public String getName() {
		return this.name;
	}

	
	
}
